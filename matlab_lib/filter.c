/*
 * filter.c
 *
 * Code generation for function 'filter'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "filter.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void filter(const real_T b[102], const emxArray_real_T *x, emxArray_real_T *y)
{
  uint16_T unnamed_idx_0;
  int32_T k;
  int32_T jend;
  int32_T j;
  unnamed_idx_0 = (uint16_T)x->size[0];
  k = y->size[0];
  y->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)y, k, (int32_T)sizeof(real_T));
  jend = unnamed_idx_0;
  for (k = 0; k < jend; k++) {
    y->data[k] = 0.0;
  }

  for (k = 0; k < 102; k++) {
    jend = (k + x->size[0]) - k;
    for (j = k; j + 1 <= jend; j++) {
      y->data[j] += b[k] * x->data[j - k];
    }
  }
}

/* End of code generation (filter.c) */
