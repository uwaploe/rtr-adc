/*
 * Main_Processing_Algorithm_terminate.c
 *
 * Code generation for function 'Main_Processing_Algorithm_terminate'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Main_Processing_Algorithm_terminate.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "Main_Processing_Algorithm_data.h"

/* Function Definitions */
void Main_Processing_Algorithm_terminate(void)
{
  emxFree_real_T(&pulseFrame);
  emxFree_real_T(&nextFrameIndA);
  emxFree_real_T(&nextFrameIndB);
}

/* End of code generation (Main_Processing_Algorithm_terminate.c) */
