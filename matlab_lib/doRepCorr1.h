/*
 * doRepCorr1.h
 *
 * Code generation for function 'doRepCorr1'
 *
 *
 */

#ifndef __DOREPCORR1_H__
#define __DOREPCORR1_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void b_doRepCorr1(emxArray_real_T *data, const real_T replica[105], emxArray_real_T *repcorrdat);
extern void doRepCorr1(emxArray_real_T *data, const real_T replica[300], emxArray_real_T *repcorrdat);
#endif
/* End of code generation (doRepCorr1.h) */
