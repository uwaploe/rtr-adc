/*
 * colon.c
 *
 * Code generation for function 'colon'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "colon.h"

/* Function Definitions */
void float_colon_length(real_T b, int32_T *n, real_T *anew, real_T *bnew,
  boolean_T *n_too_large)
{
  real_T ndbl;
  real_T cdiff;
  if (rtIsNaN(b)) {
    *n = 1;
    *anew = rtNaN;
    *bnew = b;
    *n_too_large = FALSE;
  } else if (b < 1.0) {
    *n = 0;
    *anew = 1.0;
    *bnew = 0.0;
    *n_too_large = FALSE;
  } else {
    *anew = 1.0;
    ndbl = floor((b - 1.0) + 0.5);
    *bnew = 1.0 + ndbl;
    cdiff = (1.0 + ndbl) - b;
    if (fabs(cdiff) < 4.4408920985006262E-16 * b) {
      ndbl++;
      *bnew = b;
    } else if (cdiff > 0.0) {
      *bnew = 1.0 + (ndbl - 1.0);
    } else {
      ndbl++;
    }

    *n_too_large = FALSE;
    *n = (int32_T)ndbl;
  }
}

/* End of code generation (colon.c) */
