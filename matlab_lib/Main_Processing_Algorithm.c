/*
 * Main_Processing_Algorithm.c
 *
 * Code generation for function 'Main_Processing_Algorithm'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "Calculate_Time.h"
#include "Peak_Detector.h"
#include "abs.h"
#include "doRepCorr1.h"
#include "filter.h"
#include "T0_Detect.h"
#include "Main_Processing_Algorithm_data.h"

/* Function Definitions */
void Main_Processing_Algorithm(const real_T data[30000], const real_T dataSec
  [30000], const real_T PLATFORM_FILTER_COEFFS[102], const real_T
  UNIT_FILTER_COEFFS[102], real_T OVERLAP, const real_T PLATFORM_REP_WAVEFORM
  [300], const real_T UNIT_REP_WAVEFORM[105], real_T filterStyle, const real_T
  varLimitA[4], const real_T varLimitB[4], real_T VARBUFFERLEN, real_T
  thresholdAPercent, real_T thresholdBPercent, real_T PEAK_SMOOTH, real_T
  prevPulseMatrix_data[68], int32_T prevPulseMatrix_size[2], real_T MAX_DETECTS,
  real_T SIGNAL_CHANNELS, real_T POSSIBLE_QUADS, real_T VEHICLE_TYPES, real_T
  BLANKING_TIME, real_T SAMPLES_PER_FRAME, real_T FS, real_T varA[3], real_T
  varB[3], emxArray_real_T *rxSigUnitA, emxArray_real_T *rxSigUnitB, real_T
  timeOffset_data[1], int32_T timeOffset_size[1], real_T *varACurrent, real_T
  *varBCurrent, real_T *thresholdA, real_T *thresholdB, real_T firstFrameIndA[2],
  real_T firstFrameIndB[2], real_T peakAOut_data[2], int32_T peakAOut_size[1],
  real_T peakBOut_data[2], int32_T peakBOut_size[1])
{
  real_T x;
  real_T y;
  int32_T i9;
  int32_T i;
  int32_T tmp_size[1];
  real_T tmp_data[1];
  int32_T T0_size[1];
  real_T T0_data[1];
  static real_T b_data[30000];
  emxArray_real_T *r1;
  emxArray_real_T *b_rxSigUnitA;
  emxArray_real_T *c_rxSigUnitA;
  emxArray_real_T *r2;
  emxArray_real_T *b_rxSigUnitB;
  emxArray_real_T *c_rxSigUnitB;
  emxArray_real_T *rxSigUnitRepCorrA;
  uint16_T unnamed_idx_0;
  emxArray_real_T *rxSigUnitRepCorrB;
  emxArray_real_T *d_rxSigUnitA;
  emxArray_real_T *e_rxSigUnitA;
  emxArray_real_T *r3;
  emxArray_real_T *r4;
  emxArray_real_T *d_rxSigUnitB;
  emxArray_real_T *e_rxSigUnitB;
  emxArray_real_T *r5;
  emxArray_real_T *r6;
  emxArray_real_T *r7;
  emxArray_real_T *b_rxSigUnitRepCorrA;
  emxArray_real_T *b_rxSigUnitRepCorrB;
  real_T b_varBCurrent;
  real_T b_varACurrent;
  real_T b_nextFrameIndB;
  real_T b_nextFrameIndA;
  real_T indsColB[20];
  real_T indsColA[20];
  real_T dv1[20];
  real_T dv2[20];
  int32_T b_tmp_size[2];
  int32_T i10;
  real_T b_tmp_data[68];
  emxArray_real_T *prevPulseMatrix;
  real_T c_tmp_data[1];
  int32_T loop_ub;

  /* ****************************************************************************** */
  /*  Project: ICEX */
  /*  Script: Main_Processing_Algorithm */
  /*  Programmer: Sean Lastuka */
  /*  Origination Date: 20131114 */
  /*  */
  /*  Description: */
  /*  This function contains all code necessary to filter, correlate, detect */
  /*  and timestamp frames of real-time or simulated hydrophone data. */
  /*   */
  /*  Functions Called: FIR_Filter, Replica_Correlation, Peak_Detector, */
  /*  Calculate_Time */
  /*  */
  /*  Input:    PLATFORM_FILTER_COEFFS - FIR filter for Platform */
  /*            UNIT_FILTER_COEFFS - FIR filter for Unit */
  /*            OVERLAP - Overlap required for acceptable boundary processing */
  /*            data - One frame of input data (typically 30k 24-bit samples) */
  /*            PLATFORM_REP_WAVEFORM) - Replica Platform Waveform */
  /*            UNIT_REP_WAVEFORM - Replica Unit Waveform */
  /*            filterStyle - DJ (2), Digital (1) or Analog-Style (0) filtering technique */
  /*            varLimitA - A limit of the ratio of the variance of the current  */
  /*                frame to the mean variance of the previous three frames.   */
  /*                When this ratio exceeds the varLimitA, the maximum points */
  /*                in the frame are processed and converted into timestamp */
  /*                output.   */
  /*                Used for Frequency A */
  /*                Set by the User.  */
  /*                Default value = 2 */
  /*            varLimitB - A limit of the ratio of the variance of the current  */
  /*                frame to the mean variance of the previous three frames.   */
  /*                When this ratio exceeds the varLimitA, the maximum points */
  /*                in the frame are processed and converted into timestamp */
  /*                output.   */
  /*                Used for Frequency B   */
  /*                Set by the User */
  /*                Default value = 2 */
  /*            thresholdAPercent - Determines level at which threshold line is */
  /*                set for a selected frame of data.  The level is */
  /*                thresholdAPercent of the difference between the maximum */
  /*                value in the frame and the mean value of the frame */
  /*                Used for Frequency A   */
  /*                Set by the User */
  /*                Default value = 20 */
  /*            thresholdBPercent - Determines level at which threshold line is */
  /*                set for a selected frame of data.  The level is */
  /*                thresholdBPercent of the difference between the maximum */
  /*                value in the frame and the mean value of the frame */
  /*                Used for Frequency B   */
  /*                Set by the User */
  /*                Default value = 20 */
  /*            PEAK_SMOOTH - quantity of peaks that are bridged after */
  /*                thresholding.  Increasing this value results in fewer */
  /*                timestamps in a given frame.   */
  /*                Used for Frequency A & B   */
  /*                Default value = 5 */
  /*            FS - Samples per second */
  /*                Default value = 30000 */
  /*            SAMPLES_PER_FRAME - number of samples in one frame of DAQ data */
  /*                Default value = FS   */
  /*                AKA samplesPerLoop */
  /*            MAX_DETECTS - maximum number of timestamps detections per frame */
  /*                Default value = 2 */
  /*            T0 - 1PPS timestamp indice */
  /*                Default value = 15000 (when 1PPS is not detected) */
  /*            SIGNAL_CHANNELS - Number of channels in acoustic data stream */
  /*                Used for pulseFrame organization */
  /*                Default value = 1 */
  /*            POSSIBLE_QUADS - Number of quads used for pulseFrame */
  /*                                organization */
  /*                pulseFrame matrix */
  /*                Default value = 1 */
  /*            VEHICLE_TYPES - Number of tracked objects */
  /*                Default value = 2 (platform + unit) */
  /*            BLANKING_TIME - seconds to ignore pulses after a detection */
  /*                Default value = 0.03  */
  /*                35ms doublet should be next detection */
  /*            dataSec - one frame of 1PPS signal samples.  Rising edge is */
  /*                    detected to provide a timebase for the Peak_Detection */
  /*                    software. */
  /*            varA - array of length VARBUFFERLEN (3) which contains the */
  /*                            variance of the last three frames for the  */
  /*                            platform pulse */
  /*            varB - array of length VARBUFFERLEN (3) which contains the */
  /*                            variance of the last three frames for the  */
  /*                            unit pulse */
  /*  */
  /*  Output:   rxSigUnitRepCorrA - Output Correlation Waveform (Platform) */
  /*            rxSigUnitRepCorrB - Output Correlation Waveform (Unit) */
  /*            prevPulseMatrix - timestamp display/rs232 output data */
  /*            varAOutput - array of platform variance output for last three */
  /*                            frames */
  /*            varBOutput - array of platform variance output for last three */
  /*                            frames */
  /*            timeOffset - T0 location in frame (milliseconds) */
  /*            varA - array of length VARBUFFERLEN (3) which contains the */
  /*                            variance of the last three frames for the  */
  /*                            platform pulse */
  /*            varB - array of length VARBUFFERLEN (3) which contains the */
  /*                            variance of the last three frames for the  */
  /*                            unit pulse */
  /*             */
  /*  */
  /*  Modification History */
  /*  20131114 - Sean Lastuka, Originate with FIR + Correlation Filters */
  /*  20131121 - Sean Lastuka, Add Peak Detector and Time Calculation */
  /*  20131126 - Sean Lastuka, Added Persistent variables to reduce I/O */
  /*                parameters */
  /*  20131205 - Sean Lastuka, Rev 60.002, Added variance and T0 offset debug output */
  /*  20131206 - Sean Lastuka, Rev 60.003, Added itertive end point fit output */
  /*  20140221 - Sean Lastuka, Rev 60.004, Added scalar variance output */
  /*  20140222 - Sean Lastuka, Rev 60.005, Removed persistence for varA and */
  /*        varB and added those variables to input/output parameters */
  /*  20140223 - Sean Lastuka, Rev 60.006, Removed varDebugA,B & varOutputA,B   */
  /*  20140225 - Sean Lastuka, Rev 60.008, Added varCurrentA,B   */
  /*  */
  /* ****************************************************************************** */
  /*             %% Initialize persistent variables */
  /*  Tracks 1PPS sample index from frame to frame */
  /*  Intialize the bookkeeping matrixes used for timestamp processing */
  if (!pulseFrame_not_empty) {
    x = ceil(1.0 / BLANKING_TIME);
    y = SIGNAL_CHANNELS * POSSIBLE_QUADS * VEHICLE_TYPES;
    i9 = pulseFrame->size[0] * pulseFrame->size[1];
    pulseFrame->size[0] = (int32_T)x;
    pulseFrame->size[1] = (int32_T)y;
    emxEnsureCapacity((emxArray__common *)pulseFrame, i9, (int32_T)sizeof(real_T));
    i = (int32_T)x * (int32_T)y;
    for (i9 = 0; i9 < i; i9++) {
      pulseFrame->data[i9] = -1.0;
    }

    pulseFrame_not_empty = !((pulseFrame->size[0] == 0) || (pulseFrame->size[1] ==
      0));
  }

  if (!nextFrameIndA_not_empty) {
    i9 = nextFrameIndA->size[0] * nextFrameIndA->size[1];
    nextFrameIndA->size[0] = 1;
    nextFrameIndA->size[1] = (int32_T)SIGNAL_CHANNELS;
    emxEnsureCapacity((emxArray__common *)nextFrameIndA, i9, (int32_T)sizeof
                      (real_T));
    i = (int32_T)SIGNAL_CHANNELS;
    for (i9 = 0; i9 < i; i9++) {
      nextFrameIndA->data[i9] = -1.0;
    }

    nextFrameIndA_not_empty = !(nextFrameIndA->size[1] == 0);
  }

  if (!nextFrameIndB_not_empty) {
    i9 = nextFrameIndB->size[0] * nextFrameIndB->size[1];
    nextFrameIndB->size[0] = 1;
    nextFrameIndB->size[1] = (int32_T)SIGNAL_CHANNELS;
    emxEnsureCapacity((emxArray__common *)nextFrameIndB, i9, (int32_T)sizeof
                      (real_T));
    i = (int32_T)SIGNAL_CHANNELS;
    for (i9 = 0; i9 < i; i9++) {
      nextFrameIndB->data[i9] = -1.0;
    }

    nextFrameIndB_not_empty = !(nextFrameIndB->size[1] == 0);
  }

  /*  initialize the array of frame variance calculations used for detection */
  /*  persistent varA; */
  /*  if isempty(varA) */
  /*      varA = 0.0001*ones(VARBUFFERLEN,SIGNAL_CHANNELS); % running avg calculation */
  /*  end */
  /*   */
  /*  persistent varB; */
  /*  if isempty(varB) */
  /*      varB = 0.0001*ones(VARBUFFERLEN,SIGNAL_CHANNELS); % running avg calculation */
  /*  end */
  /* % 1PPS detect ->  used lastT0(end) because of matlab coder bug */
  T0_Detect(dataSec, lastT0, FS, T0_data, T0_size, tmp_data, tmp_size);
  lastT0 = tmp_data[0];

  /* % FIR Filter */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   FIR_Filter.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       June 27, 2012 */
  /*   */
  /*  Desc:       FIR Filter on input data */
  /*              NOTE - The FIR is performed on the previous frame of data */
  /*                Therefore, another second of delay is required in */
  /*                Read_UTC.m */
  /*  */
  /*  Inputs:     filterCoeffsA:   FIR filter coefficients for the platform */
  /*              filterCoeffsB:   FIR filter coefficients for the unit */
  /*              data:            Input Data with each hydrophone channel in a */
  /*                                column */
  /*              overlap:         number of samples of overlap used at frame */
  /*                                transitions */
  /*              corrFitlerLengthA: length of Platform Correlation Filter  */
  /*              corrFitlerLengthB: length of Unit Correlation Filter  */
  /*              */
  /*  Outputs:    rxSigUnitA:      one frame of conditioned plaform FIR output */
  /*              rxSigUnitB:      one frame of conditioned unit FIR output  */
  /*  */
  /*  Revisions:  7/30/12 - Added inputs for correlation filter length and  */
  /*                frame-to-frame overlap */
  /*              3/5/14 - Added  */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  y = oldData[0];
  for (i = 0; i < 29999; i++) {
    y += oldData[i + 1];
  }

  y /= 30000.0;
  for (i9 = 0; i9 < 30000; i9++) {
    oldData[i9] -= y;
  }

  y = data[0];
  for (i = 0; i < 29999; i++) {
    y += data[i + 1];
  }

  y /= 30000.0;
  for (i = 0; i < 30000; i++) {
    b_data[i] = data[i] - y;
  }

  /*     %% FILTER SIGNAL: 50 Point Bandpass FIR filters */
  /*  10250Hz Center Freq */
  if (1.0 > 402.0 + OVERLAP) {
    i = -1;
  } else {
    i = (int32_T)(402.0 + OVERLAP) - 1;
  }

  b_emxInit_real_T(&r1, 1);
  i9 = r1->size[0];
  r1->size[0] = i + 30001;
  emxEnsureCapacity((emxArray__common *)r1, i9, (int32_T)sizeof(real_T));
  for (i9 = 0; i9 < 30000; i9++) {
    r1->data[i9] = oldData[i9];
  }

  for (i9 = 0; i9 <= i; i9++) {
    r1->data[i9 + 30000] = b_data[i9];
  }

  b_emxInit_real_T(&b_rxSigUnitA, 1);
  b_emxInit_real_T(&c_rxSigUnitA, 1);
  filter(PLATFORM_FILTER_COEFFS, r1, b_rxSigUnitA);
  i = b_rxSigUnitA->size[0] - 51;
  i9 = c_rxSigUnitA->size[0];
  c_rxSigUnitA->size[0] = i + 1;
  emxEnsureCapacity((emxArray__common *)c_rxSigUnitA, i9, (int32_T)sizeof(real_T));
  emxFree_real_T(&r1);
  for (i9 = 0; i9 <= i; i9++) {
    c_rxSigUnitA->data[i9] = b_rxSigUnitA->data[50 + i9];
  }

  i9 = b_rxSigUnitA->size[0];
  b_rxSigUnitA->size[0] = c_rxSigUnitA->size[0];
  emxEnsureCapacity((emxArray__common *)b_rxSigUnitA, i9, (int32_T)sizeof(real_T));
  i = c_rxSigUnitA->size[0];
  for (i9 = 0; i9 < i; i9++) {
    b_rxSigUnitA->data[i9] = c_rxSigUnitA->data[i9];
  }

  emxFree_real_T(&c_rxSigUnitA);

  /*  13500Hz Center Freq */
  if (1.0 > 207.0 + OVERLAP) {
    i = -1;
  } else {
    i = (int32_T)(207.0 + OVERLAP) - 1;
  }

  b_emxInit_real_T(&r2, 1);
  i9 = r2->size[0];
  r2->size[0] = i + 30001;
  emxEnsureCapacity((emxArray__common *)r2, i9, (int32_T)sizeof(real_T));
  for (i9 = 0; i9 < 30000; i9++) {
    r2->data[i9] = oldData[i9];
  }

  for (i9 = 0; i9 <= i; i9++) {
    r2->data[i9 + 30000] = b_data[i9];
  }

  b_emxInit_real_T(&b_rxSigUnitB, 1);
  b_emxInit_real_T(&c_rxSigUnitB, 1);
  filter(UNIT_FILTER_COEFFS, r2, b_rxSigUnitB);
  i = b_rxSigUnitB->size[0] - 51;
  i9 = c_rxSigUnitB->size[0];
  c_rxSigUnitB->size[0] = i + 1;
  emxEnsureCapacity((emxArray__common *)c_rxSigUnitB, i9, (int32_T)sizeof(real_T));
  emxFree_real_T(&r2);
  for (i9 = 0; i9 <= i; i9++) {
    c_rxSigUnitB->data[i9] = b_rxSigUnitB->data[50 + i9];
  }

  i9 = b_rxSigUnitB->size[0];
  b_rxSigUnitB->size[0] = c_rxSigUnitB->size[0];
  emxEnsureCapacity((emxArray__common *)b_rxSigUnitB, i9, (int32_T)sizeof(real_T));
  i = c_rxSigUnitB->size[0];
  for (i9 = 0; i9 < i; i9++) {
    b_rxSigUnitB->data[i9] = c_rxSigUnitB->data[i9];
  }

  emxFree_real_T(&c_rxSigUnitB);
  memcpy(&oldData[0], &b_data[0], 30000U * sizeof(real_T));
  i9 = rxSigUnitA->size[0];
  rxSigUnitA->size[0] = b_rxSigUnitA->size[0];
  emxEnsureCapacity((emxArray__common *)rxSigUnitA, i9, (int32_T)sizeof(real_T));
  i = b_rxSigUnitA->size[0];
  for (i9 = 0; i9 < i; i9++) {
    rxSigUnitA->data[i9] = b_rxSigUnitA->data[i9];
  }

  i9 = rxSigUnitB->size[0];
  rxSigUnitB->size[0] = b_rxSigUnitB->size[0];
  emxEnsureCapacity((emxArray__common *)rxSigUnitB, i9, (int32_T)sizeof(real_T));
  i = b_rxSigUnitB->size[0];
  for (i9 = 0; i9 < i; i9++) {
    rxSigUnitB->data[i9] = b_rxSigUnitB->data[i9];
  }

  b_emxInit_real_T(&rxSigUnitRepCorrA, 1);

  /* % Replica Correlation */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   Replica_Correlation.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       June 28, 2012 */
  /*   */
  /*  Desc:       Correlates input data to replica.  Primary filter is a */
  /*                replica corraltion.  An 'analog' option exists which  */
  /*                mimics the operation of the legacy Analog Tracking  */
  /*                Range (ATR) */
  /*  */
  /*  Inputs:     txPulseA:        Platform replica correlation waveform  */
  /*              txPulseB:        Unit replica correlation waveform  */
  /*              rxSigUnitA:      FIR Filtered platform data */
  /*              rxSigUnitB:      FIR Filtered unit data */
  /*              filterStyle:    envelope filter or matched correlation */
  /*                                filter */
  /*  */
  /*  Outputs:    rxSigUnitRepCorrA: Correlated platform data */
  /*              rxSigUnitRepCorrB: Correlated unit data */
  /*  */
  /*  Revisions: */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  initialize matrices */
  unnamed_idx_0 = (uint16_T)b_rxSigUnitA->size[0];
  i9 = rxSigUnitRepCorrA->size[0];
  rxSigUnitRepCorrA->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)rxSigUnitRepCorrA, i9, (int32_T)sizeof
                    (real_T));
  i = unnamed_idx_0;
  for (i9 = 0; i9 < i; i9++) {
    rxSigUnitRepCorrA->data[i9] = 0.0;
  }

  b_emxInit_real_T(&rxSigUnitRepCorrB, 1);
  unnamed_idx_0 = (uint16_T)b_rxSigUnitB->size[0];
  i9 = rxSigUnitRepCorrB->size[0];
  rxSigUnitRepCorrB->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)rxSigUnitRepCorrB, i9, (int32_T)sizeof
                    (real_T));
  i = unnamed_idx_0;
  for (i9 = 0; i9 < i; i9++) {
    rxSigUnitRepCorrB->data[i9] = 0.0;
  }

  if (filterStyle != 0.0) {
    b_emxInit_real_T(&d_rxSigUnitA, 1);
    b_emxInit_real_T(&e_rxSigUnitA, 1);

    /*  replication correlation */
    /*  10250Hz Center Freq */
    i9 = e_rxSigUnitA->size[0];
    e_rxSigUnitA->size[0] = b_rxSigUnitA->size[0];
    emxEnsureCapacity((emxArray__common *)e_rxSigUnitA, i9, (int32_T)sizeof
                      (real_T));
    i = b_rxSigUnitA->size[0];
    for (i9 = 0; i9 < i; i9++) {
      e_rxSigUnitA->data[i9] = b_rxSigUnitA->data[i9];
    }

    i = b_rxSigUnitA->size[0];
    i9 = d_rxSigUnitA->size[0];
    d_rxSigUnitA->size[0] = i;
    emxEnsureCapacity((emxArray__common *)d_rxSigUnitA, i9, (int32_T)sizeof
                      (real_T));
    for (i9 = 0; i9 < i; i9++) {
      d_rxSigUnitA->data[i9] = e_rxSigUnitA->data[i9];
    }

    emxFree_real_T(&e_rxSigUnitA);
    b_emxInit_real_T(&r3, 1);
    b_emxInit_real_T(&r4, 1);
    doRepCorr1(d_rxSigUnitA, PLATFORM_REP_WAVEFORM, r4);
    b_abs(r4, r3);
    i = r3->size[0];
    emxFree_real_T(&d_rxSigUnitA);
    emxFree_real_T(&r4);
    for (i9 = 0; i9 < i; i9++) {
      rxSigUnitRepCorrA->data[i9] = r3->data[i9];
    }

    b_emxInit_real_T(&d_rxSigUnitB, 1);
    b_emxInit_real_T(&e_rxSigUnitB, 1);

    /*  13500Hz Center Freq */
    i9 = e_rxSigUnitB->size[0];
    e_rxSigUnitB->size[0] = b_rxSigUnitB->size[0];
    emxEnsureCapacity((emxArray__common *)e_rxSigUnitB, i9, (int32_T)sizeof
                      (real_T));
    i = b_rxSigUnitB->size[0];
    for (i9 = 0; i9 < i; i9++) {
      e_rxSigUnitB->data[i9] = b_rxSigUnitB->data[i9];
    }

    i = b_rxSigUnitB->size[0];
    i9 = d_rxSigUnitB->size[0];
    d_rxSigUnitB->size[0] = i;
    emxEnsureCapacity((emxArray__common *)d_rxSigUnitB, i9, (int32_T)sizeof
                      (real_T));
    for (i9 = 0; i9 < i; i9++) {
      d_rxSigUnitB->data[i9] = e_rxSigUnitB->data[i9];
    }

    emxFree_real_T(&e_rxSigUnitB);
    b_emxInit_real_T(&r5, 1);
    b_doRepCorr1(d_rxSigUnitB, UNIT_REP_WAVEFORM, r5);
    b_abs(r5, r3);
    i = r3->size[0];
    emxFree_real_T(&d_rxSigUnitB);
    emxFree_real_T(&r5);
    for (i9 = 0; i9 < i; i9++) {
      rxSigUnitRepCorrB->data[i9] = r3->data[i9];
    }

    emxFree_real_T(&r3);
  } else {
    /*  'analog' style envelope filter */
    b_abs(b_rxSigUnitA, rxSigUnitRepCorrA);
    b_abs(b_rxSigUnitB, rxSigUnitRepCorrB);
  }

  emxFree_real_T(&b_rxSigUnitB);
  emxFree_real_T(&b_rxSigUnitA);
  emxInit_real_T(&r6, 2);

  /* % */
  i9 = r6->size[0] * r6->size[1];
  r6->size[0] = 1;
  r6->size[1] = nextFrameIndA->size[1];
  emxEnsureCapacity((emxArray__common *)r6, i9, (int32_T)sizeof(real_T));
  i = nextFrameIndA->size[0] * nextFrameIndA->size[1];
  for (i9 = 0; i9 < i; i9++) {
    r6->data[i9] = nextFrameIndA->data[i9];
  }

  emxInit_real_T(&r7, 2);
  i9 = r7->size[0] * r7->size[1];
  r7->size[0] = 1;
  r7->size[1] = nextFrameIndB->size[1];
  emxEnsureCapacity((emxArray__common *)r7, i9, (int32_T)sizeof(real_T));
  i = nextFrameIndB->size[0] * nextFrameIndB->size[1];
  for (i9 = 0; i9 < i; i9++) {
    r7->data[i9] = nextFrameIndB->data[i9];
  }

  b_emxInit_real_T(&b_rxSigUnitRepCorrA, 1);
  i9 = b_rxSigUnitRepCorrA->size[0];
  b_rxSigUnitRepCorrA->size[0] = rxSigUnitRepCorrA->size[0];
  emxEnsureCapacity((emxArray__common *)b_rxSigUnitRepCorrA, i9, (int32_T)sizeof
                    (real_T));
  i = rxSigUnitRepCorrA->size[0];
  for (i9 = 0; i9 < i; i9++) {
    b_rxSigUnitRepCorrA->data[i9] = rxSigUnitRepCorrA->data[i9];
  }

  b_emxInit_real_T(&b_rxSigUnitRepCorrB, 1);
  i9 = b_rxSigUnitRepCorrB->size[0];
  b_rxSigUnitRepCorrB->size[0] = rxSigUnitRepCorrB->size[0];
  emxEnsureCapacity((emxArray__common *)b_rxSigUnitRepCorrB, i9, (int32_T)sizeof
                    (real_T));
  i = rxSigUnitRepCorrB->size[0];
  for (i9 = 0; i9 < i; i9++) {
    b_rxSigUnitRepCorrB->data[i9] = rxSigUnitRepCorrB->data[i9];
  }

  Peak_Detector(b_rxSigUnitRepCorrA, b_rxSigUnitRepCorrB, varA, varB, varLimitA,
                varLimitB, r6, r7, filterStyle, thresholdAPercent,
                thresholdBPercent, PEAK_SMOOTH, SAMPLES_PER_FRAME, indsColA,
                indsColB, thresholdA, thresholdB, rxSigUnitRepCorrA,
                rxSigUnitRepCorrB, &b_nextFrameIndA, &b_nextFrameIndB,
                &b_varACurrent, &b_varBCurrent, peakAOut_data, peakAOut_size,
                peakBOut_data, peakBOut_size);
  i9 = nextFrameIndA->size[0] * nextFrameIndA->size[1];
  nextFrameIndA->size[0] = 1;
  nextFrameIndA->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)nextFrameIndA, i9, (int32_T)sizeof
                    (real_T));
  nextFrameIndA->data[0] = b_nextFrameIndA;
  nextFrameIndA_not_empty = TRUE;
  i9 = nextFrameIndB->size[0] * nextFrameIndB->size[1];
  nextFrameIndB->size[0] = 1;
  nextFrameIndB->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)nextFrameIndB, i9, (int32_T)sizeof
                    (real_T));
  nextFrameIndB->data[0] = b_nextFrameIndB;
  nextFrameIndB_not_empty = TRUE;
  *varACurrent = b_varACurrent;
  *varBCurrent = b_varBCurrent;

  /* % */
  i = T0_size[0];
  emxFree_real_T(&b_rxSigUnitRepCorrB);
  emxFree_real_T(&b_rxSigUnitRepCorrA);
  emxFree_real_T(&r7);
  emxFree_real_T(&r6);
  emxFree_real_T(&rxSigUnitRepCorrB);
  emxFree_real_T(&rxSigUnitRepCorrA);
  for (i9 = 0; i9 < i; i9++) {
    tmp_data[i9] = T0_data[i9];
  }

  for (i = 0; i < 20; i++) {
    dv1[i] = indsColA[i];
    dv2[i] = indsColB[i];
  }

  b_tmp_size[0] = prevPulseMatrix_size[0];
  b_tmp_size[1] = 2;
  for (i9 = 0; i9 < 2; i9++) {
    i = prevPulseMatrix_size[0];
    for (i10 = 0; i10 < i; i10++) {
      b_tmp_data[i10 + b_tmp_size[0] * i9] = prevPulseMatrix_data[i10 +
        prevPulseMatrix_size[0] * i9];
    }
  }

  emxInit_real_T(&prevPulseMatrix, 2);
  c_tmp_data[0] = tmp_data[0];
  Calculate_Time(dv1, dv2, pulseFrame, b_tmp_data, b_tmp_size, MAX_DETECTS,
                 c_tmp_data, SIGNAL_CHANNELS, POSSIBLE_QUADS, VEHICLE_TYPES,
                 BLANKING_TIME, SAMPLES_PER_FRAME, FS, prevPulseMatrix);
  pulseFrame_not_empty = !((pulseFrame->size[0] == 0) || (pulseFrame->size[1] ==
    0));
  prevPulseMatrix_size[0] = prevPulseMatrix->size[0];
  prevPulseMatrix_size[1] = prevPulseMatrix->size[1];
  i = prevPulseMatrix->size[1];
  for (i9 = 0; i9 < i; i9++) {
    loop_ub = prevPulseMatrix->size[0];
    for (i10 = 0; i10 < loop_ub; i10++) {
      prevPulseMatrix_data[i10 + prevPulseMatrix_size[0] * i9] =
        prevPulseMatrix->data[i10 + prevPulseMatrix->size[0] * i9];
    }
  }

  emxFree_real_T(&prevPulseMatrix);

  /* %  */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   Debug_Output.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       Dec 5, 2013 */
  /*   */
  /*  Desc:       Output RTR debug data for use in logfile or standard output */
  /*                stream.  Intended for use in the real-time function.  Some */
  /*                input parameters are global variables which cannot be */
  /*                passed out of the realtime function */
  /*                Main_Processing_Algorithm.  Debug_Output simply */
  /*                reassigns global variables to intermediate variables which */
  /*                can be passed as output.   */
  /*  */
  /*  Inputs:     T0 - index of 1PPS edge in current frame of sampled data */
  /*              SAMPLES_PER_FRAME - constant defined at initialization */
  /*  */
  /*  Outputs:    timeOffset - 1PPS edge location from beginning of the frame. */
  /*                Measured in milliseconds.   */
  /*               */
  /*  Revisions:  20140223 - Removed all variance related output */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  timeOffset_size[0] = T0_size[0];
  i = T0_size[0];
  for (i9 = 0; i9 < i; i9++) {
    timeOffset_data[i9] = 1000.0 * T0_data[i9] / SAMPLES_PER_FRAME;
  }

  for (i = 0; i < 2; i++) {
    firstFrameIndA[i] = indsColA[i];
  }

  for (i = 0; i < 2; i++) {
    firstFrameIndB[i] = indsColB[i];
  }
}

/* End of code generation (Main_Processing_Algorithm.c) */
