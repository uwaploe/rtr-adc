/*
 * diff.h
 *
 * Code generation for function 'diff'
 *
 *
 */

#ifndef __DIFF_H__
#define __DIFF_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void diff(const boolean_T x_data[59950], const int32_T x_size[1], emxArray_real_T *y);
#endif
/* End of code generation (diff.h) */
