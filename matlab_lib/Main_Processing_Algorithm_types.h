/*
 * Main_Processing_Algorithm_types.h
 *
 * Code generation for function 'Main_Processing_Algorithm'
 *
 *
 */

#ifndef __MAIN_PROCESSING_ALGORITHM_TYPES_H__
#define __MAIN_PROCESSING_ALGORITHM_TYPES_H__

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef struct_emxArray__common
#define struct_emxArray__common
struct emxArray__common
{
    void *data;
    int32_T *size;
    int32_T allocatedSize;
    int32_T numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray__common*/
#ifndef typedef_emxArray__common
#define typedef_emxArray__common
typedef struct emxArray__common emxArray__common;
#endif /*typedef_emxArray__common*/
#ifndef struct_emxArray_b_real_T_1
#define struct_emxArray_b_real_T_1
struct emxArray_b_real_T_1
{
    real_T data[1];
    int32_T size[1];
};
#endif /*struct_emxArray_b_real_T_1*/
#ifndef typedef_emxArray_b_real_T_1
#define typedef_emxArray_b_real_T_1
typedef struct emxArray_b_real_T_1 emxArray_b_real_T_1;
#endif /*typedef_emxArray_b_real_T_1*/
#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T
struct emxArray_boolean_T
{
    boolean_T *data;
    int32_T *size;
    int32_T allocatedSize;
    int32_T numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_boolean_T*/
#ifndef typedef_emxArray_boolean_T
#define typedef_emxArray_boolean_T
typedef struct emxArray_boolean_T emxArray_boolean_T;
#endif /*typedef_emxArray_boolean_T*/
#ifndef struct_emxArray_boolean_T_1
#define struct_emxArray_boolean_T_1
struct emxArray_boolean_T_1
{
    boolean_T data[1];
    int32_T size[1];
};
#endif /*struct_emxArray_boolean_T_1*/
#ifndef typedef_emxArray_boolean_T_1
#define typedef_emxArray_boolean_T_1
typedef struct emxArray_boolean_T_1 emxArray_boolean_T_1;
#endif /*typedef_emxArray_boolean_T_1*/
#ifndef struct_emxArray_boolean_T_59950
#define struct_emxArray_boolean_T_59950
struct emxArray_boolean_T_59950
{
    boolean_T data[59950];
    int32_T size[1];
};
#endif /*struct_emxArray_boolean_T_59950*/
#ifndef typedef_emxArray_boolean_T_59950
#define typedef_emxArray_boolean_T_59950
typedef struct emxArray_boolean_T_59950 emxArray_boolean_T_59950;
#endif /*typedef_emxArray_boolean_T_59950*/
#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T
struct emxArray_int32_T
{
    int32_T *data;
    int32_T *size;
    int32_T allocatedSize;
    int32_T numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_int32_T*/
#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T
typedef struct emxArray_int32_T emxArray_int32_T;
#endif /*typedef_emxArray_int32_T*/
#ifndef struct_emxArray_int32_T_1
#define struct_emxArray_int32_T_1
struct emxArray_int32_T_1
{
    int32_T data[1];
    int32_T size[1];
};
#endif /*struct_emxArray_int32_T_1*/
#ifndef typedef_emxArray_int32_T_1
#define typedef_emxArray_int32_T_1
typedef struct emxArray_int32_T_1 emxArray_int32_T_1;
#endif /*typedef_emxArray_int32_T_1*/
#ifndef struct_emxArray_int32_T_2
#define struct_emxArray_int32_T_2
struct emxArray_int32_T_2
{
    int32_T data[2];
    int32_T size[1];
};
#endif /*struct_emxArray_int32_T_2*/
#ifndef typedef_emxArray_int32_T_2
#define typedef_emxArray_int32_T_2
typedef struct emxArray_int32_T_2 emxArray_int32_T_2;
#endif /*typedef_emxArray_int32_T_2*/
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T
struct emxArray_real_T
{
    real_T *data;
    int32_T *size;
    int32_T allocatedSize;
    int32_T numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_real_T*/
#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T
typedef struct emxArray_real_T emxArray_real_T;
#endif /*typedef_emxArray_real_T*/

#endif
/* End of code generation (Main_Processing_Algorithm_types.h) */
