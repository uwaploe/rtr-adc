/*
 * abs.c
 *
 * Code generation for function 'abs'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "abs.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void b_abs(const emxArray_real_T *x, emxArray_real_T *y)
{
  uint16_T unnamed_idx_0;
  int32_T k;
  unnamed_idx_0 = (uint16_T)x->size[0];
  k = y->size[0];
  y->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)y, k, (int32_T)sizeof(real_T));
  for (k = 0; k < x->size[0]; k++) {
    y->data[k] = fabs(x->data[k]);
  }
}

/* End of code generation (abs.c) */
