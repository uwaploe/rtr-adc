/*
 * flipud.c
 *
 * Code generation for function 'flipud'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "flipud.h"

/* Function Definitions */
void flipud(real_T x[300])
{
  int32_T i;
  real_T xtmp;
  for (i = 0; i < 150; i++) {
    xtmp = x[i];
    x[i] = x[299 - i];
    x[299 - i] = xtmp;
  }
}

/* End of code generation (flipud.c) */
