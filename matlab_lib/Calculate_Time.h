/*
 * Calculate_Time.h
 *
 * Code generation for function 'Calculate_Time'
 *
 *
 */

#ifndef __CALCULATE_TIME_H__
#define __CALCULATE_TIME_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void Calculate_Time(const real_T indsColA[20], const real_T indsColB[20], emxArray_real_T *b_pulseFrame, const real_T prevPulseMatrix_data[68], const int32_T prevPulseMatrix_size[2], real_T maxDetects, const real_T T0_data[1], real_T channels, real_T possibleQuads, real_T vehicleTypes, real_T blanking, real_T samplesPerLoop, real_T fs, emxArray_real_T *prevPulseMatrix);
#endif
/* End of code generation (Calculate_Time.h) */
