/*
 * T0_Detect.h
 *
 * Code generation for function 'T0_Detect'
 *
 *
 */

#ifndef __T0_DETECT_H__
#define __T0_DETECT_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void T0_Detect(const real_T dataSec[30000], real_T b_lastT0, real_T fs, real_T T0_data[1], int32_T T0_size[1], real_T lastT0_data[1], int32_T lastT0_size[1]);
#endif
/* End of code generation (T0_Detect.h) */
