/*
 * Peak_Detector.h
 *
 * Code generation for function 'Peak_Detector'
 *
 *
 */

#ifndef __PEAK_DETECTOR_H__
#define __PEAK_DETECTOR_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void Peak_Detector(const emxArray_real_T *rxSigUnitRepCorrA, const emxArray_real_T *rxSigUnitRepCorrB, real_T varA[3], real_T varB[3], const real_T varLimitA[4], const real_T varLimitB[4], const emxArray_real_T *b_nextFrameIndA, const emxArray_real_T *b_nextFrameIndB, real_T filterStyle, real_T thresholdAPercent, real_T thresholdBPercent, real_T thresholdSmooth, real_T samplesPerLoop, real_T indsColA[20], real_T indsColB[20], real_T *thresholdA, real_T *thresholdB, emxArray_real_T *ampRxSigRepCorrA, emxArray_real_T *ampRxSigRepCorrB, real_T *c_nextFrameIndA, real_T *c_nextFrameIndB, real_T *varACurrent, real_T *varBCurrent, real_T peakAOut_data[2], int32_T peakAOut_size[1], real_T peakBOut_data[2], int32_T peakBOut_size[1]);
#endif
/* End of code generation (Peak_Detector.h) */
