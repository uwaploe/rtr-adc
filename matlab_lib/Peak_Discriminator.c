/*
 * Peak_Discriminator.c
 *
 * Code generation for function 'Peak_Discriminator'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Peak_Discriminator.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "round.h"
#include "sum.h"
#include "colon.h"
#include "power.h"
#include "any.h"
#include "diff.h"
#include "Main_Processing_Algorithm_rtwutil.h"

/* Function Definitions */
void Peak_Discriminator(const boolean_T peak_data[59950], const int32_T
  peak_size[1], const emxArray_real_T *ampRxSigRepCorr, const emxArray_real_T
  *nextFrameInd, real_T filterStyle, real_T samplesPerLoop, real_T indsCol[20],
  real_T *b_nextFrameInd)
{
  int32_T i;
  emxArray_boolean_T *timestampStartIndex;
  int32_T i4;
  int32_T idx;
  boolean_T b_peak_data[59950];
  int32_T ii_data[1];
  emxArray_real_T *r0;
  int32_T x_size_idx_1;
  boolean_T x_data[59949];
  emxArray_int32_T *ii;
  boolean_T exitg8;
  boolean_T guard3 = FALSE;
  emxArray_int32_T *b_ii;
  emxArray_real_T *pulseStartTime;
  int32_T b_peak_size[1];
  boolean_T exitg7;
  boolean_T guard2 = FALSE;
  emxArray_int32_T *c_ii;
  emxArray_real_T *pulseEndTime;
  int32_T i5;
  emxArray_real_T *b_pulseEndTime;
  emxArray_boolean_T *x;
  int32_T k;
  boolean_T exitg6;
  int32_T i6;
  real_T indexOfLastValidPulseTime_data[1];
  int32_T lastValidPulseEndTime;
  boolean_T exitg5;
  real_T b_ii_data[1];
  int32_T ii_size[1];
  boolean_T exitg4;
  emxArray_real_T *b_pulseStartTime;
  emxArray_real_T *c_pulseEndTime;
  int32_T qq;
  emxArray_real_T *amp;
  emxArray_real_T *y;
  emxArray_real_T *b_amp;
  emxArray_real_T *peak;
  boolean_T exitg1;
  boolean_T guard1 = FALSE;
  boolean_T n_too_large;
  real_T ampSumTimeWeighted;
  real_T ampSum;
  boolean_T exitg3;
  boolean_T exitg2;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   Peak_Discriminator.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       July 30, 2012 */
  /*   */
  /*  Desc:       Detects and time stamps peaks in correlation data */
  /*  */
  /*  Inputs:     peak:  indices of the signal that was above the threshold */
  /*              rxSigUnitRepCorr: Correlated data */
  /*              filterStyle: analog filter simulates legacy ATR filtering */
  /*                digital filter uses correlation function */
  /*                auxilliary filter uses DJ Tang's algorithm */
  /*              samplesPerLoop: number of samples per frame of data */
  /*              nextFrameInd: timestamp from the overlap region from the last */
  /*                frame's indsCol */
  /*  */
  /*  Outputs:    indsCol: matrix of peak indices for correlation values  */
  /*                exceeding the threshold */
  /*              nextFrameInd: timestamp from the overlap region to be applied */
  /*                to the next frame's indsCol */
  /*  */
  /*  Revisions:   */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  initialize timestamp indexes for platform */
  for (i = 0; i < 20; i++) {
    indsCol[i] = -1.0;
  }

  b_emxInit_boolean_T(&timestampStartIndex, 2);

  /*  place previous frame's timestamp values in this frame's timestamp indice  */
  /*  matrix */
  indsCol[0] = nextFrameInd->data[nextFrameInd->size[1] - 1];

  /*  coder required (end) */
  /*  adjust timestampStartIndice used in the for loop at end of function */
  i4 = timestampStartIndex->size[0] * timestampStartIndex->size[1];
  timestampStartIndex->size[0] = 1;
  timestampStartIndex->size[1] = nextFrameInd->size[1];
  emxEnsureCapacity((emxArray__common *)timestampStartIndex, i4, (int32_T)sizeof
                    (boolean_T));
  idx = nextFrameInd->size[0] * nextFrameInd->size[1];
  for (i4 = 0; i4 < idx; i4++) {
    timestampStartIndex->data[i4] = (nextFrameInd->data[i4] > 0.0);
  }

  /*  initialize next frame timestamp indices */
  *b_nextFrameInd = -1.0;

  /*  Find the beginning and end of each thresholded pulse for each channel */
  /*  Store these beginning and end times in pulseStartTime and pulseEndTime. */
  /*  For a given pulseStartTime its indice will match the */
  /*  indice of the corresponding pulseEndTime. */
  /*  Use the pulseStartTime and pulseEndTime matrices to look for  */
  /*  maximums in ampRxSigRepCorr and set the timestamp.  Alternatively, more  */
  /*  complex calculations can be used to determine the timestamp by entering */
  /*  new code into the Auxiliary function */
  /*  */
  /*  Special bookkeeping needs to be considered in this section.  To assist with  */
  /*  bookkeeping operations, overlap is employed to capture pulses that pass  */
  /*  through a frame boundary. Overlap is simply extra samples beyond the */
  /*  nominal frame size.  These extra samples will be processed twice.  Once at */
  /*  the tail of the present frame.  And again at the head of the next frame. */
  /*  */
  /*  A discussion of bookkeeping methods follows: */
  /*  */
  /*    1.  If a single pulseStartTime is found at the end of a frame (at an  */
  /*    indice just less than samplesPerLoop) the corresponding pulseEndTime  */
  /*    will be in the overlap region.  No other pulseStartTime will be used  */
  /*    for timestamp calcuations for that channel. */
  /*    2.  The overlap region should be a length that is larger than the */
  /*    longest expected pulse when considering spreading at longer ranges.  In */
  /*    ICEX11 simulations, this time was assumed to be less than 100ms and so */
  /*    the overlap was left at 100ms.  That way, a pulse that exceeds the */
  /*    overlap region is noise and can (hopefully) be disregarded in */
  /*    downstream processing.   */
  /*    3.  If a pulse is already high at the beginning of the frame */
  /*    (in other words: peak(1,:)==1) then the timestamp for that pulse will */
  /*    have been recorded in the previous frame.  Therefore, the corresponding */
  /*    pulseEndTime will be dropped.   */
  /*  loop through the channels */
  /*  a rising edge in peak results in a '1' at that indice in  */
  /*  pulseStartTime  */
  idx = peak_size[0];
  ii_data[0] = peak_size[0];
  for (i4 = 0; i4 < idx; i4++) {
    b_peak_data[i4] = peak_data[i4];
  }

  emxInit_real_T(&r0, 2);
  diff(b_peak_data, ii_data, r0);
  i = r0->size[0];
  x_size_idx_1 = r0->size[1];
  idx = r0->size[0] * r0->size[1];
  for (i4 = 0; i4 < idx; i4++) {
    x_data[i4] = (r0->data[i4] > 0.0);
  }

  emxInit_int32_T(&ii, 1);
  i *= x_size_idx_1;
  idx = 0;
  i4 = ii->size[0];
  ii->size[0] = i;
  emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
  x_size_idx_1 = 1;
  exitg8 = FALSE;
  while ((exitg8 == FALSE) && (x_size_idx_1 <= i)) {
    guard3 = FALSE;
    if (x_data[x_size_idx_1 - 1]) {
      idx++;
      ii->data[idx - 1] = x_size_idx_1;
      if (idx >= i) {
        exitg8 = TRUE;
      } else {
        guard3 = TRUE;
      }
    } else {
      guard3 = TRUE;
    }

    if (guard3 == TRUE) {
      x_size_idx_1++;
    }
  }

  if (i == 1) {
    if (idx == 0) {
      i4 = ii->size[0];
      ii->size[0] = 0;
      emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
    }
  } else {
    if (1 > idx) {
      idx = 0;
    }

    emxInit_int32_T(&b_ii, 1);
    i4 = b_ii->size[0];
    b_ii->size[0] = idx;
    emxEnsureCapacity((emxArray__common *)b_ii, i4, (int32_T)sizeof(int32_T));
    for (i4 = 0; i4 < idx; i4++) {
      b_ii->data[i4] = ii->data[i4];
    }

    i4 = ii->size[0];
    ii->size[0] = b_ii->size[0];
    emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
    idx = b_ii->size[0];
    for (i4 = 0; i4 < idx; i4++) {
      ii->data[i4] = b_ii->data[i4];
    }

    emxFree_int32_T(&b_ii);
  }

  b_emxInit_real_T(&pulseStartTime, 1);
  i4 = pulseStartTime->size[0];
  pulseStartTime->size[0] = ii->size[0];
  emxEnsureCapacity((emxArray__common *)pulseStartTime, i4, (int32_T)sizeof
                    (real_T));
  idx = ii->size[0];
  for (i4 = 0; i4 < idx; i4++) {
    pulseStartTime->data[i4] = ii->data[i4];
  }

  /*  a falling edge in peak results in a '-1' at that indice in */
  /*  pulseEndTime */
  idx = peak_size[0];
  b_peak_size[0] = peak_size[0];
  for (i4 = 0; i4 < idx; i4++) {
    b_peak_data[i4] = peak_data[i4];
  }

  diff(b_peak_data, b_peak_size, r0);
  i = r0->size[0];
  x_size_idx_1 = r0->size[1];
  idx = r0->size[0] * r0->size[1];
  for (i4 = 0; i4 < idx; i4++) {
    x_data[i4] = (r0->data[i4] < 0.0);
  }

  emxFree_real_T(&r0);
  i *= x_size_idx_1;
  idx = 0;
  i4 = ii->size[0];
  ii->size[0] = i;
  emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
  x_size_idx_1 = 1;
  exitg7 = FALSE;
  while ((exitg7 == FALSE) && (x_size_idx_1 <= i)) {
    guard2 = FALSE;
    if (x_data[x_size_idx_1 - 1]) {
      idx++;
      ii->data[idx - 1] = x_size_idx_1;
      if (idx >= i) {
        exitg7 = TRUE;
      } else {
        guard2 = TRUE;
      }
    } else {
      guard2 = TRUE;
    }

    if (guard2 == TRUE) {
      x_size_idx_1++;
    }
  }

  if (i == 1) {
    if (idx == 0) {
      i4 = ii->size[0];
      ii->size[0] = 0;
      emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
    }
  } else {
    if (1 > idx) {
      idx = 0;
    }

    emxInit_int32_T(&c_ii, 1);
    i4 = c_ii->size[0];
    c_ii->size[0] = idx;
    emxEnsureCapacity((emxArray__common *)c_ii, i4, (int32_T)sizeof(int32_T));
    for (i4 = 0; i4 < idx; i4++) {
      c_ii->data[i4] = ii->data[i4];
    }

    i4 = ii->size[0];
    ii->size[0] = c_ii->size[0];
    emxEnsureCapacity((emxArray__common *)ii, i4, (int32_T)sizeof(int32_T));
    idx = c_ii->size[0];
    for (i4 = 0; i4 < idx; i4++) {
      ii->data[i4] = c_ii->data[i4];
    }

    emxFree_int32_T(&c_ii);
  }

  b_emxInit_real_T(&pulseEndTime, 1);
  i4 = pulseEndTime->size[0];
  pulseEndTime->size[0] = ii->size[0];
  emxEnsureCapacity((emxArray__common *)pulseEndTime, i4, (int32_T)sizeof(real_T));
  idx = ii->size[0];
  for (i4 = 0; i4 < idx; i4++) {
    pulseEndTime->data[i4] = ii->data[i4];
  }

  emxFree_int32_T(&ii);

  /*  This conditional tree pre-formats the indice arrays for unusual noise */
  /*  and frame boundary scenarios */
  /*  Details in SML notebook 10/2/2012 */
  if (!(pulseStartTime->size[0] == 0)) {
    if (pulseEndTime->size[0] == 0) {
      i4 = pulseEndTime->size[0];
      pulseEndTime->size[0] = 1;
      emxEnsureCapacity((emxArray__common *)pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      pulseEndTime->data[0] = peak_size[0];
    } else if (pulseEndTime->data[0] < pulseStartTime->data[0]) {
      if (2 > pulseEndTime->size[0]) {
        i4 = 0;
        i5 = 0;
      } else {
        i4 = 1;
        i5 = pulseEndTime->size[0];
      }

      b_emxInit_real_T(&b_pulseEndTime, 1);
      i = b_pulseEndTime->size[0];
      b_pulseEndTime->size[0] = i5 - i4;
      emxEnsureCapacity((emxArray__common *)b_pulseEndTime, i, (int32_T)sizeof
                        (real_T));
      idx = i5 - i4;
      for (i5 = 0; i5 < idx; i5++) {
        b_pulseEndTime->data[i5] = pulseEndTime->data[i4 + i5];
      }

      i4 = pulseEndTime->size[0];
      pulseEndTime->size[0] = b_pulseEndTime->size[0];
      emxEnsureCapacity((emxArray__common *)pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      idx = b_pulseEndTime->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        pulseEndTime->data[i4] = b_pulseEndTime->data[i4];
      }

      emxFree_real_T(&b_pulseEndTime);
      if (pulseEndTime->size[0] < pulseStartTime->size[0]) {
        pulseEndTime->data[pulseEndTime->size[0]] = peak_size[0];
      } else {
        /*  for clarity */
      }
    } else if (pulseEndTime->size[0] < pulseStartTime->size[0]) {
      pulseEndTime->data[1] = peak_size[0];
    } else {
      /*  for clarity */
    }
  } else {
    if (!(pulseEndTime->size[0] == 0)) {
      if (1 > pulseEndTime->size[0] - 1) {
        idx = -1;
      } else {
        idx = pulseEndTime->size[0] - 2;
      }

      b_emxInit_real_T(&b_pulseEndTime, 1);
      i4 = b_pulseEndTime->size[0];
      b_pulseEndTime->size[0] = idx + 1;
      emxEnsureCapacity((emxArray__common *)b_pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      for (i4 = 0; i4 <= idx; i4++) {
        b_pulseEndTime->data[i4] = pulseEndTime->data[i4];
      }

      i4 = pulseEndTime->size[0];
      pulseEndTime->size[0] = b_pulseEndTime->size[0];
      emxEnsureCapacity((emxArray__common *)pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      idx = b_pulseEndTime->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        pulseEndTime->data[i4] = b_pulseEndTime->data[i4];
      }

      emxFree_real_T(&b_pulseEndTime);
    }
  }

  emxInit_boolean_T(&x, 1);

  /*  This conditional tree formats the indice arrays for frame overflow */
  /*  behavior */
  /*  Details in SML notebook 10/2/2012 */
  i4 = x->size[0];
  x->size[0] = pulseStartTime->size[0];
  emxEnsureCapacity((emxArray__common *)x, i4, (int32_T)sizeof(boolean_T));
  idx = pulseStartTime->size[0];
  for (i4 = 0; i4 < idx; i4++) {
    x->data[i4] = (pulseStartTime->data[i4] <= samplesPerLoop);
  }

  i = x->size[0];
  if (1 <= i) {
    k = 1;
  } else {
    k = i;
  }

  idx = 0;
  x_size_idx_1 = 1;
  exitg6 = FALSE;
  while ((exitg6 == FALSE) && (x_size_idx_1 <= x->size[0])) {
    if (x->data[x_size_idx_1 - 1]) {
      idx = 1;
      exitg6 = TRUE;
    } else {
      x_size_idx_1++;
    }
  }

  if (k == 1) {
    if (idx == 0) {
      k = 0;
    }
  } else {
    if (1 > idx) {
      i6 = -1;
    } else {
      i6 = 0;
    }

    k = i6 + 1;
  }

  if (k == 0) {
    k = 1;
    indexOfLastValidPulseTime_data[0] = 0.0;

    /*  skip subsequent for loop */
    /*  next line satisfies matlab coder requirements */
    lastValidPulseEndTime = 0;

    /*  shouldn't need this value */
    /*  determine index of last valid end time if there were pulses extending */
    /*  into the overlap region */
  } else {
    /*  truncate arrays to values in the nominal frame of data */
    i4 = x->size[0];
    x->size[0] = pulseStartTime->size[0];
    emxEnsureCapacity((emxArray__common *)x, i4, (int32_T)sizeof(boolean_T));
    idx = pulseStartTime->size[0];
    for (i4 = 0; i4 < idx; i4++) {
      x->data[i4] = (pulseStartTime->data[i4] > samplesPerLoop);
    }

    i = x->size[0];
    if (1 <= i) {
      k = 1;
    } else {
      k = i;
    }

    idx = 0;
    x_size_idx_1 = 1;
    exitg5 = FALSE;
    while ((exitg5 == FALSE) && (x_size_idx_1 <= x->size[0])) {
      if (x->data[x_size_idx_1 - 1]) {
        idx = 1;
        ii_data[0] = x_size_idx_1;
        exitg5 = TRUE;
      } else {
        x_size_idx_1++;
      }
    }

    if (k == 1) {
      if (idx == 0) {
        k = 0;
      }
    } else {
      if (1 > idx) {
        idx = -1;
      } else {
        idx = 0;
      }

      i4 = 0;
      while (i4 <= idx) {
        b_peak_size[0] = ii_data[0];
        i4 = 1;
      }

      k = idx + 1;
      idx++;
      i4 = 0;
      while (i4 <= idx - 1) {
        ii_data[0] = b_peak_size[0];
        i4 = 1;
      }
    }

    ii_size[0] = k;
    for (i4 = 0; i4 < k; i4++) {
      b_ii_data[i4] = ii_data[i4];
    }

    if (any(b_ii_data, ii_size)) {
      i4 = x->size[0];
      x->size[0] = pulseStartTime->size[0];
      emxEnsureCapacity((emxArray__common *)x, i4, (int32_T)sizeof(boolean_T));
      idx = pulseStartTime->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        x->data[i4] = (pulseStartTime->data[i4] > samplesPerLoop);
      }

      i = x->size[0];
      if (1 <= i) {
        k = 1;
      } else {
        k = i;
      }

      idx = 0;
      x_size_idx_1 = 1;
      exitg4 = FALSE;
      while ((exitg4 == FALSE) && (x_size_idx_1 <= x->size[0])) {
        if (x->data[x_size_idx_1 - 1]) {
          idx = 1;
          ii_data[0] = x_size_idx_1;
          exitg4 = TRUE;
        } else {
          x_size_idx_1++;
        }
      }

      if (k == 1) {
        if (idx == 0) {
          k = 0;
        }
      } else {
        if (1 > idx) {
          idx = -1;
        } else {
          idx = 0;
        }

        i4 = 0;
        while (i4 <= idx) {
          b_peak_size[0] = ii_data[0];
          i4 = 1;
        }

        k = idx + 1;
        idx++;
        i4 = 0;
        while (i4 <= idx - 1) {
          ii_data[0] = b_peak_size[0];
          i4 = 1;
        }
      }

      for (i4 = 0; i4 < k; i4++) {
        indexOfLastValidPulseTime_data[i4] = (real_T)ii_data[i4] - 1.0;
      }

      /*  truncate pulseStartTime to contain only start times before  */
      /*  the overlap */
      if (1.0 > indexOfLastValidPulseTime_data[k - 1]) {
        idx = 0;
      } else {
        idx = (int32_T)indexOfLastValidPulseTime_data[k - 1];
      }

      b_emxInit_real_T(&b_pulseStartTime, 1);
      i4 = b_pulseStartTime->size[0];
      b_pulseStartTime->size[0] = idx;
      emxEnsureCapacity((emxArray__common *)b_pulseStartTime, i4, (int32_T)
                        sizeof(real_T));
      for (i4 = 0; i4 < idx; i4++) {
        b_pulseStartTime->data[i4] = pulseStartTime->data[i4];
      }

      i4 = pulseStartTime->size[0];
      pulseStartTime->size[0] = b_pulseStartTime->size[0];
      emxEnsureCapacity((emxArray__common *)pulseStartTime, i4, (int32_T)sizeof
                        (real_T));
      idx = b_pulseStartTime->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        pulseStartTime->data[i4] = b_pulseStartTime->data[i4];
      }

      emxFree_real_T(&b_pulseStartTime);

      /*  truncate pulseEndTime to line up with start times before the  */
      /*  overlap */
      /*  */
      /*  if the peak ran through the end of the overlap, produce a */
      /*  synthetic (and wrong) pulseEndTime representing the end of the */
      /*  last pulse.  This will allow the array to match up. */
      if ((int32_T)indexOfLastValidPulseTime_data[k - 1] > pulseEndTime->size[0])
      {
        pulseEndTime->data[(int32_T)indexOfLastValidPulseTime_data[k - 1] - 1] =
          ampRxSigRepCorr->size[0];
      }

      if (1.0 > indexOfLastValidPulseTime_data[k - 1]) {
        idx = 0;
      } else {
        idx = (int32_T)indexOfLastValidPulseTime_data[k - 1];
      }

      b_emxInit_real_T(&c_pulseEndTime, 1);
      i4 = c_pulseEndTime->size[0];
      c_pulseEndTime->size[0] = idx;
      emxEnsureCapacity((emxArray__common *)c_pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      for (i4 = 0; i4 < idx; i4++) {
        c_pulseEndTime->data[i4] = pulseEndTime->data[i4];
      }

      i4 = pulseEndTime->size[0];
      pulseEndTime->size[0] = c_pulseEndTime->size[0];
      emxEnsureCapacity((emxArray__common *)pulseEndTime, i4, (int32_T)sizeof
                        (real_T));
      idx = c_pulseEndTime->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        pulseEndTime->data[i4] = c_pulseEndTime->data[i4];
      }

      emxFree_real_T(&c_pulseEndTime);
      lastValidPulseEndTime = (int32_T)pulseEndTime->data[(int32_T)
        indexOfLastValidPulseTime_data[k - 1] - 1];
    } else {
      /*  all pulseStartTime values were contained within samplesPerLoop */
      k = 1;
      indexOfLastValidPulseTime_data[0] = pulseStartTime->size[0];
      lastValidPulseEndTime = (int32_T)pulseEndTime->data[pulseEndTime->size[0]
        - 1];
    }
  }

  emxFree_boolean_T(&x);

  /*  debug output - remove after sufficient testing */
  /*      if length(pulseStartTime) ~= length(pulseEndTime) */
  /*          save pulseStartErr.mat ampRxSigRepCorr peak pulseStartTime pulseEndTime  */
  /*          str=sprintf(... */
  /*              'Mismatched Arrays. Channel:%d, Start: %d, %d, End: %d, %d', ... */
  /*              pp, length(pulseStartTime), pulseStartTime(end), ... */
  /*              length(pulseEndTime), pulseEndTime(end)); */
  /*          disp(str); */
  /*      end */
  /*  use array of pulse starts and ends to discover the regions where */
  /*  local maxima occur, then detect those maxima */
  qq = 0;
  b_emxInit_real_T(&amp, 1);
  emxInit_real_T(&y, 2);
  b_emxInit_real_T(&b_amp, 1);
  b_emxInit_real_T(&peak, 1);
  exitg1 = FALSE;
  while ((exitg1 == FALSE) && (qq <= (int32_T)indexOfLastValidPulseTime_data[k -
          1] - 1)) {
    guard1 = FALSE;
    switch ((int32_T)filterStyle) {
     case 2:
      /*  DJ's Auxiliary Algorithm */
      if (1 > lastValidPulseEndTime) {
        idx = 0;
      } else {
        idx = lastValidPulseEndTime;
      }

      i4 = peak->size[0];
      peak->size[0] = idx;
      emxEnsureCapacity((emxArray__common *)peak, i4, (int32_T)sizeof(real_T));
      for (i4 = 0; i4 < idx; i4++) {
        peak->data[i4] = (real_T)peak_data[i4] * ampRxSigRepCorr->data[i4];
      }

      power(peak, amp);
      float_colon_length(lastValidPulseEndTime, &x_size_idx_1, &ampSum,
                         &ampSumTimeWeighted, &n_too_large);
      i4 = y->size[0] * y->size[1];
      y->size[0] = 1;
      y->size[1] = x_size_idx_1;
      emxEnsureCapacity((emxArray__common *)y, i4, (int32_T)sizeof(real_T));
      if (x_size_idx_1 > 0) {
        y->data[0] = ampSum;
        if (x_size_idx_1 > 1) {
          y->data[x_size_idx_1 - 1] = ampSumTimeWeighted;
          i4 = x_size_idx_1 - 1;
          i = i4 / 2;
          for (k = 1; k < i; k++) {
            y->data[k] = ampSum + (real_T)k;
            y->data[(x_size_idx_1 - k) - 1] = ampSumTimeWeighted - (real_T)k;
          }

          if (i << 1 == x_size_idx_1 - 1) {
            y->data[i] = (ampSum + ampSumTimeWeighted) / 2.0;
          } else {
            y->data[i] = ampSum + (real_T)i;
            y->data[i + 1] = ampSumTimeWeighted - (real_T)i;
          }
        }
      }

      ampSum = sum(amp);
      i4 = b_amp->size[0];
      b_amp->size[0] = amp->size[0];
      emxEnsureCapacity((emxArray__common *)b_amp, i4, (int32_T)sizeof(real_T));
      idx = amp->size[0];
      for (i4 = 0; i4 < idx; i4++) {
        b_amp->data[i4] = amp->data[i4] * (y->data[i4] / samplesPerLoop);
      }

      ampSumTimeWeighted = sum(b_amp);
      ampSum = rt_roundd_snf(samplesPerLoop * ampSumTimeWeighted / ampSum);
      if (ampSum <= samplesPerLoop) {
        indsCol[qq + timestampStartIndex->data[0]] = ampSum;
      } else {
        *b_nextFrameInd = ampSum - samplesPerLoop;
      }

      exitg1 = TRUE;
      break;

     case 1:
      /*  Conventional Replication Correlation */
      if (pulseStartTime->data[qq] <= samplesPerLoop) {
        /*  pulseEndTime is before the final indice of this frame */
        if (qq + 1 <= pulseEndTime->size[0]) {
          if (pulseStartTime->data[qq] > pulseEndTime->data[qq]) {
            i4 = 1;
            i5 = 1;
          } else {
            i4 = (int32_T)pulseStartTime->data[qq];
            i5 = (int32_T)pulseEndTime->data[qq] + 1;
          }

          i = 1;
          ampSum = ampRxSigRepCorr->data[i4 - 1];
          x_size_idx_1 = 1;
          if (i5 - i4 > 1) {
            if (rtIsNaN(ampRxSigRepCorr->data[i4 - 1])) {
              idx = 2;
              exitg3 = FALSE;
              while ((exitg3 == FALSE) && (idx <= i5 - i4)) {
                i = idx;
                if (!rtIsNaN(ampRxSigRepCorr->data[(i4 + idx) - 2])) {
                  ampSum = ampRxSigRepCorr->data[(i4 + idx) - 2];
                  x_size_idx_1 = idx;
                  exitg3 = TRUE;
                } else {
                  idx++;
                }
              }
            }

            if (i < i5 - i4) {
              for (idx = i - 1; idx + 2 <= i5 - i4; idx++) {
                if (ampRxSigRepCorr->data[i4 + idx] > ampSum) {
                  ampSum = ampRxSigRepCorr->data[i4 + idx];
                  x_size_idx_1 = idx + 2;
                }
              }
            }
          }

          if ((real_T)x_size_idx_1 + pulseStartTime->data[qq] < samplesPerLoop)
          {
            indsCol[qq + timestampStartIndex->data[0]] = (real_T)x_size_idx_1 +
              pulseStartTime->data[qq];
          } else {
            /*  assumes only one pulse in the overlap region */
            *b_nextFrameInd = ((real_T)x_size_idx_1 + pulseStartTime->data[qq])
              - samplesPerLoop;
          }

          /*  noise or waveform distortion causes pulseEndTime to  */
          /*  exceed overlap.  debug msg above should appear on */
          /*  matlab command window output */
        } else {
          if ((int32_T)pulseStartTime->data[qq] > ampRxSigRepCorr->size[0]) {
            i4 = 1;
            i5 = 1;
          } else {
            i4 = (int32_T)pulseStartTime->data[qq];
            i5 = ampRxSigRepCorr->size[0] + 1;
          }

          i = 1;
          ampSum = ampRxSigRepCorr->data[i4 - 1];
          x_size_idx_1 = 1;
          if (i5 - i4 > 1) {
            if (rtIsNaN(ampRxSigRepCorr->data[i4 - 1])) {
              idx = 2;
              exitg2 = FALSE;
              while ((exitg2 == FALSE) && (idx <= i5 - i4)) {
                i = idx;
                if (!rtIsNaN(ampRxSigRepCorr->data[(i4 + idx) - 2])) {
                  ampSum = ampRxSigRepCorr->data[(i4 + idx) - 2];
                  x_size_idx_1 = idx;
                  exitg2 = TRUE;
                } else {
                  idx++;
                }
              }
            }

            if (i < i5 - i4) {
              for (idx = i - 1; idx + 2 <= i5 - i4; idx++) {
                if (ampRxSigRepCorr->data[i4 + idx] > ampSum) {
                  ampSum = ampRxSigRepCorr->data[i4 + idx];
                  x_size_idx_1 = idx + 2;
                }
              }
            }
          }

          if ((real_T)x_size_idx_1 + pulseStartTime->data[qq] < samplesPerLoop)
          {
            indsCol[qq + timestampStartIndex->data[0]] = (real_T)x_size_idx_1 +
              pulseStartTime->data[qq];
          } else {
            /*  assumes only one pulse in the overlap region */
            *b_nextFrameInd = ((real_T)x_size_idx_1 + pulseStartTime->data[qq])
              - samplesPerLoop;
          }
        }
      }

      guard1 = TRUE;
      break;

     default:
      /*  analog style filter grabs leading edge of pulse */
      if (pulseStartTime->data[qq] < samplesPerLoop) {
        indsCol[qq + timestampStartIndex->data[0]] = pulseStartTime->data[qq];
      } else {
        /*  assumes only one pulse in the overlap region */
        *b_nextFrameInd = pulseStartTime->data[qq] - samplesPerLoop;
      }

      guard1 = TRUE;
      break;
    }

    if (guard1 == TRUE) {
      qq++;
    }
  }

  emxFree_real_T(&peak);
  emxFree_real_T(&b_amp);
  emxFree_real_T(&y);
  emxFree_real_T(&amp);
  emxFree_real_T(&pulseEndTime);
  emxFree_real_T(&pulseStartTime);
  emxFree_boolean_T(&timestampStartIndex);
}

/* End of code generation (Peak_Discriminator.c) */
