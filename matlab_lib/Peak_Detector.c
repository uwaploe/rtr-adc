/*
 * Peak_Detector.c
 *
 * Code generation for function 'Peak_Detector'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Peak_Detector.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "Peak_Discriminator.h"
#include "var.h"
#include "abs.h"

/* Function Declarations */
static int32_T compute_nones(const boolean_T x_data[59950], int32_T n);
static void eml_li_find(const boolean_T x_data[59950], const int32_T x_size[1],
  emxArray_int32_T *y);

/* Function Definitions */
static int32_T compute_nones(const boolean_T x_data[59950], int32_T n)
{
  int32_T k;
  int32_T i;
  k = 0;
  for (i = 1; i <= n; i++) {
    if (x_data[i - 1]) {
      k++;
    }
  }

  return k;
}

static void eml_li_find(const boolean_T x_data[59950], const int32_T x_size[1],
  emxArray_int32_T *y)
{
  int32_T k;
  int32_T i;
  k = compute_nones(x_data, x_size[0]);
  i = y->size[0];
  y->size[0] = k;
  emxEnsureCapacity((emxArray__common *)y, i, (int32_T)sizeof(int32_T));
  k = 0;
  for (i = 1; i <= x_size[0]; i++) {
    if (x_data[i - 1]) {
      y->data[k] = i;
      k++;
    }
  }
}

void Peak_Detector(const emxArray_real_T *rxSigUnitRepCorrA, const
                   emxArray_real_T *rxSigUnitRepCorrB, real_T varA[3], real_T
                   varB[3], const real_T varLimitA[4], const real_T varLimitB[4],
                   const emxArray_real_T *b_nextFrameIndA, const emxArray_real_T
                   *b_nextFrameIndB, real_T filterStyle, real_T
                   thresholdAPercent, real_T thresholdBPercent, real_T
                   thresholdSmooth, real_T samplesPerLoop, real_T indsColA[20],
                   real_T indsColB[20], real_T *thresholdA, real_T *thresholdB,
                   emxArray_real_T *ampRxSigRepCorrA, emxArray_real_T
                   *ampRxSigRepCorrB, real_T *c_nextFrameIndA, real_T
                   *c_nextFrameIndB, real_T *varACurrent, real_T *varBCurrent,
                   real_T peakAOut_data[2], int32_T peakAOut_size[1], real_T
                   peakBOut_data[2], int32_T peakBOut_size[1])
{
  real_T y;
  int32_T ixstart;
  real_T b_y;
  real_T b_varA[2];
  int32_T i11;
  uint16_T unnamed_idx_0;
  int32_T peakA_size[1];
  static boolean_T peakA_data[59950];
  int32_T peakB_size[1];
  static boolean_T peakB_data[59950];
  emxArray_int32_T *r8;
  real_T mtmp;
  int32_T ix;
  int32_T exitg12;
  int32_T exitg11;
  int32_T exitg10;
  real_T b_mtmp;
  int32_T exitg9;
  int32_T exitg8;
  static boolean_T ampRxSigRepCorrA_data[59950];
  int32_T ampRxSigRepCorrA_size[1];
  boolean_T x_data[59950];
  uint16_T ii_data[2];
  boolean_T exitg7;
  boolean_T guard2 = FALSE;
  uint16_T b_ii_data[2];
  int32_T exitg6;
  int32_T exitg5;
  int32_T exitg4;
  int32_T exitg3;
  int32_T exitg2;
  int32_T ampRxSigRepCorrB_size[1];
  boolean_T exitg1;
  boolean_T guard1 = FALSE;
  emxArray_real_T *r9;
  emxArray_real_T *r10;
  int32_T loop_ub;
  boolean_T b_peakB_data;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   Peak_Detector.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       July 6, 2012 */
  /*   */
  /*  Desc:       Detects and time stamps peaks in correlation data */
  /*                The input data (a segment of acoustic correlation filter  */
  /*                output) is processed through a threshold detector.  All */
  /*                values exceeding the threshold are processed through the */
  /*                discriminator.  The discriminator chooses one or more */
  /*                timestamps from the thresholded data.  The user chooses the */
  /*                discriminator style in the real-time GUI. */
  /*  */
  /*  Inputs:     rxSigUnitRepCorrA: Correlated platform data */
  /*              rxSigUnitRepCorrB: Correlated unit data */
  /*              varA: Platform Variance history for last varBufferLen frames */
  /*              varB: Unit Variance history for last varBufferLen frames */
  /*              filterStyle: analog filter simulates legacy ATR filtering */
  /*                digital filter uses correlation function */
  /*              thresholdAPercent: user's gui input of plat threshold level  */
  /*                as a percent between 0-100 */
  /*              thresholdBPercent: user's gui input of unit threshold level  */
  /*                as a percent between 0-100 */
  /*              thresholdSmooth: removes small gaps between blocks of */
  /*                high threshold output */
  /*              samplesPerLoop: number of samples per frame of data */
  /*              nextFrameIndA: indice of pulse that occurred in overlap */
  /*                region during last frame */
  /*              nextFrameIndB: indice of pulse that occurred in overlap */
  /*                region during last frame */
  /*  */
  /*  Outputs:    indsColA: matrix of peak indices for correlation values  */
  /*                exceeding the threshold */
  /*              indsColB: matrix of peak indices for correlation values  */
  /*                exceeding the threshold */
  /*              varA: Plat Variance history for last varBufferLen frames */
  /*              varB: Unit Variance history for last varBufferLen frames */
  /*              thresholdA:  used for display purposes elsewhere in program */
  /*              thresholdB:  used for display purposes elsewhere in program */
  /*              nextFrameIndA: indice of pulse that occurs in overlap region */
  /*                during this frame */
  /*              nextFrameIndB: indice of pulse that occurs in overlap region */
  /*                during this frame */
  /*  */
  /*  Revisions:  7/30/12 - added overlap functionality */
  /*              2/21/14 - added scalar variance output, varDebugA, varDebugB */
  /*              2/23/14 - removed varDebugA and varDebugA */
  /*              3/5/14  - added peakB output, removed ampRxSigRepCorrA/B */
  /*                            output */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  remove negative values from correlation data */
  b_abs(rxSigUnitRepCorrA, ampRxSigRepCorrA);
  b_abs(rxSigUnitRepCorrB, ampRxSigRepCorrB);

  /*  mean of the variance for past frames (last three seconds worth)  */
  /*  to be used for detection */
  y = varA[0];
  for (ixstart = 0; ixstart < 2; ixstart++) {
    y += varA[ixstart + 1];
  }

  b_y = varB[0];
  for (ixstart = 0; ixstart < 2; ixstart++) {
    b_y += varB[ixstart + 1];
  }

  /*  A frame of Gaussian noise has a low variance.  */
  /*  A frame containing a pulse has a high variance.   */
  /*  Detect a frame by keeping track of variance measurements in the last */
  /*  three frames and when the newest exceeds the three frame average, */
  /*  assign a detection to the frame. */
  /*  Shift the oldest measurement out and append the newest for each channel */
  for (i11 = 0; i11 < 2; i11++) {
    b_varA[i11] = varA[1 + i11];
  }

  for (i11 = 0; i11 < 2; i11++) {
    varA[i11] = b_varA[i11];
  }

  /*  shift out oldest variance */
  varA[2] = var(ampRxSigRepCorrA);

  /*  shift in newest variance */
  for (i11 = 0; i11 < 2; i11++) {
    b_varA[i11] = varB[1 + i11];
  }

  for (i11 = 0; i11 < 2; i11++) {
    varB[i11] = b_varA[i11];
  }

  /*  shift out oldest variance */
  varB[2] = var(ampRxSigRepCorrB);

  /*  shift in newest variance */
  /*  create array to store max indices for multiplication through */
  /*  ampRxSigRepCorrA & ampRxSigRepCorrB.   */
  /*  Logical required for Matlab Coder */
  unnamed_idx_0 = (uint16_T)ampRxSigRepCorrA->size[0];
  peakA_size[0] = unnamed_idx_0;
  ixstart = unnamed_idx_0;
  for (i11 = 0; i11 < ixstart; i11++) {
    peakA_data[i11] = FALSE;
  }

  unnamed_idx_0 = (uint16_T)ampRxSigRepCorrB->size[0];
  peakB_size[0] = unnamed_idx_0;
  ixstart = unnamed_idx_0;
  for (i11 = 0; i11 < ixstart; i11++) {
    peakB_data[i11] = FALSE;
  }

  /*  ampRxSigRepCorrA/B is scanned to find elements greater than  */
  /*  max - threshold_percent*(mean)  */
  /*  for platform */
  /*  var of current frame > var past frames */
  *varACurrent = y / 3.0 * varLimitA[0];
  emxInit_int32_T(&r8, 1);
  if (varA[2] > *varACurrent) {
    ixstart = 1;
    mtmp = ampRxSigRepCorrA->data[0];
    i11 = ampRxSigRepCorrA->size[0];
    if (i11 > 1) {
      if (rtIsNaN(ampRxSigRepCorrA->data[0])) {
        ix = 2;
        do {
          exitg12 = 0;
          i11 = ampRxSigRepCorrA->size[0];
          if (ix <= i11) {
            ixstart = ix;
            if (!rtIsNaN(ampRxSigRepCorrA->data[ix - 1])) {
              mtmp = ampRxSigRepCorrA->data[ix - 1];
              exitg12 = 1;
            } else {
              ix++;
            }
          } else {
            exitg12 = 1;
          }
        } while (exitg12 == 0);
      }

      i11 = ampRxSigRepCorrA->size[0];
      if (ixstart < i11) {
        do {
          exitg11 = 0;
          i11 = ampRxSigRepCorrA->size[0];
          if (ixstart + 1 <= i11) {
            if (ampRxSigRepCorrA->data[ixstart] > mtmp) {
              mtmp = ampRxSigRepCorrA->data[ixstart];
            }

            ixstart++;
          } else {
            exitg11 = 1;
          }
        } while (exitg11 == 0);
      }
    }

    y = ampRxSigRepCorrA->data[0];
    ixstart = 2;
    do {
      exitg10 = 0;
      i11 = ampRxSigRepCorrA->size[0];
      if (ixstart <= i11) {
        y += ampRxSigRepCorrA->data[ixstart - 1];
        ixstart++;
      } else {
        exitg10 = 1;
      }
    } while (exitg10 == 0);

    /*  thresholdA is updated for display purposes and to calculate */
    /*  threshold */
    ixstart = 1;
    b_mtmp = ampRxSigRepCorrA->data[0];
    i11 = ampRxSigRepCorrA->size[0];
    if (i11 > 1) {
      if (rtIsNaN(ampRxSigRepCorrA->data[0])) {
        ix = 2;
        do {
          exitg9 = 0;
          i11 = ampRxSigRepCorrA->size[0];
          if (ix <= i11) {
            ixstart = ix;
            if (!rtIsNaN(ampRxSigRepCorrA->data[ix - 1])) {
              b_mtmp = ampRxSigRepCorrA->data[ix - 1];
              exitg9 = 1;
            } else {
              ix++;
            }
          } else {
            exitg9 = 1;
          }
        } while (exitg9 == 0);
      }

      i11 = ampRxSigRepCorrA->size[0];
      if (ixstart < i11) {
        do {
          exitg8 = 0;
          i11 = ampRxSigRepCorrA->size[0];
          if (ixstart + 1 <= i11) {
            if (ampRxSigRepCorrA->data[ixstart] > b_mtmp) {
              b_mtmp = ampRxSigRepCorrA->data[ixstart];
            }

            ixstart++;
          } else {
            exitg8 = 1;
          }
        } while (exitg8 == 0);
      }
    }

    i11 = ampRxSigRepCorrA->size[0];
    *thresholdA = b_mtmp - (1.0 - thresholdAPercent / 100.0) * (mtmp - y /
      (real_T)i11);

    /*  all the indices where the correlation exceeds the threshold */
    ixstart = ampRxSigRepCorrA->size[0];
    ampRxSigRepCorrA_size[0] = ixstart;
    for (i11 = 0; i11 < ixstart; i11++) {
      ampRxSigRepCorrA_data[i11] = (ampRxSigRepCorrA->data[i11] > *thresholdA);
    }

    eml_li_find(ampRxSigRepCorrA_data, ampRxSigRepCorrA_size, r8);
    ixstart = r8->size[0];
    for (i11 = 0; i11 < ixstart; i11++) {
      peakA_data[r8->data[i11] - 1] = TRUE;
    }
  } else {
    *thresholdA = 100.0;

    /*  large number for display purposes */
  }

  ixstart = peakA_size[0];
  for (i11 = 0; i11 < ixstart; i11++) {
    x_data[i11] = peakA_data[i11];
  }

  ixstart = 0;
  ix = 1;
  exitg7 = FALSE;
  while ((exitg7 == FALSE) && (ix <= peakA_size[0])) {
    guard2 = FALSE;
    if (x_data[ix - 1]) {
      ixstart++;
      ii_data[ixstart - 1] = (uint16_T)ix;
      if (ixstart >= 2) {
        exitg7 = TRUE;
      } else {
        guard2 = TRUE;
      }
    } else {
      guard2 = TRUE;
    }

    if (guard2 == TRUE) {
      ix++;
    }
  }

  if (1 > ixstart) {
    ixstart = 0;
  }

  for (i11 = 0; i11 < ixstart; i11++) {
    b_ii_data[i11] = ii_data[i11];
  }

  for (i11 = 0; i11 < ixstart; i11++) {
    ii_data[i11] = b_ii_data[i11];
  }

  peakAOut_size[0] = ixstart;
  for (i11 = 0; i11 < ixstart; i11++) {
    peakAOut_data[i11] = ii_data[i11];
  }

  /*  for unit */
  /*  var of current frame > var past frames */
  *varBCurrent = b_y / 3.0 * varLimitB[0];
  if (varB[2] > *varBCurrent) {
    ixstart = 1;
    mtmp = ampRxSigRepCorrB->data[0];
    i11 = ampRxSigRepCorrB->size[0];
    if (i11 > 1) {
      if (rtIsNaN(ampRxSigRepCorrB->data[0])) {
        ix = 2;
        do {
          exitg6 = 0;
          i11 = ampRxSigRepCorrB->size[0];
          if (ix <= i11) {
            ixstart = ix;
            if (!rtIsNaN(ampRxSigRepCorrB->data[ix - 1])) {
              mtmp = ampRxSigRepCorrB->data[ix - 1];
              exitg6 = 1;
            } else {
              ix++;
            }
          } else {
            exitg6 = 1;
          }
        } while (exitg6 == 0);
      }

      i11 = ampRxSigRepCorrB->size[0];
      if (ixstart < i11) {
        do {
          exitg5 = 0;
          i11 = ampRxSigRepCorrB->size[0];
          if (ixstart + 1 <= i11) {
            if (ampRxSigRepCorrB->data[ixstart] > mtmp) {
              mtmp = ampRxSigRepCorrB->data[ixstart];
            }

            ixstart++;
          } else {
            exitg5 = 1;
          }
        } while (exitg5 == 0);
      }
    }

    y = ampRxSigRepCorrB->data[0];
    ixstart = 2;
    do {
      exitg4 = 0;
      i11 = ampRxSigRepCorrB->size[0];
      if (ixstart <= i11) {
        y += ampRxSigRepCorrB->data[ixstart - 1];
        ixstart++;
      } else {
        exitg4 = 1;
      }
    } while (exitg4 == 0);

    /*  thresholdB is updated for display purposes and to calculate */
    /*  threshold */
    ixstart = 1;
    b_mtmp = ampRxSigRepCorrB->data[0];
    i11 = ampRxSigRepCorrB->size[0];
    if (i11 > 1) {
      if (rtIsNaN(ampRxSigRepCorrB->data[0])) {
        ix = 2;
        do {
          exitg3 = 0;
          i11 = ampRxSigRepCorrB->size[0];
          if (ix <= i11) {
            ixstart = ix;
            if (!rtIsNaN(ampRxSigRepCorrB->data[ix - 1])) {
              b_mtmp = ampRxSigRepCorrB->data[ix - 1];
              exitg3 = 1;
            } else {
              ix++;
            }
          } else {
            exitg3 = 1;
          }
        } while (exitg3 == 0);
      }

      i11 = ampRxSigRepCorrB->size[0];
      if (ixstart < i11) {
        do {
          exitg2 = 0;
          i11 = ampRxSigRepCorrB->size[0];
          if (ixstart + 1 <= i11) {
            if (ampRxSigRepCorrB->data[ixstart] > b_mtmp) {
              b_mtmp = ampRxSigRepCorrB->data[ixstart];
            }

            ixstart++;
          } else {
            exitg2 = 1;
          }
        } while (exitg2 == 0);
      }
    }

    i11 = ampRxSigRepCorrB->size[0];
    *thresholdB = b_mtmp - (1.0 - thresholdBPercent / 100.0) * (mtmp - y /
      (real_T)i11);

    /*  all the indices where the correlation exceeds the threshold */
    ixstart = ampRxSigRepCorrB->size[0];
    ampRxSigRepCorrB_size[0] = ixstart;
    for (i11 = 0; i11 < ixstart; i11++) {
      ampRxSigRepCorrA_data[i11] = (ampRxSigRepCorrB->data[i11] > *thresholdB);
    }

    eml_li_find(ampRxSigRepCorrA_data, ampRxSigRepCorrB_size, r8);
    ixstart = r8->size[0];
    for (i11 = 0; i11 < ixstart; i11++) {
      peakB_data[r8->data[i11] - 1] = TRUE;
    }
  } else {
    *thresholdB = 100.0;

    /*  large number for display purposes */
  }

  emxFree_int32_T(&r8);
  ixstart = unnamed_idx_0;
  for (i11 = 0; i11 < ixstart; i11++) {
    x_data[i11] = peakB_data[i11];
  }

  ixstart = 0;
  ix = 1;
  exitg1 = FALSE;
  while ((exitg1 == FALSE) && (ix <= unnamed_idx_0)) {
    guard1 = FALSE;
    if (x_data[ix - 1]) {
      ixstart++;
      ii_data[ixstart - 1] = (uint16_T)ix;
      if (ixstart >= 2) {
        exitg1 = TRUE;
      } else {
        guard1 = TRUE;
      }
    } else {
      guard1 = TRUE;
    }

    if (guard1 == TRUE) {
      ix++;
    }
  }

  if (1 > ixstart) {
    ixstart = 0;
  }

  for (i11 = 0; i11 < ixstart; i11++) {
    b_ii_data[i11] = ii_data[i11];
  }

  for (i11 = 0; i11 < ixstart; i11++) {
    ii_data[i11] = b_ii_data[i11];
  }

  peakBOut_size[0] = ixstart;
  for (i11 = 0; i11 < ixstart; i11++) {
    peakBOut_data[i11] = ii_data[i11];
  }

  /*  This is a low pass filter for the detected peaks */
  /*  Basically, right shift the peak matrix by thresholdSmooth and logical */
  /*  OR peak with itself.   */
  /*  The time durations of the gaps that will be 'smeared over' will be  */
  /*  equal to theshold_smooth/fs2.  This was more important before hilbert  */
  /*  processing was added to the replica correlation data, however, it  */
  /*  could still be useful around those areas where the peaks remain jagged. */
  /*  If this function is removed, there would be more timestamps per frame.  */
  ix = 0;
  b_emxInit_real_T(&r9, 1);
  b_emxInit_real_T(&r10, 1);
  while (ix <= (int32_T)thresholdSmooth - 1) {
    y = (real_T)peakA_size[0] - (1.0 + (real_T)ix);
    if (1.0 > y) {
      ixstart = 0;
    } else {
      ixstart = (int32_T)y;
    }

    i11 = r9->size[0];
    r9->size[0] = (int32_T)(1.0 + (real_T)ix) + ixstart;
    emxEnsureCapacity((emxArray__common *)r9, i11, (int32_T)sizeof(real_T));
    loop_ub = (int32_T)(1.0 + (real_T)ix);
    for (i11 = 0; i11 < loop_ub; i11++) {
      r9->data[i11] = 0.0;
    }

    for (i11 = 0; i11 < ixstart; i11++) {
      r9->data[i11 + (int32_T)(1.0 + (real_T)ix)] = peakA_data[i11];
    }

    ixstart = peakA_size[0];
    for (i11 = 0; i11 < ixstart; i11++) {
      b_peakB_data = (peakA_data[i11] || (r9->data[i11] != 0.0));
      peakA_data[i11] = b_peakB_data;
    }

    y = (real_T)unnamed_idx_0 - (1.0 + (real_T)ix);
    if (1.0 > y) {
      ixstart = 0;
    } else {
      ixstart = (int32_T)y;
    }

    i11 = r10->size[0];
    r10->size[0] = (int32_T)(1.0 + (real_T)ix) + ixstart;
    emxEnsureCapacity((emxArray__common *)r10, i11, (int32_T)sizeof(real_T));
    loop_ub = (int32_T)(1.0 + (real_T)ix);
    for (i11 = 0; i11 < loop_ub; i11++) {
      r10->data[i11] = 0.0;
    }

    for (i11 = 0; i11 < ixstart; i11++) {
      r10->data[i11 + (int32_T)(1.0 + (real_T)ix)] = peakB_data[i11];
    }

    ixstart = unnamed_idx_0;
    for (i11 = 0; i11 < ixstart; i11++) {
      b_peakB_data = (peakB_data[i11] || (r10->data[i11] != 0.0));
      peakB_data[i11] = b_peakB_data;
    }

    ix++;
  }

  emxFree_real_T(&r10);
  emxFree_real_T(&r9);

  /*  Discriminators */
  /*  find timestamps for platform */
  Peak_Discriminator(peakA_data, peakA_size, ampRxSigRepCorrA, b_nextFrameIndA,
                     filterStyle, samplesPerLoop, indsColA, c_nextFrameIndA);

  /*  find timestamps for unit */
  Peak_Discriminator(peakB_data, peakB_size, ampRxSigRepCorrB, b_nextFrameIndB,
                     filterStyle, samplesPerLoop, indsColB, c_nextFrameIndB);
}

/* End of code generation (Peak_Detector.c) */
