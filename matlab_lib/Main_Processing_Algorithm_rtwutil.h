/*
 * Main_Processing_Algorithm_rtwutil.h
 *
 * Code generation for function 'Main_Processing_Algorithm_rtwutil'
 *
 *
 */

#ifndef __MAIN_PROCESSING_ALGORITHM_RTWUTIL_H__
#define __MAIN_PROCESSING_ALGORITHM_RTWUTIL_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern real_T rt_roundd_snf(real_T u);
#endif
/* End of code generation (Main_Processing_Algorithm_rtwutil.h) */
