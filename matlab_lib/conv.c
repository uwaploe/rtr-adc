/*
 * conv.c
 *
 * Code generation for function 'conv'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "conv.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void b_conv(const emxArray_real_T *A, const real_T B[105], emxArray_real_T *C)
{
  uint16_T unnamed_idx_0;
  int32_T jC;
  int32_T jA2;
  real_T s;
  int32_T k;
  unnamed_idx_0 = (uint16_T)(A->size[0] + 104);
  jC = C->size[0];
  C->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)C, jC, (int32_T)sizeof(real_T));
  for (jC = 1; jC <= A->size[0] + 104; jC++) {
    if (A->size[0] < jC) {
      jA2 = A->size[0];
    } else {
      jA2 = jC;
    }

    s = 0.0;
    if (105 < jC + 1) {
      k = jC - 104;
    } else {
      k = 1;
    }

    while (k <= jA2) {
      s += A->data[k - 1] * B[jC - k];
      k++;
    }

    C->data[jC - 1] = s;
  }
}

void conv(const emxArray_real_T *A, const real_T B[300], emxArray_real_T *C)
{
  uint16_T unnamed_idx_0;
  int32_T jC;
  int32_T jA2;
  real_T s;
  int32_T k;
  unnamed_idx_0 = (uint16_T)(A->size[0] + 299);
  jC = C->size[0];
  C->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)C, jC, (int32_T)sizeof(real_T));
  for (jC = 1; jC <= A->size[0] + 299; jC++) {
    if (A->size[0] < jC) {
      jA2 = A->size[0];
    } else {
      jA2 = jC;
    }

    s = 0.0;
    if (300 < jC + 1) {
      k = jC - 299;
    } else {
      k = 1;
    }

    while (k <= jA2) {
      s += A->data[k - 1] * B[jC - k];
      k++;
    }

    C->data[jC - 1] = s;
  }
}

/* End of code generation (conv.c) */
