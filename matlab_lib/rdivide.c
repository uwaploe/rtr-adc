/*
 * rdivide.c
 *
 * Code generation for function 'rdivide'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "rdivide.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void rdivide(const emxArray_real_T *x, real_T y, emxArray_real_T *z)
{
  int32_T i4;
  int32_T loop_ub;
  i4 = z->size[0] * z->size[1];
  z->size[0] = 1;
  z->size[1] = x->size[1];
  emxEnsureCapacity((emxArray__common *)z, i4, (int32_T)sizeof(real_T));
  loop_ub = x->size[0] * x->size[1];
  for (i4 = 0; i4 < loop_ub; i4++) {
    z->data[i4] = x->data[i4] / y;
  }
}

/* End of code generation (rdivide.c) */
