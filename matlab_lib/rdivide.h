/*
 * rdivide.h
 *
 * Code generation for function 'rdivide'
 *
 *
 */

#ifndef __RDIVIDE_H__
#define __RDIVIDE_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void rdivide(const emxArray_real_T *x, real_T y, emxArray_real_T *z);
#endif
/* End of code generation (rdivide.h) */
