/*
 * any.c
 *
 * Code generation for function 'any'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "any.h"

/* Function Definitions */
boolean_T any(const real_T x_data[1], const int32_T x_size[1])
{
  boolean_T y;
  int32_T ix;
  boolean_T exitg1;
  boolean_T b0;
  y = FALSE;
  ix = 1;
  exitg1 = FALSE;
  while ((exitg1 == FALSE) && (ix <= x_size[0])) {
    if ((x_data[0] == 0.0) || rtIsNaN(x_data[0])) {
      b0 = TRUE;
    } else {
      b0 = FALSE;
    }

    if (!b0) {
      y = TRUE;
      exitg1 = TRUE;
    } else {
      ix = 2;
    }
  }

  return y;
}

boolean_T b_any(const boolean_T x_data[1], const int32_T x_size[1])
{
  boolean_T y;
  int32_T ix;
  boolean_T exitg1;
  y = FALSE;
  ix = 1;
  exitg1 = FALSE;
  while ((exitg1 == FALSE) && (ix <= x_size[0])) {
    if (!(x_data[0] == 0)) {
      y = TRUE;
      exitg1 = TRUE;
    } else {
      ix = 2;
    }
  }

  return y;
}

/* End of code generation (any.c) */
