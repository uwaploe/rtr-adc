/*
 * round.c
 *
 * Code generation for function 'round'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "round.h"
#include "Main_Processing_Algorithm_rtwutil.h"

/* Function Definitions */
void b_round(real_T x_data[1])
{
  x_data[0] = rt_roundd_snf(x_data[0]);
}

/* End of code generation (round.c) */
