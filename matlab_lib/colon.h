/*
 * colon.h
 *
 * Code generation for function 'colon'
 *
 *
 */

#ifndef __COLON_H__
#define __COLON_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void float_colon_length(real_T b, int32_T *n, real_T *anew, real_T *bnew, boolean_T *n_too_large);
#endif
/* End of code generation (colon.h) */
