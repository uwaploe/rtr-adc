/*
 * round.h
 *
 * Code generation for function 'round'
 *
 *
 */

#ifndef __ROUND_H__
#define __ROUND_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void b_round(real_T x_data[1]);
#endif
/* End of code generation (round.h) */
