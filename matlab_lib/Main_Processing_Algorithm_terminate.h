/*
 * Main_Processing_Algorithm_terminate.h
 *
 * Code generation for function 'Main_Processing_Algorithm_terminate'
 *
 *
 */

#ifndef __MAIN_PROCESSING_ALGORITHM_TERMINATE_H__
#define __MAIN_PROCESSING_ALGORITHM_TERMINATE_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void Main_Processing_Algorithm_terminate(void);
#endif
/* End of code generation (Main_Processing_Algorithm_terminate.h) */
