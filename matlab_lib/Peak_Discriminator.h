/*
 * Peak_Discriminator.h
 *
 * Code generation for function 'Peak_Discriminator'
 *
 *
 */

#ifndef __PEAK_DISCRIMINATOR_H__
#define __PEAK_DISCRIMINATOR_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void Peak_Discriminator(const boolean_T peak_data[59950], const int32_T peak_size[1], const emxArray_real_T *ampRxSigRepCorr, const emxArray_real_T *nextFrameInd, real_T filterStyle, real_T samplesPerLoop, real_T indsCol[20], real_T *b_nextFrameInd);
#endif
/* End of code generation (Peak_Discriminator.h) */
