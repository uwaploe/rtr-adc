/*
 * var.h
 *
 * Code generation for function 'var'
 *
 *
 */

#ifndef __VAR_H__
#define __VAR_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern real_T var(const emxArray_real_T *x);
#endif
/* End of code generation (var.h) */
