/*
 * diff.c
 *
 * Code generation for function 'diff'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "diff.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void diff(const boolean_T x_data[59950], const int32_T x_size[1],
          emxArray_real_T *y)
{
  emxArray_real_T *b_y1;
  int32_T tmp1;
  int32_T ixLead;
  int32_T iyLead;
  int32_T work_data_idx_0;
  int32_T m;
  int32_T tmp2;
  b_emxInit_real_T(&b_y1, 1);
  tmp1 = b_y1->size[0];
  b_y1->size[0] = x_size[0] - 1;
  emxEnsureCapacity((emxArray__common *)b_y1, tmp1, (int32_T)sizeof(real_T));
  ixLead = 1;
  iyLead = 0;
  work_data_idx_0 = x_data[0];
  for (m = 2; m <= x_size[0]; m++) {
    tmp2 = work_data_idx_0;
    work_data_idx_0 = x_data[ixLead];
    tmp1 = x_data[ixLead] - tmp2;
    ixLead++;
    b_y1->data[iyLead] = tmp1;
    iyLead++;
  }

  tmp2 = b_y1->size[0];
  tmp1 = y->size[0] * y->size[1];
  y->size[0] = tmp2;
  emxEnsureCapacity((emxArray__common *)y, tmp1, (int32_T)sizeof(real_T));
  tmp1 = y->size[0] * y->size[1];
  y->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)y, tmp1, (int32_T)sizeof(real_T));
  tmp2 = b_y1->size[0];
  for (tmp1 = 0; tmp1 < tmp2; tmp1++) {
    y->data[tmp1] = b_y1->data[tmp1];
  }

  emxFree_real_T(&b_y1);
}

/* End of code generation (diff.c) */
