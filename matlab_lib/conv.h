/*
 * conv.h
 *
 * Code generation for function 'conv'
 *
 *
 */

#ifndef __CONV_H__
#define __CONV_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void b_conv(const emxArray_real_T *A, const real_T B[105], emxArray_real_T *C);
extern void conv(const emxArray_real_T *A, const real_T B[300], emxArray_real_T *C);
#endif
/* End of code generation (conv.h) */
