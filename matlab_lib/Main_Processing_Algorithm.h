/*
 * Main_Processing_Algorithm.h
 *
 * Code generation for function 'Main_Processing_Algorithm'
 *
 *
 */

#ifndef __MAIN_PROCESSING_ALGORITHM_H__
#define __MAIN_PROCESSING_ALGORITHM_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void Main_Processing_Algorithm(const real_T data[30000], const real_T dataSec[30000], const real_T PLATFORM_FILTER_COEFFS[102], const real_T UNIT_FILTER_COEFFS[102], real_T OVERLAP, const real_T PLATFORM_REP_WAVEFORM[300], const real_T UNIT_REP_WAVEFORM[105], real_T filterStyle, const real_T varLimitA[4], const real_T varLimitB[4], real_T VARBUFFERLEN, real_T thresholdAPercent, real_T thresholdBPercent, real_T PEAK_SMOOTH, real_T prevPulseMatrix_data[68], int32_T prevPulseMatrix_size[2], real_T MAX_DETECTS, real_T SIGNAL_CHANNELS, real_T POSSIBLE_QUADS, real_T VEHICLE_TYPES, real_T BLANKING_TIME, real_T SAMPLES_PER_FRAME, real_T FS, real_T varA[3], real_T varB[3], emxArray_real_T *rxSigUnitA, emxArray_real_T *rxSigUnitB, real_T timeOffset_data[1], int32_T timeOffset_size[1], real_T *varACurrent, real_T *varBCurrent, real_T *thresholdA, real_T *thresholdB, real_T firstFrameIndA[2], real_T firstFrameIndB[2], real_T peakAOut_data[2], int32_T peakAOut_size[1], real_T peakBOut_data[2], int32_T peakBOut_size[1]);
#endif
/* End of code generation (Main_Processing_Algorithm.h) */
