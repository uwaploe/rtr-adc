/*
 * power.h
 *
 * Code generation for function 'power'
 *
 *
 */

#ifndef __POWER_H__
#define __POWER_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void power(const emxArray_real_T *a, emxArray_real_T *y);
#endif
/* End of code generation (power.h) */
