/*
 * mean.c
 *
 * Code generation for function 'mean'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "mean.h"

/* Function Definitions */
real_T b_mean(const emxArray_real_T *x)
{
  real_T y;
  int32_T k;
  y = x->data[0];
  for (k = 2; k <= x->size[0]; k++) {
    y += x->data[k - 1];
  }

  y /= (real_T)x->size[0];
  return y;
}

real_T mean(const real_T x[3])
{
  real_T y;
  int32_T k;
  y = x[0];
  for (k = 0; k < 2; k++) {
    y += x[k + 1];
  }

  y /= 3.0;
  return y;
}

/* End of code generation (mean.c) */
