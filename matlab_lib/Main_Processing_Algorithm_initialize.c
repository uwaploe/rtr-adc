/*
 * Main_Processing_Algorithm_initialize.c
 *
 * Code generation for function 'Main_Processing_Algorithm_initialize'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Main_Processing_Algorithm_initialize.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "Main_Processing_Algorithm_data.h"

/* Function Definitions */
void Main_Processing_Algorithm_initialize(void)
{
  rt_InitInfAndNaN(8U);
  emxInit_real_T(&pulseFrame, 2);
  emxInit_real_T(&nextFrameIndA, 2);
  emxInit_real_T(&nextFrameIndB, 2);
  nextFrameIndB_not_empty = FALSE;
  nextFrameIndA_not_empty = FALSE;
  pulseFrame_not_empty = FALSE;
  lastT0 = -1.0;
  memset(&oldData[0], 0, 30000U * sizeof(real_T));
}

/* End of code generation (Main_Processing_Algorithm_initialize.c) */
