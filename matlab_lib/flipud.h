/*
 * flipud.h
 *
 * Code generation for function 'flipud'
 *
 *
 */

#ifndef __FLIPUD_H__
#define __FLIPUD_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void flipud(real_T x[300]);
#endif
/* End of code generation (flipud.h) */
