/*
 * filter.h
 *
 * Code generation for function 'filter'
 *
 *
 */

#ifndef __FILTER_H__
#define __FILTER_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern void filter(const real_T b[102], const emxArray_real_T *x, emxArray_real_T *y);
#endif
/* End of code generation (filter.h) */
