/*
 * power.c
 *
 * Code generation for function 'power'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "power.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Declarations */
static void eml_scalexp_alloc(const emxArray_real_T *varargin_1, emxArray_real_T
  *z);

/* Function Definitions */
static void eml_scalexp_alloc(const emxArray_real_T *varargin_1, emxArray_real_T
  *z)
{
  uint16_T unnamed_idx_0;
  int32_T i8;
  unnamed_idx_0 = (uint16_T)varargin_1->size[0];
  i8 = z->size[0];
  z->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)z, i8, (int32_T)sizeof(real_T));
}

void power(const emxArray_real_T *a, emxArray_real_T *y)
{
  int32_T i7;
  int32_T k;
  eml_scalexp_alloc(a, y);
  i7 = y->size[0];
  for (k = 0; k < i7; k++) {
    y->data[k] = a->data[k] * a->data[k];
  }
}

/* End of code generation (power.c) */
