/*
 * var.c
 *
 * Code generation for function 'var'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "var.h"

/* Function Definitions */
real_T var(const emxArray_real_T *x)
{
  real_T y;
  int32_T ix;
  real_T xbar;
  int32_T k;
  real_T r;
  ix = 0;
  xbar = x->data[0];
  for (k = 0; k <= x->size[0] - 2; k++) {
    ix++;
    xbar += x->data[ix];
  }

  xbar /= (real_T)x->size[0];
  ix = 0;
  r = x->data[0] - xbar;
  y = r * r;
  for (k = 0; k <= x->size[0] - 2; k++) {
    ix++;
    r = x->data[ix] - xbar;
    y += r * r;
  }

  y /= (real_T)x->size[0] - 1.0;
  return y;
}

/* End of code generation (var.c) */
