/*
 * Calculate_Time.c
 *
 * Code generation for function 'Calculate_Time'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Calculate_Time.h"
#include "any.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "round.h"

/* Function Definitions */
void Calculate_Time(const real_T indsColA[20], const real_T indsColB[20],
                    emxArray_real_T *b_pulseFrame, const real_T
                    prevPulseMatrix_data[68], const int32_T
                    prevPulseMatrix_size[2], real_T maxDetects, const real_T
                    T0_data[1], real_T channels, real_T possibleQuads, real_T
                    vehicleTypes, real_T blanking, real_T samplesPerLoop, real_T
                    fs, emxArray_real_T *prevPulseMatrix)
{
  int32_T i12;
  int32_T nrows;
  real_T columns;
  int32_T idx;
  int32_T i13;
  real_T x;
  uint32_T prevJMax;
  int32_T I;
  emxArray_boolean_T *b_x;
  int32_T i;
  uint32_T prevJ;
  int32_T pulsePointer;
  int32_T exitg7;
  int32_T k;
  boolean_T exitg10;
  int32_T i14;
  int32_T ii_data[1];
  boolean_T exitg9;
  int32_T b_ii_data[1];
  int32_T j;
  int32_T prevJRaw_data[1];
  boolean_T exitg8;
  int32_T JRaw_data[1];
  real_T pulseTimeStampReal_data[1];
  boolean_T exitg6;
  boolean_T c_ii_data[1];
  int32_T ii_size[1];
  int32_T exitg2;
  boolean_T exitg5;
  int32_T i15;
  boolean_T exitg4;
  boolean_T exitg3;
  boolean_T exitg1;
  int32_T b_ii_size[1];
  emxArray_real_T *r11;
  emxArray_real_T *b_idx;
  emxArray_boolean_T *b;
  emxArray_real_T *b_prevPulseMatrix;
  i12 = prevPulseMatrix->size[0] * prevPulseMatrix->size[1];
  prevPulseMatrix->size[0] = prevPulseMatrix_size[0];
  prevPulseMatrix->size[1] = 2;
  emxEnsureCapacity((emxArray__common *)prevPulseMatrix, i12, (int32_T)sizeof
                    (real_T));
  nrows = prevPulseMatrix_size[0] * prevPulseMatrix_size[1];
  for (i12 = 0; i12 < nrows; i12++) {
    prevPulseMatrix->data[i12] = prevPulseMatrix_data[i12];
  }

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   Calculate_Time.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       June 28, 2012 */
  /*   */
  /*  Desc:       Assembles timestamps into matrix corrected for the 1PPS pulse */
  /*                    Method:  */
  /*                    - find start of detection for a single channel */
  /*                    - subtract a fixed blanking pulse */
  /*                    - then look for the next detection in the channel */
  /*                    - when the channel has been fully checked, move to the  */
  /*                        next channel */
  /*  */
  /*  Inputs:     indsColA:         matrix of peak indices for correlation values  */
  /*                                    exceeding the threshold */
  /*              indsColB:         matrix of peak indices for correlation values  */
  /*              pulseFrame:       matrix of 1PPS referenced timestamps */
  /*                                    from the previous frame */
  /*              prevPulseMatrix:  matrix from last frame */
  /*              maxDetects:       maximum number of timestamps detections  */
  /*              T0:               1PPS timestamp indice */
  /*              channels:    Number of channels in acoustic data stream */
  /*                                    (1PPS channel + number of hydrophone channels) */
  /*              blanking:         seconds to ignore other pulses after a */
  /*                                    detection */
  /*              samplesPerLoop:   typically 30000 */
  /*              fs:               typically 30000 */
  /*              possibleQuads:    number of quads represented in pulseFrame */
  /*              vehicleTypes:     number of vehihles represented in */
  /*                                    pulseFrame */
  /*  */
  /*  Outputs:    pulseFrame:       matrix of 1PPS referenced timestamps from */
  /*                                    this second */
  /*              prevPulseMatrix:  timestamp display/rs232 output data */
  /*               */
  /*  Revisions: */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  columns = channels * possibleQuads * vehicleTypes;

  /*  A pulse frame contains the time stamps for each detection.  The */
  /*  blanking rate will restrict the number of detections per frame. */
  /*  With a blanking rate of 0.03 seconds, the pulseFrame would be 34 rows by */
  /*  16 hydrophones.  As it is discovered how few pulses there are in a frame, */
  /*  the number of rows will be reduced. */
  if (!((b_pulseFrame->size[0] == 0) || (b_pulseFrame->size[1] == 0))) {
    i12 = prevPulseMatrix->size[0] * prevPulseMatrix->size[1];
    prevPulseMatrix->size[0] = b_pulseFrame->size[0];
    prevPulseMatrix->size[1] = b_pulseFrame->size[1];
    emxEnsureCapacity((emxArray__common *)prevPulseMatrix, i12, (int32_T)sizeof
                      (real_T));
    nrows = b_pulseFrame->size[1];
    for (i12 = 0; i12 < nrows; i12++) {
      idx = b_pulseFrame->size[0];
      for (i13 = 0; i13 < idx; i13++) {
        prevPulseMatrix->data[i13 + prevPulseMatrix->size[0] * i12] =
          b_pulseFrame->data[i13 + b_pulseFrame->size[0] * i12];
      }
    }

    /*  save last pulseFrame for bookkeeping */
  }

  x = ceil(1.0 / blanking);
  i12 = b_pulseFrame->size[0] * b_pulseFrame->size[1];
  b_pulseFrame->size[0] = (int32_T)x;
  b_pulseFrame->size[1] = (int32_T)columns;
  emxEnsureCapacity((emxArray__common *)b_pulseFrame, i12, (int32_T)sizeof
                    (real_T));
  nrows = (int32_T)columns;
  for (i12 = 0; i12 < nrows; i12++) {
    idx = (int32_T)x;
    for (i13 = 0; i13 < idx; i13++) {
      b_pulseFrame->data[i13 + b_pulseFrame->size[0] * i12] = -1.0;
    }
  }

  /*  initialize pulse timing matrix */
  prevJMax = 1U;

  /*  FREQA FREQA FREQA  */
  /*  find array index of pulses for each channel for Freq A */
  I = 0;
  emxInit_boolean_T(&b_x, 1);
  while (I <= (int32_T)channels - 1) {
    /*  initialize max number of rows in a frame */
    i = 1;

    /*  frame row */
    prevJ = 1U;

    /*  previous frame row */
    pulsePointer = 0;

    /*  index the row of the max values array */
    /*  gather timestamp indexes until you reach the end of indsColA for each */
    /*  channel */
    do {
      exitg7 = 0;
      if (pulsePointer + 1 > 20) {
        i12 = 0;
        i13 = 0;
      } else {
        i12 = pulsePointer;
        i13 = 20;
      }

      idx = i13 - i12;
      if (1 <= idx) {
        k = 1;
      } else {
        k = idx;
      }

      idx = 0;
      nrows = 1;
      exitg10 = FALSE;
      while ((exitg10 == FALSE) && (nrows <= i13 - i12)) {
        if (indsColA[(i12 + nrows) - 1] != 0.0) {
          idx = 1;
          exitg10 = TRUE;
        } else {
          nrows++;
        }
      }

      if (k == 1) {
        if (idx == 0) {
          k = 0;
        }
      } else {
        if (1 > idx) {
          i14 = -1;
        } else {
          i14 = 0;
        }

        k = i14 + 1;
      }

      if ((!(k == 0)) && ((i < maxDetects + 1.0) || (prevJ < maxDetects + 1.0)))
      {
        nrows = prevPulseMatrix->size[0];
        i12 = b_x->size[0];
        b_x->size[0] = nrows;
        emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof
                          (boolean_T));
        for (i12 = 0; i12 < nrows; i12++) {
          b_x->data[i12] = (prevPulseMatrix->data[i12] == -1.0);
        }

        idx = b_x->size[0];
        if (1 <= idx) {
          k = 1;
        } else {
          k = idx;
        }

        idx = 0;
        nrows = 1;
        exitg9 = FALSE;
        while ((exitg9 == FALSE) && (nrows <= b_x->size[0])) {
          if (b_x->data[nrows - 1]) {
            idx = 1;
            ii_data[0] = nrows;
            exitg9 = TRUE;
          } else {
            nrows++;
          }
        }

        if (k == 1) {
          if (idx == 0) {
            k = 0;
          }
        } else {
          if (1 > idx) {
            nrows = -1;
          } else {
            nrows = 0;
          }

          i12 = 0;
          while (i12 <= nrows) {
            b_ii_data[0] = ii_data[0];
            i12 = 1;
          }

          k = nrows + 1;
          nrows++;
          i12 = 0;
          while (i12 <= nrows - 1) {
            ii_data[0] = b_ii_data[0];
            i12 = 1;
          }
        }

        j = k;
        for (i12 = 0; i12 < k; i12++) {
          prevJRaw_data[i12] = ii_data[i12];
        }

        prevJ = (uint32_T)(real_T)prevJRaw_data[k - 1];

        /*  matlab coder requires this formatting */
        nrows = b_pulseFrame->size[0];
        i12 = b_x->size[0];
        b_x->size[0] = nrows;
        emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof
                          (boolean_T));
        for (i12 = 0; i12 < nrows; i12++) {
          b_x->data[i12] = (b_pulseFrame->data[i12] == -1.0);
        }

        idx = b_x->size[0];
        if (1 <= idx) {
          k = 1;
        } else {
          k = idx;
        }

        idx = 0;
        nrows = 1;
        exitg8 = FALSE;
        while ((exitg8 == FALSE) && (nrows <= b_x->size[0])) {
          if (b_x->data[nrows - 1]) {
            idx = 1;
            ii_data[0] = nrows;
            exitg8 = TRUE;
          } else {
            nrows++;
          }
        }

        if (k == 1) {
          if (idx == 0) {
            k = 0;
          }
        } else {
          if (1 > idx) {
            nrows = -1;
          } else {
            nrows = 0;
          }

          i12 = 0;
          while (i12 <= nrows) {
            b_ii_data[0] = ii_data[0];
            i12 = 1;
          }

          k = nrows + 1;
          nrows++;
          i12 = 0;
          while (i12 <= nrows - 1) {
            ii_data[0] = b_ii_data[0];
            i12 = 1;
          }
        }

        for (i12 = 0; i12 < k; i12++) {
          JRaw_data[i12] = ii_data[i12];
        }

        i = JRaw_data[k - 1];
        if (indsColA[pulsePointer] != -1.0) {
          /*  if a timestamp is found  */
          /*  calculate the timestamp in 10's of microseconds */
          pulseTimeStampReal_data[0] = 100000.0 * (indsColA[pulsePointer] -
            T0_data[0]) / samplesPerLoop;
          b_round(pulseTimeStampReal_data);
          if (pulseTimeStampReal_data[0] < -1.0) {
            /*  timestamp in previous second */
            /*  if there is room for another timestamp */
            if (prevJRaw_data[j - 1] < maxDetects + 1.0) {
              prevPulseMatrix->data[prevJRaw_data[j - 1] - 1] =
                pulseTimeStampReal_data[0] + 100000.0;
              prevJ = (uint32_T)(real_T)prevJRaw_data[j - 1] + 1U;
            }
          } else {
            /*  timestamp in current second (after T0) */
            /*  if there is room for another timestamp         */
            if (JRaw_data[k - 1] < maxDetects + 1.0) {
              b_pulseFrame->data[JRaw_data[k - 1] - 1] =
                pulseTimeStampReal_data[0];
            }
          }

          /*  if the pointer is near the frame's end, break out of the while */
          /*  loop (this is the blanking pulse conditional) */
          if (indsColA[pulsePointer] + floor(fs * blanking) < samplesPerLoop) {
            pulsePointer++;
          } else {
            exitg7 = 1;
          }
        } else {
          exitg7 = 1;
        }
      } else {
        exitg7 = 1;
      }
    } while (exitg7 == 0);

    /*  if there was no pulse (in the first row), J=1 will save '-1'  */
    /*  data for all channels */
    /*  all is required for matlab coder */
    nrows = prevPulseMatrix->size[0];
    i12 = b_x->size[0];
    b_x->size[0] = nrows;
    emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof(boolean_T));
    for (i12 = 0; i12 < nrows; i12++) {
      b_x->data[i12] = (prevPulseMatrix->data[i12] == -1.0);
    }

    idx = b_x->size[0];
    if (1 <= idx) {
      k = 1;
    } else {
      k = idx;
    }

    idx = 0;
    nrows = 1;
    exitg6 = FALSE;
    while ((exitg6 == FALSE) && (nrows <= b_x->size[0])) {
      if (b_x->data[nrows - 1]) {
        idx = 1;
        ii_data[0] = nrows;
        exitg6 = TRUE;
      } else {
        nrows++;
      }
    }

    if (k == 1) {
      if (idx == 0) {
        k = 0;
      }
    } else {
      if (1 > idx) {
        nrows = -1;
      } else {
        nrows = 0;
      }

      i12 = 0;
      while (i12 <= nrows) {
        b_ii_data[0] = ii_data[0];
        i12 = 1;
      }

      k = nrows + 1;
      nrows++;
      i12 = 0;
      while (i12 <= nrows - 1) {
        ii_data[0] = b_ii_data[0];
        i12 = 1;
      }
    }

    ii_size[0] = k;
    for (i12 = 0; i12 < k; i12++) {
      c_ii_data[i12] = (ii_data[i12] == 1);
    }

    if (b_any(c_ii_data, ii_size)) {
      prevJ = 1U;
    }

    /*  create a JMax to reduce the column length of pulseFrame */
    if (prevJMax < prevJ) {
      prevJMax = prevJ;
    }

    I++;
  }

  /*  FREQB  FREQB FREQB  */
  /*  find array index of pulses for each channel for Freq B */
  for (I = 0; I < (int32_T)channels; I++) {
    /*  initialize max number of rows in a frame */
    i = 1;
    prevJ = 1U;

    /*  previous frame row */
    pulsePointer = 0;

    /*  find the array index of a pulse for the current channel in while loop */
    do {
      exitg2 = 0;
      if (pulsePointer + 1 > 20) {
        i12 = 0;
        i13 = 0;
      } else {
        i12 = pulsePointer;
        i13 = 20;
      }

      idx = i13 - i12;
      if (1 <= idx) {
        k = 1;
      } else {
        k = idx;
      }

      idx = 0;
      nrows = 1;
      exitg5 = FALSE;
      while ((exitg5 == FALSE) && (nrows <= i13 - i12)) {
        if (indsColB[(i12 + nrows) - 1] != 0.0) {
          idx = 1;
          exitg5 = TRUE;
        } else {
          nrows++;
        }
      }

      if (k == 1) {
        if (idx == 0) {
          k = 0;
        }
      } else {
        if (1 > idx) {
          i15 = -1;
        } else {
          i15 = 0;
        }

        k = i15 + 1;
      }

      if ((!(k == 0)) && ((i < maxDetects + 1.0) || (prevJ < maxDetects + 1.0)))
      {
        nrows = prevPulseMatrix->size[0];
        i12 = b_x->size[0];
        b_x->size[0] = nrows;
        emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof
                          (boolean_T));
        for (i12 = 0; i12 < nrows; i12++) {
          b_x->data[i12] = (prevPulseMatrix->data[i12 + prevPulseMatrix->size[0]
                            * ((int32_T)(1.0 + channels) - 1)] == -1.0);
        }

        idx = b_x->size[0];
        if (1 <= idx) {
          k = 1;
        } else {
          k = idx;
        }

        idx = 0;
        nrows = 1;
        exitg4 = FALSE;
        while ((exitg4 == FALSE) && (nrows <= b_x->size[0])) {
          if (b_x->data[nrows - 1]) {
            idx = 1;
            ii_data[0] = nrows;
            exitg4 = TRUE;
          } else {
            nrows++;
          }
        }

        if (k == 1) {
          if (idx == 0) {
            k = 0;
          }
        } else {
          if (1 > idx) {
            nrows = -1;
          } else {
            nrows = 0;
          }

          i12 = 0;
          while (i12 <= nrows) {
            b_ii_data[0] = ii_data[0];
            i12 = 1;
          }

          k = nrows + 1;
          nrows++;
          i12 = 0;
          while (i12 <= nrows - 1) {
            ii_data[0] = b_ii_data[0];
            i12 = 1;
          }
        }

        j = k;
        for (i12 = 0; i12 < k; i12++) {
          prevJRaw_data[i12] = ii_data[i12];
        }

        prevJ = (uint32_T)(real_T)prevJRaw_data[k - 1];
        nrows = b_pulseFrame->size[0];
        i12 = b_x->size[0];
        b_x->size[0] = nrows;
        emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof
                          (boolean_T));
        for (i12 = 0; i12 < nrows; i12++) {
          b_x->data[i12] = (b_pulseFrame->data[i12 + b_pulseFrame->size[0] *
                            ((int32_T)(1.0 + channels) - 1)] == -1.0);
        }

        idx = b_x->size[0];
        if (1 <= idx) {
          k = 1;
        } else {
          k = idx;
        }

        idx = 0;
        nrows = 1;
        exitg3 = FALSE;
        while ((exitg3 == FALSE) && (nrows <= b_x->size[0])) {
          if (b_x->data[nrows - 1]) {
            idx = 1;
            ii_data[0] = nrows;
            exitg3 = TRUE;
          } else {
            nrows++;
          }
        }

        if (k == 1) {
          if (idx == 0) {
            k = 0;
          }
        } else {
          if (1 > idx) {
            nrows = -1;
          } else {
            nrows = 0;
          }

          i12 = 0;
          while (i12 <= nrows) {
            b_ii_data[0] = ii_data[0];
            i12 = 1;
          }

          k = nrows + 1;
          nrows++;
          i12 = 0;
          while (i12 <= nrows - 1) {
            ii_data[0] = b_ii_data[0];
            i12 = 1;
          }
        }

        for (i12 = 0; i12 < k; i12++) {
          JRaw_data[i12] = ii_data[i12];
        }

        i = JRaw_data[k - 1];
        if (indsColB[pulsePointer] != -1.0) {
          /*  if a timestamp as found in the temp array */
          pulseTimeStampReal_data[0] = 100000.0 * (indsColB[pulsePointer] -
            T0_data[0]) / samplesPerLoop;
          b_round(pulseTimeStampReal_data);
          if (pulseTimeStampReal_data[0] < -1.0) {
            /*  timestamp saved to previous second */
            /*  if there is room for another timestamp */
            if (prevJRaw_data[j - 1] < maxDetects + 1.0) {
              prevPulseMatrix->data[(prevJRaw_data[j - 1] +
                prevPulseMatrix->size[0] * ((int32_T)(1.0 + channels) - 1)) - 1]
                = pulseTimeStampReal_data[0] + 100000.0;
              prevJ = (uint32_T)(real_T)prevJRaw_data[j - 1] + 1U;
            }
          } else {
            /*  timestamp saved to current second (after T0) */
            /*  if there is room for another timestamp         */
            if (JRaw_data[k - 1] < maxDetects + 1.0) {
              b_pulseFrame->data[(JRaw_data[k - 1] + b_pulseFrame->size[0] *
                                  ((int32_T)(1.0 + channels) - 1)) - 1] =
                pulseTimeStampReal_data[0];
            }
          }

          /*  if the pointer is near the frame's end, break out of the while */
          /*  loop (this is the blanking pulse conditional) */
          if (indsColB[pulsePointer] + floor(fs * blanking) < samplesPerLoop) {
            pulsePointer++;
          } else {
            exitg2 = 1;
          }
        } else {
          exitg2 = 1;
        }
      } else {
        exitg2 = 1;
      }
    } while (exitg2 == 0);

    /*  If there was no pulse the first index will be -1  */
    /*   Assigning prevJ,J=1 will save '-1' data for all channels */
    nrows = prevPulseMatrix->size[0];
    i12 = b_x->size[0];
    b_x->size[0] = nrows;
    emxEnsureCapacity((emxArray__common *)b_x, i12, (int32_T)sizeof(boolean_T));
    for (i12 = 0; i12 < nrows; i12++) {
      b_x->data[i12] = (prevPulseMatrix->data[i12 + prevPulseMatrix->size[0] *
                        ((int32_T)(1.0 + channels) - 1)] == -1.0);
    }

    idx = b_x->size[0];
    if (1 <= idx) {
      k = 1;
    } else {
      k = idx;
    }

    idx = 0;
    nrows = 1;
    exitg1 = FALSE;
    while ((exitg1 == FALSE) && (nrows <= b_x->size[0])) {
      if (b_x->data[nrows - 1]) {
        idx = 1;
        ii_data[0] = nrows;
        exitg1 = TRUE;
      } else {
        nrows++;
      }
    }

    if (k == 1) {
      if (idx == 0) {
        k = 0;
      }
    } else {
      if (1 > idx) {
        nrows = -1;
      } else {
        nrows = 0;
      }

      i12 = 0;
      while (i12 <= nrows) {
        b_ii_data[0] = ii_data[0];
        i12 = 1;
      }

      k = nrows + 1;
      nrows++;
      i12 = 0;
      while (i12 <= nrows - 1) {
        ii_data[0] = b_ii_data[0];
        i12 = 1;
      }
    }

    b_ii_size[0] = k;
    for (i12 = 0; i12 < k; i12++) {
      c_ii_data[i12] = (ii_data[i12] == 1);
    }

    if (b_any(c_ii_data, b_ii_size)) {
      prevJ = 1U;
    }

    /*  create a JMax to reduce the column length of pulseFrame  */
    if (prevJMax < prevJ) {
      prevJMax = prevJ;
    }
  }

  emxFree_boolean_T(&b_x);
  b_emxInit_real_T(&r11, 1);

  /*  the most detects for a single channel will set the number of rows in */
  /*  pulseFrame */
  i12 = prevPulseMatrix->size[0];
  i13 = r11->size[0];
  r11->size[0] = (int32_T)((real_T)i12 - ((real_T)prevJMax + 1.0)) + 1;
  emxEnsureCapacity((emxArray__common *)r11, i13, (int32_T)sizeof(real_T));
  nrows = (int32_T)((real_T)i12 - ((real_T)prevJMax + 1.0));
  for (i12 = 0; i12 <= nrows; i12++) {
    r11->data[i12] = ((real_T)prevJMax + 1.0) + (real_T)i12;
  }

  emxInit_real_T(&b_idx, 2);
  i12 = b_idx->size[0] * b_idx->size[1];
  b_idx->size[0] = 1;
  emxEnsureCapacity((emxArray__common *)b_idx, i12, (int32_T)sizeof(real_T));
  idx = r11->size[0];
  i12 = b_idx->size[0] * b_idx->size[1];
  b_idx->size[1] = idx;
  emxEnsureCapacity((emxArray__common *)b_idx, i12, (int32_T)sizeof(real_T));
  nrows = r11->size[0];
  for (i12 = 0; i12 < nrows; i12++) {
    b_idx->data[i12] = r11->data[i12];
  }

  emxFree_real_T(&r11);
  if (b_idx->size[1] == 1) {
    idx = prevPulseMatrix->size[0];
    nrows = prevPulseMatrix->size[0] - 1;
    i12 = prevPulseMatrix->size[1];
    for (j = 0; j + 1 <= i12; j++) {
      for (i = (int32_T)b_idx->data[0]; i < idx; i++) {
        prevPulseMatrix->data[(i + prevPulseMatrix->size[0] * j) - 1] =
          prevPulseMatrix->data[i + prevPulseMatrix->size[0] * j];
      }
    }
  } else {
    b_emxInit_boolean_T(&b, 2);
    i12 = b->size[0] * b->size[1];
    b->size[0] = 1;
    emxEnsureCapacity((emxArray__common *)b, i12, (int32_T)sizeof(boolean_T));
    idx = prevPulseMatrix->size[0];
    i12 = b->size[0] * b->size[1];
    b->size[1] = idx;
    emxEnsureCapacity((emxArray__common *)b, i12, (int32_T)sizeof(boolean_T));
    nrows = prevPulseMatrix->size[0];
    for (i12 = 0; i12 < nrows; i12++) {
      b->data[i12] = FALSE;
    }

    for (k = 1; k <= b_idx->size[1]; k++) {
      b->data[(int32_T)b_idx->data[k - 1] - 1] = TRUE;
    }

    idx = 0;
    for (k = 1; k <= b->size[1]; k++) {
      idx += b->data[k - 1];
    }

    nrows = prevPulseMatrix->size[0] - idx;
    i = 0;
    i12 = prevPulseMatrix->size[0];
    for (k = 1; k <= i12; k++) {
      if ((k > b->size[1]) || (!b->data[k - 1])) {
        i13 = prevPulseMatrix->size[1];
        for (j = 0; j + 1 <= i13; j++) {
          prevPulseMatrix->data[i + prevPulseMatrix->size[0] * j] =
            prevPulseMatrix->data[(k + prevPulseMatrix->size[0] * j) - 1];
        }

        i++;
      }
    }

    emxFree_boolean_T(&b);
  }

  emxFree_real_T(&b_idx);
  if (1 > nrows) {
    nrows = 0;
  }

  emxInit_real_T(&b_prevPulseMatrix, 2);
  idx = prevPulseMatrix->size[1];
  i12 = b_prevPulseMatrix->size[0] * b_prevPulseMatrix->size[1];
  b_prevPulseMatrix->size[0] = nrows;
  b_prevPulseMatrix->size[1] = idx;
  emxEnsureCapacity((emxArray__common *)b_prevPulseMatrix, i12, (int32_T)sizeof
                    (real_T));
  for (i12 = 0; i12 < idx; i12++) {
    for (i13 = 0; i13 < nrows; i13++) {
      b_prevPulseMatrix->data[i13 + b_prevPulseMatrix->size[0] * i12] =
        prevPulseMatrix->data[i13 + prevPulseMatrix->size[0] * i12];
    }
  }

  i12 = prevPulseMatrix->size[0] * prevPulseMatrix->size[1];
  prevPulseMatrix->size[0] = b_prevPulseMatrix->size[0];
  prevPulseMatrix->size[1] = b_prevPulseMatrix->size[1];
  emxEnsureCapacity((emxArray__common *)prevPulseMatrix, i12, (int32_T)sizeof
                    (real_T));
  nrows = b_prevPulseMatrix->size[1];
  for (i12 = 0; i12 < nrows; i12++) {
    idx = b_prevPulseMatrix->size[0];
    for (i13 = 0; i13 < idx; i13++) {
      prevPulseMatrix->data[i13 + prevPulseMatrix->size[0] * i12] =
        b_prevPulseMatrix->data[i13 + b_prevPulseMatrix->size[0] * i12];
    }
  }

  emxFree_real_T(&b_prevPulseMatrix);
}

/* End of code generation (Calculate_Time.c) */
