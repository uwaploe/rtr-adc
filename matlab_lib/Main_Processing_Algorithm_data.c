/*
 * Main_Processing_Algorithm_data.c
 *
 * Code generation for function 'Main_Processing_Algorithm_data'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Main_Processing_Algorithm_data.h"

/* Variable Definitions */
real_T lastT0;
emxArray_real_T *pulseFrame;
boolean_T pulseFrame_not_empty;
emxArray_real_T *nextFrameIndA;
boolean_T nextFrameIndA_not_empty;
emxArray_real_T *nextFrameIndB;
boolean_T nextFrameIndB_not_empty;
real_T oldData[30000];

/* End of code generation (Main_Processing_Algorithm_data.c) */
