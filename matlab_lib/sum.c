/*
 * sum.c
 *
 * Code generation for function 'sum'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "sum.h"

/* Function Definitions */
real_T sum(const emxArray_real_T *x)
{
  real_T y;
  int32_T k;
  if (x->size[0] == 0) {
    y = 0.0;
  } else {
    y = x->data[0];
    for (k = 2; k <= x->size[0]; k++) {
      y += x->data[k - 1];
    }
  }

  return y;
}

/* End of code generation (sum.c) */
