/*
 * T0_Detect.c
 *
 * Code generation for function 'T0_Detect'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "T0_Detect.h"
#include "Main_Processing_Algorithm_emxutil.h"

/* Function Definitions */
void T0_Detect(const real_T dataSec[30000], real_T b_lastT0, real_T fs, real_T
               T0_data[1], int32_T T0_size[1], real_T lastT0_data[1], int32_T
               lastT0_size[1])
{
  real_T y;
  int32_T i0;
  int32_T ii;
  emxArray_boolean_T *x;
  int32_T idx;
  int32_T k;
  boolean_T exitg4;
  int32_T i1;
  int32_T ii_data[1];
  boolean_T exitg3;
  int32_T b_ii_data[1];
  boolean_T exitg2;
  boolean_T p;
  boolean_T b_p;
  boolean_T exitg1;
  lastT0_size[0] = 1;
  lastT0_data[0] = b_lastT0;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  Function:   T0_Detect.m */
  /*  Programmer: Sean Lastuka */
  /*  Date:       July 25, 2012 */
  /*   */
  /*  Desc:       Detect T0 data from input file stream or DAQ stream */
  /*  */
  /*  Inputs:     dataSec:   an array containing the samples of the 1PPS signal */
  /*              lastT0:    index of rising edge of 1PPS pulse.  Necessarily,  */
  /*                            because this value is sampled, it is accurate  */
  /*                            to within 1/fs of the true rising edge. */
  /*              fs:        sample rate for this function that will also be  */
  /*                            used in downstream calculations */
  /*  Outputs:    T0:        array indice of rising edge of 1PPS signal in  */
  /*                            dataSec */
  /*              lastT0:    array indice of rising edge of 1PPS signal in last */
  /*                            frame of dataSec */
  /*  */
  /*  Revisions: */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*     %% 1PPS SIGNAL DETECT */
  /*  detect 1PPS data stream */
  /*  Save index for rising edge of 50% duty 1PPS signal. */
  /*  Did the rising edge occur in the previous frame? */
  if (dataSec[0] > 1.0) {
    /*  did the signal exceed one volt? */
    /*  Assume signal is repeating once a second.  */
    /*  Check the last half of the dataSec array to see */
    /*  if the rising edge was in the previous frame */
    y = fs / 2.0;
    if (y + 1.0 > 30000.0) {
      i0 = 1;
      ii = 1;
    } else {
      i0 = (int32_T)(y + 1.0);
      ii = 30001;
    }

    emxInit_boolean_T(&x, 1);
    idx = x->size[0];
    x->size[0] = ii - i0;
    emxEnsureCapacity((emxArray__common *)x, idx, (int32_T)sizeof(boolean_T));
    idx = ii - i0;
    for (ii = 0; ii < idx; ii++) {
      x->data[ii] = (dataSec[(i0 + ii) - 1] > 2.0);
    }

    idx = x->size[0];
    if (1 <= idx) {
      k = 1;
    } else {
      k = idx;
    }

    idx = 0;
    ii = 1;
    exitg4 = FALSE;
    while ((exitg4 == FALSE) && (ii <= x->size[0])) {
      if (x->data[ii - 1]) {
        idx = 1;
        exitg4 = TRUE;
      } else {
        ii++;
      }
    }

    if (k == 1) {
      if (idx == 0) {
        k = 0;
      }
    } else {
      if (1 > idx) {
        i1 = -1;
      } else {
        i1 = 0;
      }

      k = i1 + 1;
    }

    if (!(k == 0)) {
      y = fs / 2.0;
      if (y + 1.0 > 30000.0) {
        i0 = 1;
        ii = 1;
      } else {
        i0 = (int32_T)(y + 1.0);
        ii = 30001;
      }

      idx = x->size[0];
      x->size[0] = ii - i0;
      emxEnsureCapacity((emxArray__common *)x, idx, (int32_T)sizeof(boolean_T));
      idx = ii - i0;
      for (ii = 0; ii < idx; ii++) {
        x->data[ii] = (dataSec[(i0 + ii) - 1] > 2.0);
      }

      idx = x->size[0];
      if (1 <= idx) {
        k = 1;
      } else {
        k = idx;
      }

      idx = 0;
      ii = 1;
      exitg3 = FALSE;
      while ((exitg3 == FALSE) && (ii <= x->size[0])) {
        if (x->data[ii - 1]) {
          idx = 1;
          ii_data[0] = ii;
          exitg3 = TRUE;
        } else {
          ii++;
        }
      }

      if (k == 1) {
        if (idx == 0) {
          k = 0;
        }
      } else {
        if (1 > idx) {
          idx = -1;
        } else {
          idx = 0;
        }

        i0 = 0;
        while (i0 <= idx) {
          b_ii_data[0] = ii_data[0];
          i0 = 1;
        }

        k = idx + 1;
        idx++;
        i0 = 0;
        while (i0 <= idx - 1) {
          ii_data[0] = b_ii_data[0];
          i0 = 1;
        }
      }

      y = fs / 2.0;
      T0_size[0] = k;
      for (i0 = 0; i0 < k; i0++) {
        T0_data[i0] = (real_T)ii_data[i0] + y;
      }
    } else {
      /*  in the very unlikely case the last half is all low */
      T0_size[0] = 1;
      T0_data[0] = 0.0;
    }

    emxFree_boolean_T(&x);
  } else {
    /*  the rising edge should be in the first half of the frame */
    idx = 0;
    k = 1;
    ii = 1;
    exitg2 = FALSE;
    while ((exitg2 == FALSE) && (ii < 30001)) {
      if (dataSec[ii - 1] > 1.0) {
        idx = 1;
        ii_data[0] = ii;
        exitg2 = TRUE;
      } else {
        ii++;
      }
    }

    if (idx == 0) {
      k = 0;
    }

    T0_size[0] = k;
    i0 = 0;
    while (i0 <= k - 1) {
      T0_data[0] = ii_data[0];
      i0 = 1;
    }
  }

  /*  check if a T0 is detected in data stream or otherwise */
  if (!(T0_size[0] == 0)) {
    /*   */
    lastT0_size[0] = T0_size[0];
    idx = T0_size[0];
    for (i0 = 0; i0 < idx; i0++) {
      lastT0_data[i0] = T0_data[i0];
    }
  } else {
    /*  T0 is empty */
    /*  remove the initial condition and create a 'real' time */
    p = FALSE;
    b_p = TRUE;
    k = 0;
    exitg1 = FALSE;
    while ((exitg1 == FALSE) && (k <= 0)) {
      if (!(b_lastT0 == -1.0)) {
        b_p = FALSE;
        exitg1 = TRUE;
      } else {
        k = 1;
      }
    }

    if (!b_p) {
    } else {
      p = TRUE;
    }

    if (p) {
      lastT0_size[0] = 1;
      lastT0_data[0] = 1.0;
    }

    T0_size[0] = 1;
    T0_data[0] = lastT0_data[0];
  }
}

/* End of code generation (T0_Detect.c) */
