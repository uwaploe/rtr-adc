/*
 * Main_Processing_Algorithm_data.h
 *
 * Code generation for function 'Main_Processing_Algorithm_data'
 *
 *
 */

#ifndef __MAIN_PROCESSING_ALGORITHM_DATA_H__
#define __MAIN_PROCESSING_ALGORITHM_DATA_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Variable Declarations */
extern real_T lastT0;
extern emxArray_real_T *pulseFrame;
extern boolean_T pulseFrame_not_empty;
extern emxArray_real_T *nextFrameIndA;
extern boolean_T nextFrameIndA_not_empty;
extern emxArray_real_T *nextFrameIndB;
extern boolean_T nextFrameIndB_not_empty;
extern real_T oldData[30000];
#endif
/* End of code generation (Main_Processing_Algorithm_data.h) */
