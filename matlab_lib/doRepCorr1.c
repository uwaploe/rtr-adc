/*
 * doRepCorr1.c
 *
 * Code generation for function 'doRepCorr1'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "doRepCorr1.h"
#include "Main_Processing_Algorithm_emxutil.h"
#include "conv.h"
#include "flipud.h"

/* Function Definitions */
void b_doRepCorr1(emxArray_real_T *data, const real_T replica[105],
                  emxArray_real_T *repcorrdat)
{
  emxArray_real_T *b_data;
  int32_T c_data;
  int32_T i3;
  emxArray_real_T *b_repcorrdat;
  b_emxInit_real_T(&b_data, 1);

  /* ****************************************************************************** */
  /*  Project: ICEX */
  /*  Script: doRepCorr1 */
  /*  Programmer: Sean Lastuka */
  /*  Origination Date: 20131114 */
  /*  */
  /*  Description: */
  /*  Replica Correlation function using conventional matlab convolution */
  /*  function */
  /*  */
  /*   */
  /*  Functions Called:  conv, fliplr */
  /*  */
  /*  Input:    replica  */
  /*            data - one frame of time series data */
  /*  */
  /*  Output:   repcorrdat - correlation function output */
  /*  */
  /*  Modification History */
  /*  20131114 - Sean Lastuka, originate */
  /*  */
  /* ****************************************************************************** */
  c_data = data->size[0];
  i3 = b_data->size[0];
  b_data->size[0] = c_data + 104;
  emxEnsureCapacity((emxArray__common *)b_data, i3, (int32_T)sizeof(real_T));
  for (i3 = 0; i3 < c_data; i3++) {
    b_data->data[i3] = data->data[i3];
  }

  for (i3 = 0; i3 < 104; i3++) {
    b_data->data[i3 + c_data] = 0.0;
  }

  i3 = data->size[0];
  data->size[0] = b_data->size[0];
  emxEnsureCapacity((emxArray__common *)data, i3, (int32_T)sizeof(real_T));
  c_data = b_data->size[0];
  for (i3 = 0; i3 < c_data; i3++) {
    data->data[i3] = b_data->data[i3];
  }

  emxFree_real_T(&b_data);
  b_emxInit_real_T(&b_repcorrdat, 1);
  b_conv(data, replica, repcorrdat);
  c_data = repcorrdat->size[0] - 104;
  c_data -= 105;
  i3 = b_repcorrdat->size[0];
  b_repcorrdat->size[0] = c_data + 1;
  emxEnsureCapacity((emxArray__common *)b_repcorrdat, i3, (int32_T)sizeof(real_T));
  for (i3 = 0; i3 <= c_data; i3++) {
    b_repcorrdat->data[i3] = repcorrdat->data[104 + i3];
  }

  i3 = repcorrdat->size[0];
  repcorrdat->size[0] = b_repcorrdat->size[0];
  emxEnsureCapacity((emxArray__common *)repcorrdat, i3, (int32_T)sizeof(real_T));
  c_data = b_repcorrdat->size[0];
  for (i3 = 0; i3 < c_data; i3++) {
    repcorrdat->data[i3] = b_repcorrdat->data[i3];
  }

  emxFree_real_T(&b_repcorrdat);
}

void doRepCorr1(emxArray_real_T *data, const real_T replica[300],
                emxArray_real_T *repcorrdat)
{
  emxArray_real_T *b_data;
  int32_T c_data;
  int32_T i2;
  real_T dv0[300];
  emxArray_real_T *b_repcorrdat;
  b_emxInit_real_T(&b_data, 1);

  /* ****************************************************************************** */
  /*  Project: ICEX */
  /*  Script: doRepCorr1 */
  /*  Programmer: Sean Lastuka */
  /*  Origination Date: 20131114 */
  /*  */
  /*  Description: */
  /*  Replica Correlation function using conventional matlab convolution */
  /*  function */
  /*  */
  /*   */
  /*  Functions Called:  conv, fliplr */
  /*  */
  /*  Input:    replica  */
  /*            data - one frame of time series data */
  /*  */
  /*  Output:   repcorrdat - correlation function output */
  /*  */
  /*  Modification History */
  /*  20131114 - Sean Lastuka, originate */
  /*  */
  /* ****************************************************************************** */
  c_data = data->size[0];
  i2 = b_data->size[0];
  b_data->size[0] = c_data + 299;
  emxEnsureCapacity((emxArray__common *)b_data, i2, (int32_T)sizeof(real_T));
  for (i2 = 0; i2 < c_data; i2++) {
    b_data->data[i2] = data->data[i2];
  }

  for (i2 = 0; i2 < 299; i2++) {
    b_data->data[i2 + c_data] = 0.0;
  }

  i2 = data->size[0];
  data->size[0] = b_data->size[0];
  emxEnsureCapacity((emxArray__common *)data, i2, (int32_T)sizeof(real_T));
  c_data = b_data->size[0];
  for (i2 = 0; i2 < c_data; i2++) {
    data->data[i2] = b_data->data[i2];
  }

  emxFree_real_T(&b_data);
  memcpy(&dv0[0], &replica[0], 300U * sizeof(real_T));
  b_emxInit_real_T(&b_repcorrdat, 1);
  flipud(dv0);
  conv(data, dv0, repcorrdat);
  c_data = repcorrdat->size[0] - 299;
  c_data -= 300;
  i2 = b_repcorrdat->size[0];
  b_repcorrdat->size[0] = c_data + 1;
  emxEnsureCapacity((emxArray__common *)b_repcorrdat, i2, (int32_T)sizeof(real_T));
  for (i2 = 0; i2 <= c_data; i2++) {
    b_repcorrdat->data[i2] = repcorrdat->data[299 + i2];
  }

  i2 = repcorrdat->size[0];
  repcorrdat->size[0] = b_repcorrdat->size[0];
  emxEnsureCapacity((emxArray__common *)repcorrdat, i2, (int32_T)sizeof(real_T));
  c_data = b_repcorrdat->size[0];
  for (i2 = 0; i2 < c_data; i2++) {
    repcorrdat->data[i2] = b_repcorrdat->data[i2];
  }

  emxFree_real_T(&b_repcorrdat);
}

/* End of code generation (doRepCorr1.c) */
