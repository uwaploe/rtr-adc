/*
 * Main_Processing_Algorithm_rtwutil.c
 *
 * Code generation for function 'Main_Processing_Algorithm_rtwutil'
 *
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Main_Processing_Algorithm.h"
#include "Main_Processing_Algorithm_rtwutil.h"

/* Function Definitions */
real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = -0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* End of code generation (Main_Processing_Algorithm_rtwutil.c) */
