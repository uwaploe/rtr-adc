/*
 * any.h
 *
 * Code generation for function 'any'
 *
 *
 */

#ifndef __ANY_H__
#define __ANY_H__
/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"

#include "rtwtypes.h"
#include "Main_Processing_Algorithm_types.h"

/* Function Declarations */
extern boolean_T any(const real_T x_data[1], const int32_T x_size[1]);
extern boolean_T b_any(const boolean_T x_data[1], const int32_T x_size[1]);
#endif
/* End of code generation (any.h) */
