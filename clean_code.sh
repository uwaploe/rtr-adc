#!/bin/bash
#
# Remove the "generated on" comment from the Matlab generated
# C files.

for f in *.c *.h
do
    echo "Cleaning $f ... "
    sed -i .bak -e '/.*generated on:.*/d' $f
done

