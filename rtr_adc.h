/*
**
*/
#ifndef _RTR_ADC_H_
#define _RTR_ADC_H_

#include <sys/types.h>
#include <sys/ioctl.h>
#include <24dsi.h>
#include <errno.h>
#include <glib.h>
#include <sys/time.h>

#define CHANNELS_PER_BOARD	4
typedef u_int32_t	sample_t;

typedef struct {
    int32_t	fref;	/**> Reference oscillator frequency */
    int32_t	fsamp;	/**> Sample frequency */
    int32_t	nvco;	/**> PLL VCO factor */
    int32_t	nref;	/**> PLL reference factor */
    int32_t	nrate;	/**> Only used on legacy boards */
    int32_t	ndiv;	/**> Rate divisor */
} adc_clock_t;


/*
 * Channel group masks. RTR uses 4 channel boards, 0-1 is Group 0 and
 * 2-3 is Group 1.
 */
#define GROUP_0		0x03
#define GROUP_1		0x0c

/* Mask to extract the channel tag from each sample */
#define CHANNEL_TAG_MASK	0x1f000000

/* External trigger bit in Board Control register */
#define ARM_EXTERNAL_TRIGGER	(1L << 21)

/* Low Frequency Filter bit in Board Control register */
#define LOW_FREQ_FILTER		(1L << 19)

/*
 * We are configuring the board for 24-bit 2's complement data values. The
 * following macros are used to extract the data value from the sample word.
 */
#define DATA_SIZE		24
#define DATA_MASK		((1UL << DATA_SIZE) - 1)
#define DATA_SIGN_BIT		(1 << (DATA_SIZE-1))

static __inline__ int32_t DATA_VALUE(sample_t sample)
{
    return (sample & DATA_SIGN_BIT) ? (sample | ~DATA_MASK) : (sample & DATA_MASK);
}

static __inline__ int DATA_WIDTH_CODE(void)
{
    switch(DATA_SIZE)
    {
	case 24:
	    return DSI_DATA_WIDTH_24;
	case 20:
	    return DSI_DATA_WIDTH_20;
	case 18:
	    return DSI_DATA_WIDTH_18;
	default:
	    return DSI_DATA_WIDTH_16;
    }
}

static __inline__ int AIN_RANGE_CODE(float voltage)
{
    float v = (voltage < 0.) ? -voltage : voltage;
    
    if(v > 5.)
	return DSI_AIN_RANGE_10V;
    if(v > 2.5)
	return DSI_AIN_RANGE_5V;
    return DSI_AIN_RANGE_2_5V;
}


static __inline__ int _ioctl_set(int fd, int req, int val, const char *reqname)
{
    int32_t _val = val;
    int		r;

    g_debug("ioctl(%d, %s)", fd, reqname);
    r = ioctl(fd, req, &_val);
    if(r < 0)
	g_warning("ioctl(%s) failed, errno = %d", reqname, errno);
    return r;
}

#define ioctl_set(fd, req, val)	_ioctl_set(fd, req, val, #req)

#define TRIG_START	0
#define TRIG_END	1


#endif /* _RTR_ADC_H_ */
