Remote Tracking Range Software
==============================

This repository contains the source code for the RTR software.

rtr_adc_test.c logging.c matlab_lib/*
-------------------------------------

These source files get compiled into `rtr_adc_test`, the data collection
program. This program processes the hydrophone signal to produce detection
time-stamps which are written to standard output along with diagnostic
information. Each output line is "tagged" with a single character followed
by a colon. The tag defines the "record type" of the output line:

+ T: time-stamp data
+ D: diagnostic information
+ L: general logging message

rtr.py
------

This is a Python front-end which runs `rtr_adc_test`, samples the
engineering A/D, samples the GPS, and merges all of the output. It adds
the following output record types:

+ P: GPS data
+ E: engineering data

settings.conf
-------------

This is the INI-format configuration file used by `rtr_adc_test`

zrtr.py
-------

New [ZeroMQ](http://zeromq.org) based network front-end for the RTR.


service
--------

This directory contains the [Runit](http://smarden.org/runit/) service
scripts to manage the network front-end.
