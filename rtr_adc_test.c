/*
** Simple program to exercise the A/D board on the RTR system.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <24dsi_utils.h>
#include <gsc_utils.h>
#include <glib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <signal.h>
#include "rtr_adc.h"
#include "logging.h"
#include "Main_Processing_Algorithm_emxAPI.h"
#include "Main_Processing_Algorithm_initialize.h"
#include "Main_Processing_Algorithm.h"

/* Proper math modulus operation */
#define MOD(a,b) (((a < 0) ? ((a % b) + b) : a) % b)

#define ADC_DEVICE      "/dev/24dsi0"
#define SAMPLE_COUNT    30000L
#define NCHANS          2
#define CHANNEL_MASK    GROUP_0
#define VRANGE          10.
#define QSIZE           10

/* timeout in milliseconds */
#define CHANNELS_READY_TIMEOUT  10000

static int intr_signals[] = {SIGHUP, SIGINT, SIGQUIT, SIGTERM};

#define NR_INTS (sizeof(intr_signals)/sizeof(int))

#define SAMPLE_BYTES(n, chans)          (sizeof(sample_t)*(n)*(chans))
#define DATABLOCK_SIZE(n, chans)        (SAMPLE_BYTES(n, chans) + sizeof(struct timeval) + sizeof(int))


typedef struct {
    struct timeval      ts;
    int                 n;
    sample_t            data[1];
} data_block_t;

static data_block_t     *block[QSIZE];
static GAsyncQueue      *freelist;
static GAsyncQueue      *dataq;
static sig_atomic_t     proc_done = 0;
/* Always grab 1-second of data */
static gint             f_sample = SAMPLE_COUNT;
static gchar            *cfg_filename = NULL;

static GOptionEntry entries[] = {
    {"f-sample", 'f', 0, G_OPTION_ARG_INT, &f_sample, "Sampling frequency in Hz", "HZ"},
    {"config", 'c', 0, G_OPTION_ARG_FILENAME, &cfg_filename, "Name of config file", "FILE"},
    { NULL }
};

static void
queue_alloc(int samples, int channels)
{
    int         i;

    freelist = g_async_queue_new();
    dataq = g_async_queue_new();

    for(i = 0;i < QSIZE;i++)
    {
        block[i] = (data_block_t*)g_slice_alloc(DATABLOCK_SIZE(samples,
                                                               channels));
        block[i]->n = samples * channels;
        g_async_queue_push(freelist, block[i]);
    }
}

static void
queue_free(int samples, int channels)
{
    int         i;

    g_async_queue_unref(dataq);
    g_async_queue_unref(freelist);
    for(i = 0;i < QSIZE;i++)
        g_slice_free1(DATABLOCK_SIZE(samples, channels), block[i]);
}

/*
 * Get a block from the free-list. This function does not block,
 * it returns NULL if the free-list is empty.
 */
static data_block_t*
acquire_block(void)
{
    data_block_t        *dp;

    dp = (data_block_t*)g_async_queue_try_pop(freelist);
    if(dp == NULL)
        g_critical("Data queue is full!");
    return dp;
}

/*
 * Return a block to the free-list.
 */
static void
release_block(data_block_t *dp)
{
    g_async_queue_push(freelist, dp);
}


/*
 * Sign extend the data values to 32-bit integers
 */
static void
sign_extend(sample_t *data, int n)
{
    int         i;
    for(i = 0;i < n;i++)
        data[i] = (sample_t)DATA_VALUE(data[i]);
}

static double
rms_voltage_from_counts(sample_t *data, int n, int start, int stride)
{
    double      mean, mean_sq, v, vpc;
    double      sum = 0., sum_sq = 0.;
    int         i;

    /* A/D span is +/- VRANGE */
    vpc = VRANGE/(1 << (DATA_SIZE-1));

    for(i = start;i < n;i += stride)
    {
        v = (int32_t)data[i] * vpc;
        sum += v;
        sum_sq += (v*v);
    }
    mean = sum * stride / n;
    mean_sq = sum_sq * stride / n;

    return sqrt(mean_sq - mean * mean);
}

static double
rms_voltage(real_T *data, int n, int start, int stride)
{
    double      mean, mean_sq;
    double      sum = 0., sum_sq = 0.;
    real_T      v;
    int         i;

    for(i = start;i < n;i += stride)
    {
        v = data[i];
        sum += v;
        sum_sq += (v*v);
    }
    mean = sum * stride / n;
    mean_sq = sum_sq * stride / n;

    return sqrt(mean_sq - mean * mean);
}

static real_T
scan_number(GScanner *scanner)
{
    GTokenType  tok;
    real_T      mult = 1.;

    tok = g_scanner_get_next_token(scanner);
    if(tok == '-')
    {
        mult = -1.;
        tok = g_scanner_get_next_token(scanner);
    }

    if(!(tok == G_TOKEN_FLOAT || tok == G_TOKEN_INT))
        g_error("Syntax error in coefficient file (line %d)", scanner->line);
    return mult * scanner->value.v_float;
}

/*
 * Read a filter coefficients file. The first line contains two ASCII integers
 * specifying the number of rows (lines) and columns of data to follow.
 */
static int
load_coefficients(const char *filename, real_T ***c, int *rows, int *cols)
{
    int         fd, i, j, n_rows, n_cols;
    GScanner    *scanner;
    GTokenType  tok;
    real_T      **C;

    fd = open(filename, O_RDONLY);
    if(fd < 0)
        g_error("Cannot open coefficient file: %s", g_strerror(errno));

    scanner = g_scanner_new(NULL);
    g_scanner_input_file(scanner, fd);

    tok = g_scanner_get_next_token(scanner);
    if(tok != G_TOKEN_INT)
        g_error("Syntax error in coefficient file (line %d)", scanner->line);
    n_rows = scanner->value.v_int;
    tok = g_scanner_peek_next_token(scanner);
    if(tok == G_TOKEN_INT)
    {
        tok = g_scanner_get_next_token(scanner);
        n_cols = scanner->value.v_int;
    }
    else
        n_cols = 1;

    g_message("Loading %d set(s) of %d coefficients", n_cols, n_rows);

    C = g_malloc(n_cols * sizeof(gpointer));
    for(i = 0;i < n_cols;i++)
        C[i] = g_malloc0(n_rows * sizeof(real_T));

    i = 0;
    while(i < n_rows)
    {
        if(g_scanner_eof(scanner))
            g_error("Unexpected EOF in coefficient file (line %d)",
                    scanner->line);
        for(j = 0;j < n_cols;j++)
            C[j][i] = scan_number(scanner);

        i++;
    }
    g_scanner_destroy(scanner);
    *c = C;
    if(rows)
        *rows = n_rows;
    if(cols)
        *cols = n_cols;

    return n_rows * n_cols;
}

static void
show_timestamps(struct timeval *tv, real_T *vals, int rows,
                int cols, int show_all)
{
    int         i, j;

    /* matrix data is in column-major order */
    printf("T:%ld", tv->tv_sec);
    for(i = 0;i < rows;i++)
    {
        if(show_all)
        {
           j = 0;
        }
        else
        {
            /* scan row for any non -1 values */
            for(j = 0;j < cols;j++)
                if(vals[i+j*rows] >= 0)
                    break;
        }

        if(j < cols)
        {
            for(j = 0;j < cols;j++)
                printf(" %d", (int)vals[i+j*rows]);
            printf(";");
        }
    }
    printf("\n");

}

/*
 * Thread to process the input data with an FIR filter.
 */
static void*
filter_data(gpointer thread_arg)
{
    sigset_t            mask;
    int                 i, j;
    int                 n_samples, n_coeff, n_cols;
    int                 len_p_replica, len_u_replica;
    int                 data_chan, pps_chan, n_detect_out;
    int                 signal_channels, vehicle_types, quads, debug;
    int                 max_detects, var_buffer_len, show_all, last_record_only;
    real_T              var_limit_a_default, var_limit_b_default;
    real_T              blanking_time, threshold_a_percent, threshold_b_percent;
    long                d_ns, d_s;
    real_T              *rawdata, *ppsdata, *prev_pulse;
    data_block_t        *dp;
    GKeyFile            *cfgfile = (GKeyFile*)thread_arg;
    emxArray_real_T     *outputA, *outputB;
    real_T              *cA, *cB, **coeffs, **platform_replica, **unit_replica;
    real_T              overlap, vpc, style, peak_smooth;
    real_T              var_limit_a[4], var_limit_b[4], time_offset[1];
    int32_T             prev_pulse_dims[2], time_offset_dims[1];
    real_T              first_frame_index_A[2], first_frame_index_B[2];
    FILE                *outfile = NULL, *pps_outfile = NULL, *raw_outfile = NULL;
    char                *fname;
    struct timespec     ts0, ts1;
    struct timeval      ts;
    real_T              var_A_current, var_B_current, threshold_A, threshold_B;
    real_T              var_A_output[3] = {0.0001, 0.0001, 0.0001};
    real_T              var_B_output[3] = {0.0001, 0.0001, 0.0001};
    real_T      peak_A_out[2], peak_B_out[2];
    int32_T     peak_out_dims[1] = {2};

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    /* Force line buffered output */
    setlinebuf(stdout);

    /* Load FIR filter coefficients */
    fname = g_key_file_get_string(cfgfile, "filter", "coeff_file", NULL);
    if(fname == NULL)
        g_error("No filter coefficient file specified");
    load_coefficients(fname, &coeffs, &n_coeff, &n_cols);
    if(n_cols != 2)
        g_error("Two columns of filter coefficents required");
    cA = coeffs[0];
    cB = coeffs[1];

    /* Load matched-filter replica coefficients */
    fname = g_key_file_get_string(cfgfile, "filter", "platform_replica", NULL);
    if(fname == NULL)
        g_error("No platform replica file specified");
    load_coefficients(fname, &platform_replica, &len_p_replica, &n_cols);
    if(n_cols != 1)
        g_error("Bad format for platform replica");
    fname = g_key_file_get_string(cfgfile, "filter", "unit_replica", NULL);
    if(fname == NULL)
        g_error("No unit replica file specified");
    load_coefficients(fname, &unit_replica, &len_u_replica, &n_cols);
    if(n_cols != 1)
        g_error("Bad format for unit replica");

    overlap = g_key_file_get_integer(cfgfile, "filter", "overlap", NULL);
    if(overlap == 0)
        g_error("Bad filter parameters");

    n_samples = g_key_file_get_integer(cfgfile, "general", "fsample", NULL);
    if(n_samples <= 0)
        n_samples = f_sample;
    data_chan = g_key_file_get_integer(cfgfile, "general", "data_channel", NULL);
    pps_chan = g_key_file_get_integer(cfgfile, "general", "pps_channel", NULL);
    rawdata = (real_T*)g_malloc(n_samples * sizeof(real_T));
    ppsdata = (real_T*)g_malloc(n_samples * sizeof(real_T));

    outputA = emxCreate_real_T(n_samples, 1);
    outputB = emxCreate_real_T(n_samples, 1);
    if(!outputA || !outputB)
        g_error("Memory allocation failure. Aborting");

    style = (real_T)g_key_file_get_integer(cfgfile, "filter", "style", NULL);

    signal_channels = g_key_file_get_integer(cfgfile, "detect",
                                             "signal_channels", NULL);
    quads = g_key_file_get_integer(cfgfile, "detect", "quads", NULL);
    vehicle_types = g_key_file_get_integer(cfgfile, "detect",
                                           "vehicle_types", NULL);
    max_detects = g_key_file_get_integer(cfgfile, "detect",
                                         "max_detects", NULL);
    peak_smooth = g_key_file_get_double(cfgfile, "detect", "peak_smooth", NULL);
    var_buffer_len = g_key_file_get_integer(cfgfile, "detect",
                                            "var_buffer_len", NULL);
    var_limit_a_default = g_key_file_get_double(cfgfile, "detect",
                                                "var_limit_a_default", NULL);
    var_limit_b_default = g_key_file_get_double(cfgfile, "detect",
                                                "var_limit_b_default", NULL);
    threshold_a_percent = g_key_file_get_double(cfgfile, "detect",
                                                "threshold_a_percent", NULL);
    threshold_b_percent = g_key_file_get_double(cfgfile, "detect",
                                                "threshold_b_percent", NULL);
    blanking_time = g_key_file_get_double(cfgfile, "detect",
                                          "blanking_time", NULL);
    if(g_key_file_has_key(cfgfile, "detect", "show_all", NULL))
        show_all = g_key_file_get_integer(cfgfile, "detect", "show_all", NULL);
    else
        show_all = 0;


    for(i = 0;i < 4;i++)
    {
        var_limit_a[i] = var_limit_a_default;
        var_limit_b[i] = var_limit_b_default;
    }

    prev_pulse_dims[0] = ceil(1./blanking_time); /* rows */
    prev_pulse_dims[1] = signal_channels * quads * vehicle_types; /* columns */

    n_detect_out = prev_pulse_dims[0] * prev_pulse_dims[1];
    prev_pulse = (real_T*)g_malloc(n_detect_out * sizeof(real_T));

    for(i = 0;i < n_detect_out;i++)
        prev_pulse[i] = -1;

    time_offset_dims[0] = 1;

    fname = g_key_file_get_string(cfgfile, "output", "filtered", NULL);
    if(fname != NULL)
        if((outfile = fopen(fname, "wb")) == NULL)
            g_error("Cannot open output file: %s", g_strerror(errno));

    fname = g_key_file_get_string(cfgfile, "output", "pps", NULL);
    if(fname != NULL)
        if((pps_outfile = fopen(fname, "wb")) == NULL)
            g_error("Cannot open output file: %s", g_strerror(errno));

    fname = g_key_file_get_string(cfgfile, "output", "rawdata", NULL);
    if(fname != NULL)
        if((raw_outfile = fopen(fname, "wb")) == NULL)
            g_error("Cannot open output file: %s", g_strerror(errno));

    if(g_key_file_has_key(cfgfile, "output", "last_record_only", NULL))
        last_record_only = g_key_file_get_integer(cfgfile, "output",
                                                  "last_record_only", NULL);
    else
        last_record_only = 0;

    if(g_key_file_has_key(cfgfile, "general", "debug", NULL))
        debug = g_key_file_get_integer(cfgfile, "general", "debug", NULL);
    else
        debug = 0;

    /* A/D span is +/- VRANGE */
    vpc = VRANGE/(1 << (DATA_SIZE-1));

    Main_Processing_Algorithm_initialize();

    while(!proc_done)
    {
        dp = (data_block_t*)g_async_queue_pop(dataq);
        clock_gettime(CLOCK_MONOTONIC_RAW, &ts0);
        for(i = 0,j = 0;i < dp->n;i += NCHANS)
        {
            rawdata[j] = vpc * DATA_VALUE(dp->data[i+data_chan]);
            ppsdata[j] = vpc * DATA_VALUE(dp->data[i+pps_chan]);
            j++;
        }
        memcpy(&ts, &dp->ts, sizeof(struct timeval));

        release_block(dp);

        /* Filter and detect */
        Main_Processing_Algorithm(rawdata, ppsdata,
                                  cA, cB, overlap,
                                  platform_replica[0], unit_replica[0],
                                  style, var_limit_a, var_limit_b,
                                  var_buffer_len,
                                  threshold_a_percent, threshold_b_percent,
                                  peak_smooth,
                                  prev_pulse, prev_pulse_dims,
                                  max_detects, signal_channels, quads,
                                  vehicle_types,
                                  blanking_time, n_samples, n_samples,
                                  var_A_output, var_B_output,
                                  outputA, outputB,
                                  time_offset, time_offset_dims,
                                  &var_A_current, &var_B_current,
                                  &threshold_A, &threshold_B,
                                  first_frame_index_A,
                                  first_frame_index_B,
                                  peak_A_out, peak_out_dims,
                                  peak_B_out, peak_out_dims);

        clock_gettime(CLOCK_MONOTONIC_RAW, &ts1);

        d_ns = ts1.tv_nsec - ts0.tv_nsec;
        d_s = ts1.tv_sec - ts0.tv_sec;
        d_ns += (d_s * 1000000000);

        if(debug)
            g_message("%ld Run time %.1f ms", dp->ts.tv_sec, d_ns/1.0e6);
        /*
         * Calculated timestamps are for the previous frame so we must subtract
         * 1 second.
         */
        ts.tv_sec -= 1;
        show_timestamps(&ts, prev_pulse, prev_pulse_dims[0],
                        prev_pulse_dims[1], show_all);

        if(debug)
        {
            printf("D:%ld varAOutput = %.3e, %.3e, %.3e;",
                   ts.tv_sec,
                   var_A_output[0],
                   var_A_output[1],
                   var_A_output[2]);
            printf("varACurrent = %.3e;", var_A_current);
            printf("threshold_A = %.3e;", threshold_A);
            printf("firstFrameIndA = %.0f, %.0f;", first_frame_index_A[0],
                   first_frame_index_A[1]);
            printf("peakAOut = %.0f, %.0f;", peak_A_out[0], peak_A_out[1]);

            printf("varBOutput = %.3e, %.3e, %.3e;", var_B_output[0],
                   var_B_output[1],
                   var_B_output[2]);
            printf("varBCurrent = %.3e;", var_B_current);
            printf("threshold_B = %.3e;", threshold_B);
            printf("firstFrameIndB = %.0f, %.0f;", first_frame_index_B[0],
                   first_frame_index_B[1]);
            printf("peakBOut = %.0f, %.0f;", peak_B_out[0], peak_B_out[1]);

            printf("timeOffset = %.6f;\n", time_offset[0]);
        }

        /* Write the data to the output file */
        if(!last_record_only)
        {
            if(outfile)
            {
                fwrite(&outputA->data[0], outputA->allocatedSize,
                       sizeof(real_T), outfile);
                fwrite(&outputB->data[0], outputB->allocatedSize,
                       sizeof(real_T), outfile);
            }

            if(pps_outfile)
                fwrite(&ppsdata[0], n_samples, sizeof(real_T), pps_outfile);
            if(raw_outfile)
                fwrite(&rawdata[0], n_samples, sizeof(real_T), raw_outfile);
        }
    }

    if(last_record_only)
    {
        if(outfile)
        {
            fwrite(&outputA->data[0], outputA->allocatedSize,
                   sizeof(real_T), outfile);
            fwrite(&outputB->data[0], outputB->allocatedSize,
                   sizeof(real_T), outfile);
        }

        if(pps_outfile)
            fwrite(&ppsdata[0], n_samples, sizeof(real_T), pps_outfile);
        if(raw_outfile)
            fwrite(&rawdata[0], n_samples, sizeof(real_T), raw_outfile);
    }

    if(outfile)
        fclose(outfile);
    if(pps_outfile)
        fclose(pps_outfile);

    g_free(cA);
    g_free(cB);
    g_free(coeffs);
    g_free(platform_replica[0]);
    g_free(platform_replica);
    g_free(unit_replica[0]);
    g_free(unit_replica);
    g_free(rawdata);
    g_free(ppsdata);
    g_free(prev_pulse);

    emxDestroyArray_real_T(outputA);
    emxDestroyArray_real_T(outputB);

    return NULL;
}

/*
 * Thread to calculate the RMS voltage for a buffer of input
 * data and write the value to standard output.
 */
static void*
process_data(gpointer thread_arg)
{
    double              rms0, rms1;
    long                d_ns;
    data_block_t        *dp;
    struct timespec     ts0, ts1;
    sigset_t            mask;
    int                 i;
    FILE                *ofp = NULL;
    char                *fname;
    GKeyFile            *cfgfile = (GKeyFile*)thread_arg;

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    fname = g_key_file_get_string(cfgfile, "output", "rawdata", NULL);
    if(fname != NULL)
        if((ofp = fopen(fname, "wb")) == NULL)
            g_error("Cannot open output file: %s", g_strerror(errno));

    while(!proc_done)
    {
        dp = (data_block_t*)g_async_queue_pop(dataq);
        clock_gettime(CLOCK_MONOTONIC_RAW, &ts0);
        sign_extend(dp->data, dp->n);
        rms0 = rms_voltage_from_counts(dp->data, dp->n, 0, NCHANS);
        rms1 = rms_voltage_from_counts(dp->data, dp->n, 1, NCHANS);
        clock_gettime(CLOCK_MONOTONIC_RAW, &ts1);

        d_ns = ts1.tv_nsec - ts0.tv_nsec;
        if(d_ns < 0)
            d_ns += 1000000000;

        printf("R:%ld %.6f %.6f (%.1f ms)\n", dp->ts.tv_sec, rms0, rms1,
               d_ns/1.0e6);
        if(ofp)
            fwrite(dp->data, sizeof(sample_t), dp->n, ofp);
        release_block(dp);
    }

    return NULL;
}


static void
_board_set_clock(int fd, unsigned mask, adc_clock_t *cp)
{
    int32_t     src;

    if(mask & GROUP_0)
        src = DSI_CH_GRP_SRC_GEN_A;
    else
        src = DSI_CH_GRP_SRC_DISABLE;
    ioctl_set(fd, DSI_IOCTL_CH_GRP_0_SRC, src);

    if(mask & GROUP_1)
        src = DSI_CH_GRP_SRC_GEN_A;
    else
        src = DSI_CH_GRP_SRC_DISABLE;
    ioctl_set(fd, DSI_IOCTL_CH_GRP_1_SRC, src);

    ioctl(fd, DSI_IOCTL_RATE_GEN_A_NVCO, &cp->nvco);
    ioctl(fd, DSI_IOCTL_RATE_GEN_B_NVCO, &cp->nvco);
    ioctl(fd, DSI_IOCTL_RATE_GEN_A_NREF, &cp->nref);
    ioctl(fd, DSI_IOCTL_RATE_GEN_B_NREF, &cp->nref);
    ioctl(fd, DSI_IOCTL_RATE_DIV_0_NDIV, &cp->ndiv);
    ioctl(fd, DSI_IOCTL_RATE_DIV_1_NDIV, &cp->ndiv);
}


static void
_board_init(int fd, long bufsize, long timeout, float v_peak)
{
    g_message("Initializing the board (5 seconds) ...");
    ioctl(fd, DSI_IOCTL_INITIALIZE, NULL);

    ioctl_set(fd, DSI_IOCTL_RX_IO_MODE, GSC_IO_MODE_DMA);
    ioctl_set(fd, DSI_IOCTL_RX_IO_OVERFLOW, DSI_IO_OVERFLOW_IGNORE);
    ioctl_set(fd, DSI_IOCTL_RX_IO_UNDERFLOW, DSI_IO_UNDERFLOW_IGNORE);
    ioctl_set(fd, DSI_IOCTL_RX_IO_TIMEOUT, timeout);

    ioctl_set(fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_ENABLE);
    ioctl_set(fd, DSI_IOCTL_AIN_COUPLING, DSI_AIN_COUPLING_DC);
    ioctl_set(fd, DSI_IOCTL_AIN_BUF_THRESH, bufsize);
    ioctl_set(fd, DSI_IOCTL_CHANNEL_ORDER, DSI_CHANNEL_ORDER_SYNC);
    ioctl_set(fd, DSI_IOCTL_DATA_FORMAT, DSI_DATA_FORMAT_2S_COMP);
    ioctl_set(fd, DSI_IOCTL_DATA_WIDTH, DATA_WIDTH_CODE());
    ioctl_set(fd, DSI_IOCTL_INIT_MODE, DSI_INIT_MODE_INITIATOR);
    ioctl_set(fd, DSI_IOCTL_EXT_CLK_SRC, DSI_EXT_CLK_SRC_GRP_0);
    ioctl_set(fd, DSI_IOCTL_XCVR_TYPE, DSI_XCVR_TYPE_TTL);
    /* Activate low frequency filter */
    dsi_reg_mod(fd, DSI_GSC_BCTLR, LOW_FREQ_FILTER, LOW_FREQ_FILTER);
}


/*
 * Enable buffer overflow check
 */
static void
enable_buffer_check(int fd)
{
    int32_t     arg;

    arg = DSI_AIN_BUF_OVERFLOW_CLEAR;
    ioctl(fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &arg);
    arg = DSI_IO_OVERFLOW_CHECK;
    ioctl(fd, DSI_IOCTL_RX_IO_OVERFLOW, &arg);
}


/**
 * Compute the clock parameters to produce the requested sampling frequency.
 *
 * @param adc pointer to ADC data structure
 * @param fsamp desired sampling frequency in hz
 * @return actual sampling frequency in hz
 */
long
adc_compute_clock(int fd, adc_clock_t* cp, long fsamp)
{
    cp->fref = -1;
    dsi_fref_compute(fd, -1, 0, &cp->fref);
    cp->fsamp = fsamp;
    dsi_fsamp_validate(fd, -1, 0, &cp->fsamp);
    dsi_fsamp_compute(fd, -1, 0, 0, cp->fref,
                      &cp->fsamp,
                      &cp->nvco,
                      &cp->nref,
                      &cp->nrate,
                      &cp->ndiv);
    return cp->fsamp;
}


long
adc_init(int fd, int mask, long fsamp, long timeout, float v_peak)
{
    long        bufsize = fsamp * 2;
    adc_clock_t clock;

    _board_init(fd, bufsize, timeout, v_peak);

    fsamp = adc_compute_clock(fd, &clock, fsamp);
    _board_set_clock(fd, mask, &clock);

    g_message("Autocalibrating A/D board ...");
    ioctl(fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);
    g_message("Calibration complete");

    g_message("Clearing buffers");

    ioctl(fd, DSI_IOCTL_AIN_BUF_CLEAR, NULL);

    return fsamp;
}

void
stream_data(int fd, size_t nbytes)
{
    int32_t     save_irq;
    gsc_wait_t  wait;
    data_block_t        *dp;
    size_t              n_read;

    /* Save current IRQ setting */
    save_irq = -1;
    ioctl(fd, DSI_IOCTL_IRQ_SEL, &save_irq);
    /* Configure to receive CHANNEL_READY interrupt */
    ioctl_set(fd, DSI_IOCTL_IRQ_SEL, DSI_IRQ_CHAN_READY);

    memset(&wait, 0, sizeof(wait));
    wait.gsc = DSI_WAIT_GSC_CHAN_READY;
    wait.timeout_ms = CHANNELS_READY_TIMEOUT;

    /* Sync the board */
    ioctl_set(fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_CLR_BUF);
    ioctl(fd, DSI_IOCTL_SW_SYNC, NULL);

    /* Wait until the board is ready ... */
    ioctl(fd, DSI_IOCTL_WAIT_EVENT, &wait);

    /* Restore IRQ setting */
    ioctl(fd, DSI_IOCTL_IRQ_SEL, &save_irq);

    if(wait.flags != GSC_WAIT_FLAG_DONE)
    {
        g_critical("CHANNEL_READY not detected!");
        return;
    }

    enable_buffer_check(fd);
    while(!proc_done)
    {
        dp = acquire_block();
        if(dp)
        {
            n_read = read(fd, dp->data, nbytes);
            if(n_read < 0)
            {
                g_critical("Error reading ADC: %s", g_strerror(errno));
                return;
            }
            else if(n_read < nbytes)
                g_critical("Short read!");

            gettimeofday(&dp->ts, NULL);
            /* Adjust to time that data was acquired */
            dp->ts.tv_sec -= 1;
            g_async_queue_push(dataq, dp);
        }
        else
            return;
    }
}

static void
catch_signal(int signum)
{
    proc_done = 1;
}

int main(int ac, char *av[])
{
    int                 fd, err, i;
    pthread_t           t_process;
    pthread_attr_t      attr;
    void                *status;
    struct sigaction    sa, old_sa[NR_INTS];
    GError              *error = NULL;
    GOptionContext      *context;
    GKeyFile            *cfgfile = NULL;
    gboolean            do_filter = FALSE;

    g_thread_init(NULL);

    context = g_option_context_new(" [CAPTURE_FILE] - sample channels 0 and 1 of ADC");
    g_option_context_add_main_entries(context, entries, NULL);
    if(!g_option_context_parse(context, &ac, &av, &error))
    {
        g_print("command line parsing failed: %s\n", error->message);
        exit(-1);
    }

    log_set_level(G_LOG_LEVEL_DEBUG);
    g_log_set_default_handler(log_handler, NULL);

    /* Initialize ADC */
    if((fd = open(ADC_DEVICE, O_RDWR)) < 0)
        g_error("Cannot open %s (%s)", ADC_DEVICE, g_strerror(errno));

    cfgfile = g_key_file_new();
    if(cfg_filename)
        g_key_file_load_from_file(cfgfile, cfg_filename, 0, NULL);

    /* Command-line arg overrides config file setting */
    if(f_sample != SAMPLE_COUNT)
        g_key_file_set_integer(cfgfile, "general", "fsample", f_sample);
    else
    {
        int _fs;
        _fs = g_key_file_get_integer(cfgfile, "general", "fsample", NULL);
        if(_fs > 0)
            f_sample = _fs;
    }

    do_filter = g_key_file_has_group(cfgfile, "filter");

    queue_alloc(f_sample, NCHANS);

    if(ac >= 2)
        g_key_file_set_value(cfgfile, "output", "rawdata", av[1]);

    /*
     * Install a handler for the signals which can interrupt the sampling.
     */
    sa.sa_handler = catch_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &sa, &old_sa[i]);

    adc_init(fd, CHANNEL_MASK, f_sample, 5, VRANGE);

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    /* Create processing thread */
    if(do_filter)
        err = pthread_create(&t_process, &attr, filter_data, cfgfile);
    else
        err = pthread_create(&t_process, &attr, process_data, cfgfile);

    if(err != 0)
    {
        g_critical("Cannot create processing thread (%d)", err);
        return 1;
    }

    /* Start sampling */
    stream_data(fd, SAMPLE_BYTES(f_sample, NCHANS));
    if(!proc_done)
    {
        pthread_cancel(t_process);
    }
    else
    {
        g_message("Wait for processing thread to exit\n");
        pthread_join(t_process, &status);
    }

    queue_free(f_sample, NCHANS);

    return 0;
}
