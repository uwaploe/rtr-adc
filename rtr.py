#!/usr/bin/env python
#
"""
rtr.py - driver program for RTR sampling.
"""
import sys
import gps
import select
import time
import argparse
from calendar import timegm
from decimal import Decimal, getcontext
from ConfigParser import RawConfigParser, Error
from subprocess import Popen, PIPE, STDOUT


# Volts per count
VPC = Decimal(5) / Decimal(4096)

# Voltage resolution
VRES = Decimal('0.001')

# /proc interface to A/D data
PROC_FILE = '/proc/iguana_adc'


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b:a*x + b, self.C, 0)
        return y, self.units


def read_adc(fname, cals, vpc, vres):
    """
    Read the current A/D inputs and return as a CSV string.

    :param fname: /proc filename for A/D data
    :param cals: list of *channel*, *Calibration* tuples
    :param vpc: volts per A/D count
    :return: CSV formatted string with timestamp and A/D values
    """
    line = open(fname, 'r').read()
    f = line.strip().split()
    output = [f[0]]
    getcontext().prec = 4
    for chan, cal in cals:
        x = (Decimal(f[chan+1]) * vpc).quantize(vres)
        y, units = cal(x)
        output.append('{0} {1}'.format(str(y), units))
    return ','.join(output)


def input_monitor(gps_client, pipe):
    """
    Monitor the GPSd data stream and the output of the acoustic
    processing program and return each data record as it arrives.

    :param gps_client: client connection to GPSd
    :param pipe: output pipe from rtr_adc_test
    :yields: tuple of *type*, *data* where *type* is ``gps`` or ``rtr``
    """
    while True:
        rlist, _, _ = select.select([gps_client.sock, pipe], [], [])
        if gps_client.sock in rlist:
            if gps_client.read() == -1:
                break
            if gps_client.data['class'] == 'TPV':
                yield 'gps', gps_client.data
        if pipe in rlist:
            yield 'rtr', pipe.readline()


def main():
    parser = argparse.ArgumentParser(description='Collect RTR data and stream to stdout')
    parser.add_argument('cfgfile', metavar='CONFIGFILE',
                        help='configuration file')
    parser.add_argument('--prog', metavar='PROGNAME',
                        default='rtr_adc_test',
                        help='name of tracking program (must be in PATH)')
    args = parser.parse_args()
    cfg = RawConfigParser()
    with open(args.cfgfile, 'r') as fp:
        cfg.readfp(fp)
    
    # Extract A/D calibration data from the config file
    cals = []
    try:
        for name, value in cfg.items('adc'):
            if name.startswith('ai'):
                chan = int(name[2:])
                f = value.split(',')
                # The last item in ``values`` is the units, the rest of the
                # items are calibration coefficients
                coeffs = [Decimal(x) for x in f[:-1]]
                cals.append((chan, Calibration(coeffs, f[-1])))
    except Error:
        sys.stderr.write('No A/D coefficients in config file\n')
        sys.exit(1)

    if cfg.has_option('gps', 'interval'):
        gps_interval = cfg.getint('gps', 'interval')
    else:
        gps_interval = 10

    if cfg.has_option('adc', 'interval'):
        adc_interval = cfg.getint('adc', 'interval')
    else:
        adc_interval = 10

    # Open a connection to GPSD
    mode = gps.WATCH_ENABLE | gps.WATCH_JSON | gps.WATCH_SCALED
    client = gps.gps(host='localhost',
                     port=2947,
                     mode=mode)
    # GPSD date/time format string
    tfmt = '%Y-%m-%dT%H:%M:%S %Z'

    # Start the tracking program
    proc = Popen([args.prog, '-c', args.cfgfile], bufsize=0, stdout=PIPE, stderr=STDOUT)
    try:
        show_gps = 0
        show_adc = 0
        for tag, datarec in input_monitor(client, proc.stdout):
            if tag == 'rtr':
                sys.stdout.write(datarec)
            elif tag == 'gps':
                if show_gps == 0:
                    t = timegm(time.strptime(datarec.time[:-5] + ' UTC', tfmt))
                    print 'P:{0:d} {1:.6f} {2:.6f}'.format(t, datarec.lat, datarec.lon)
                    show_gps = gps_interval
                if show_adc == 0:
                    adc_data = read_adc(PROC_FILE, cals, VPC, VRES)
                    print 'E:{0}'.format(adc_data)
                    show_adc = adc_interval
                show_gps -= 1
                show_adc -= 1
    finally:
        proc.terminate()
        proc.wait()


if __name__ == '__main__':
    main()
