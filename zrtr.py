#!/usr/bin/env python
#
"""
zrtr.py - ZeroMQ based network interface to RTR
"""
import sys
import os
import zmq
import time
import socket
import threading
import struct
import logging
import argparse
import random
import json
import io
import signal
from calendar import timegm
from decimal import Decimal, getcontext
from ConfigParser import RawConfigParser, Error
from subprocess import Popen, PIPE, STDOUT


# RICS server port (TCP and UDP)
RICS_PORT = 10130

# Volts per count
VPC = Decimal(5) / Decimal(4096)

# Voltage resolution
VRES = Decimal('0.001')

# /proc interface to A/D data
PROC_FILE = '/proc/iguana_adc'


def signal_handler(signal, frame):
    sys.exit(0)


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b: a*x + b, self.C, 0)
        return y, self.units


class TaskManager(object):
    """
    Class to manage a set of background daemon threads. Each
    thread function must connect a ZeroMQ SUB socket to the
    endpoint 'inproc://master'. Any message received on that
    socket signals the function to exit.
    """
    timeout = 5

    def __init__(self, context=None):
        ctx = context or zmq.Context.instance()
        self.sig = ctx.socket(zmq.PUB)
        self.sig.bind('inproc://master')
        self.tasks = {}

    def add_task(self, func, *args, **kwds):
        t_id = threading.Thread(target=func,
                                name=func.__name__,
                                args=args,
                                kwargs=kwds)
        self.tasks[t_id] = (func, args, kwds)
        t_id.daemon = True

    def start(self):
        """
        Start all tasks.
        """
        logging.info('Starting background threads')
        for t_id in self.tasks:
            t_id.start()

    def stop(self):
        """
        Stop all tasks.
        """
        logging.info('Stopping background threads')
        self.sig.send(b'DONE')
        for t_id in self.tasks:
            t_id.join(self.timeout)
            if t_id.is_alive():
                logging.critical('Task %s did not exit', t_id.name)

    def check(self):
        """
        Check that all tasks are still running.
        """
        for t_id in self.tasks:
            if not t_id.is_alive():
                logging.critical('Task %s unexpected exit', t_id.name)
                return False
        return True

    def restart(self):
        """
        Restart all tasks.
        """
        self.stop()
        tasks = {}
        logging.info('Restarting background threads')
        for _, val in self.tasks.items():
            func, args, kwds = val
            t_id = threading.Thread(target=func,
                                    name=func.__name__,
                                    args=args,
                                    kwargs=kwds)
            tasks[t_id] = (func, args, kwds)
            t_id.daemon = True
        self.tasks = tasks
        self.start()


def cfgget(cfg, section, option, default=None, filter=None):
    """
    Return an option from a :class:`ConfigParser` object.
    """
    if cfg.has_option(section, option):
        val = cfg.get(section, option)
    else:
        val = default
    return filter(val) if filter else val


def cfgdump(cfg):
    """
    Dump the contents of a :class:`ConfigParser` instance as a list
    of tuples of the form::

        (*section*/*option*, *value*)
    """
    L = []
    for s in cfg.sections():
        for k, v in cfg.items(s):
            L.append(('{}/{}'.format(s, k), v))
    return L


def publish_gps(endpoint='inproc://gps', context=None):
    """
    Thread to stream GPSd data records over a ZeroMQ PUSH socket.
    """
    ctx = context or zmq.Context.instance()
    pub = ctx.socket(zmq.PUSH)
    pub.bind(endpoint)

    # Open a connection to GPSd
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    s.connect(('localhost', 2947))
    s.setblocking(True)
    f = s.makefile()
    cmd = '?WATCH={"enable":true,"json":true}\n'
    s.send(cmd.encode('us-ascii'))

    while True:
        pub.send(f.readline())


def fake_gps(data, endpoint='inproc://gps', context=None):
    """
    Thread to stream fake GPSd data records over a ZeroMQ PUB socket.
    """
    ctx = context or zmq.Context.instance()
    pub = ctx.socket(zmq.PUSH)
    pub.bind(endpoint)
    while True:
        time.sleep(1)
        data['mode'] = random.choice([1, 2, 3])
        data['time'] = time.strftime('%Y-%m-%dT%H:%M:%S +0000',
                                     time.gmtime())
        pub.send_json(data)


def read_adc(fname, cals, vpc, vres):
    """
    Read the current A/D inputs and return as a CSV string.

    :param fname: /proc filename for A/D data
    :param cals: list of *channel*, *Calibration* tuples
    :param vpc: volts per A/D count
    :return: CSV formatted string with timestamp and A/D values
    """
    with io.open(fname, 'r') as ifp:
        line = ifp.read()
    f = line.strip().split()
    output = [f[0]]
    getcontext().prec = 4
    for chan, cal in cals:
        x = (Decimal(f[chan+1]) * vpc).quantize(vres)
        y, units = cal(x)
        output.append('{0} {1}'.format(str(y).encode('us-ascii'), units))
    return b','.join(output)


def beacon_pkt(gdata, cmdport, data=''):
    """
    Construct a beacon packet from a GPSD TPV data record.
    """
    t = timegm(time.strptime(gdata['time'][:19] + ' UTC',
                             '%Y-%m-%dT%H:%M:%S %Z'))
    rtr_id = int(os.environ.get('RTRID', 0xff))
    pkt = struct.pack('>B3xlllB3xH',
                      rtr_id,
                      t,
                      int(gdata['lat'] * 1000000),
                      int(gdata['lon'] * 1000000),
                      int(gdata['mode']),
                      cmdport)
    if data:
        # Include engineering data
        trailer = struct.pack('{0}p'.format(len(data)+1),
                              data)
    else:
        trailer = b'\x00'
    return pkt + trailer


def beacon(addr, port, interval, cmdport, cals,
           adcfile=PROC_FILE, eng_interval=10, context=None):
    """
    Thread to send a periodic beacon packet to a UDP address.

    :param addr: broadcast IP address
    :param port: UDP port
    :param interval: initial beacon interval in seconds
    :param cmdport: TCP command port
    :params cals: calibration values for engineering A/D,
                  if empty, no engineering data will be
                  included in the packet.
    """
    # Initialize UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    # Initialize control socket
    ctx = context or zmq.Context.instance()
    ctl = ctx.socket(zmq.PAIR)
    ctl.connect('inproc://beacon')
    sub = ctx.socket(zmq.PULL)
    sub.connect('inproc://gps')

    count = interval
    eng_count = eng_interval
    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    poller.register(ctl, zmq.POLLIN)

    while True:
        socks = dict(poller.poll())
        if socks.get(sub) == zmq.POLLIN:
            gdata = sub.recv_json()
            if gdata['class'] == 'TPV':
                count -= 1
                if count == 0:
                    eng_count -= 1
                    if cals and (eng_count == 0):
                        data = read_adc(adcfile, cals, VPC, VRES)
                        data = data.encode('us-ascii')
                        eng_count = eng_interval
                    else:
                        data = ''
                    s.sendto(beacon_pkt(gdata, cmdport, data), 0,
                             (addr, port))
                    count = interval
        if socks.get(ctl) == zmq.POLLIN:
            msg = ctl.recv_json()
            interval = msg.get('interval', interval)
            if not msg.get('send_eng', False):
                cals = []


def data_msg(line):
    """
    Convert a line of output from the data collection process to
    a data-stream message.
    """
    tag, contents = line.split(':', 1)
    if tag == 'L':
        return None

    if tag in ('T', 'D'):
        # Extract the absolute time from the record
        try:
            t, rest = contents.split(' ', 1)
        except ValueError:
            t = contents.strip()
            rest = ''
    else:
        t = time.time()
        rest = contents

    rtr_id = int(os.environ.get('RTRID', 0xff))
    return [struct.pack('>cBll', tag, rtr_id, int(t), 0), rest.strip()]


def eng_monitor(adfile, cals, rics_addr, interval, context=None):
    """
    Thread to send the engineering data to RICS.
    """
    rtr_id = int(os.environ.get('RTRID', 0xff))
    ctx = context or zmq.Context.instance()
    rcvr = ctx.socket(zmq.SUB)
    rcvr.setsockopt(zmq.SUBSCRIBE, b'')
    rcvr.connect('inproc://master')

    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(4)
    dstream.connect('tcp://{0}:{1:d}'.format(rics_addr, RICS_PORT))

    poller = zmq.Poller()
    poller.register(rcvr, zmq.POLLIN)

    try:
        while True:
            socks = dict(poller.poll(timeout=interval*1000))
            # Any message from the master forces termination
            if rcvr in socks:
                rcvr.recv()
                logging.info('Shutting down')
                break
            else:
                # Interval timer expired
                data = read_adc(adfile, cals, VPC, VRES)
                t, rest = data.split(',', 1)
                secs, usecs = divmod(long(float(t) * 1000000), 1000000)
                msg = [struct.pack('>cBll', b'E', rtr_id, secs, usecs),
                       rest.encode('us-ascii')]
                try:
                    dstream.send_multipart(msg, flags=zmq.NOBLOCK)
                except Exception:
                    logging.exception('Cannot PUSH engineering message')
                    break
    except Exception:
        logging.exception('ERROR')
    rcvr.close(linger=0)
    dstream.close(linger=0)


def process_monitor(progname, cfgfile, rics_addr, context=None):
    """
    Thread to manage the external data collection process.
    """
    ctx = context or zmq.Context.instance()
    rcvr = ctx.socket(zmq.SUB)
    rcvr.setsockopt(zmq.SUBSCRIBE, b'')
    rcvr.connect('inproc://master')
    dstream = ctx.socket(zmq.PUSH)
    # Set the output queue high-water-mark low so we can
    # detect if the server goes offline.
    dstream.set_hwm(4)
    dstream.connect('tcp://{0}:{1:d}'.format(rics_addr, RICS_PORT))
    poller = zmq.Poller()

    # Start the tracking program
    logging.info('Starting process %s', progname)
    proc = Popen([progname, '-c', cfgfile],
                 bufsize=0,
                 stdout=PIPE,
                 stderr=STDOUT)

    fd = proc.stdout.fileno()
    logging.info('Process ID: %d', proc.pid)
    poller.register(rcvr, zmq.POLLIN)
    poller.register(fd, zmq.POLLIN)

    while True:
        socks = dict(poller.poll())
        # Any message from the master forces termination
        if rcvr in socks:
            rcvr.recv()
            logging.info('Shutting down')
            break
        if fd in socks:
            m = data_msg(proc.stdout.readline())
            if m is not None:
                logging.debug('SEND: %r', m)
                try:
                    dstream.send_multipart(m, flags=zmq.NOBLOCK)
                except zmq.ZMQError:
                    logging.critical('Cannot PUSH data message')
                    break
    proc.terminate()
    proc.wait()
    dstream.close(linger=0)
    rcvr.close(linger=0)


def cmd_GET(contents, cfg):
    """
    Handler for the GET command.
    """
    keys = contents.strip().split()
    result = []
    if not keys:
        # Return all of the entries
        for k, v in cfgdump(cfg):
            result.append('{}={}'.format(k, v))
    else:
        for k in keys:
            try:
                section, opt = k.split('/')
                if cfg.has_option(section, opt):
                    result.append('{0}={1}'.format(k, cfg.get(section, opt)))
            except ValueError:
                logging.exception('GET')
    return [b'GET', b' '.join(result)]


def cmd_SET(contents, cfg):
    """
    Handler for the SET command.
    """
    result = []
    for setting in contents.strip().split():
        try:
            k, v = setting.split('=')
            logging.debug('Config update: %s -> %s', v, k)
            section, opt = k.split('/')
            if cfg.has_section(section):
                cfg.set(section, opt, v)
                result.append(setting)
        except ValueError:
            logging.exception('SET')
    return [b'SET',
            b' '.join(result)]


def cmd_BYE(contents, cfg):
    """
    Handler for the BYE command.
    """
    return [b'BYE', b'']


def cmd_HELLO(contents, cfg):
    """
    Handler for the HELLO command.
    """
    body = b' '.join(['{}={}'.format(k, v)
                      for k, v in cfgdump(cfg)])
    return [b'HELLO', body.encode('us-ascii')]


def cmd_PING(contents, cfg):
    """
    Handler for the PING command.
    """
    return [b'PONG', json.dumps(time.time())]


def mainloop(progname, cfgfile, fakegps, adcfile, bcast):
    cfg = RawConfigParser()
    with io.open(cfgfile, 'rb') as fp:
        cfg.readfp(fp)

    # Extract A/D calibration data from the config file
    cals = []
    try:
        for name, value in cfg.items('adc'):
            if name.startswith('ai'):
                chan = int(name[2:])
                f = value.split(',')
                # The last item in ``values`` is the units, the rest of the
                # items are calibration coefficients
                coeffs = [Decimal(x) for x in f[:-1]]
                cals.append((chan, Calibration(coeffs, f[-1])))
    except Error:
        logging.critical('No A/D coefficients in config file')
        raise SystemExit(1)

    gps_interval = cfgget(cfg, 'gps', 'interval', default=10, filter=int)
    adc_interval = cfgget(cfg, 'adc', 'interval', default=10, filter=int)

    # Open command socket
    ctx = zmq.Context.instance()
    svc = ctx.socket(zmq.ROUTER)
    port = svc.bind_to_random_port('tcp://*')
    # Open socket to control the beacon
    bsock = ctx.socket(zmq.PAIR)
    bsock.bind('inproc://beacon')

    if fakegps:
        f = fakegps.split(',')
        data = dict(lat=float(f[0]), lon=float(f[1]), mode=3)
        data['class'] = 'TPV'
        logging.info('Using fake GPS data: %r', data)
        thr = threading.Thread(target=fake_gps,
                               name='fake_gps',
                               args=(data,),
                               kwargs={'context': ctx})
        thr.daemon = True
        thr.start()
    else:
        # Start the GPS publisher followed by the beacon
        thr = threading.Thread(target=publish_gps,
                               name='gps_reader',
                               kwargs={'context': ctx})
        thr.daemon = True
        thr.start()

    btask = threading.Thread(target=beacon,
                             name='beacon',
                             args=(bcast,
                                   RICS_PORT,
                                   1,
                                   port,
                                   cals),
                             kwargs={'context': ctx, 'adcfile': adcfile})
    btask.daemon = True
    logging.info('Starting beacon')
    btask.start()

    dispatcher = {
        'GET': (cmd_GET, False),
        'SET': (cmd_SET, True),
        'BYE': (cmd_BYE, False),
        'PING': (cmd_PING, False),
        'HELLO': (cmd_HELLO, False)
    }

    logging.info('Waiting for RICS')
    # Wait for initial HELLO message from RICS. While waiting,
    # we also allow the SET and GET commands.
    try:
        while True:
            msg = svc.recv_multipart()
            logging.debug('RECV: %r', msg)
            try:
                ident, cmd, contents = msg
            except Exception:
                svc.send_multipart([ident,
                                    b'ERROR',
                                    b'Invalid message: ' + repr(msg)])
            else:
                if cmd == b'HELLO' and contents:
                    rics_addr = contents
                    func, _ = dispatcher.get(cmd)
                    svc.send_multipart([ident] + func(contents, cfg))
                    break
                elif cmd in (b'GET', b'SET'):
                    func, cfgmod = dispatcher.get(cmd)
                    svc.send_multipart([ident] + func(contents, cfg))
                    if cfgmod:
                        # Update the config file.
                        with io.open(cfgfile, 'wb') as f:
                            cfg.write(f)
                else:
                    svc.send_multipart([ident,
                                        b'ERROR', b'Identify yourself'])
    except KeyboardInterrupt:
        logging.info('Exiting ...')
        raise SystemExit(0)

    # Slow down the beacon and no longer include engineering data
    bsock.send_json({'interval': gps_interval, 'send_eng': False})

    logging.info('RICS server available at %s', rics_addr)
    tasks = TaskManager(context=ctx)
    tasks.add_task(process_monitor, progname, cfgfile, rics_addr,
                   context=ctx)
    tasks.add_task(eng_monitor, adcfile, cals, rics_addr, adc_interval,
                   context=ctx)
    tasks.start()

    poller = zmq.Poller()
    poller.register(svc, zmq.POLLIN)

    try:
        while True:
            socks = dict(poller.poll(timeout=30000))
            if socks.get(svc) == zmq.POLLIN:
                msg = svc.recv_multipart()
                logging.debug('RECV: %r', msg)
                try:
                    ident, cmd, contents = msg
                except Exception:
                    svc.send_multipart([ident,
                                        b'ERROR',
                                        b'Invalid message: ' + repr(msg)])
                    continue

                handler = dispatcher.get(cmd)
                if handler:
                    func, restart = handler
                    reply = func(contents, cfg)
                    svc.send_multipart([ident] + reply)
                    logging.debug('SEND: %r', reply)

                    # Check for loop exit conditions
                    if cmd == b'BYE':
                        break

                    if not tasks.check():
                        raise RuntimeError('Background tasks have stopped')

                    if restart:
                        # Update config file and restart the threads
                        with io.open(cfgfile, 'wb') as f:
                            cfg.write(f)
                        tasks.restart()
                else:
                    svc.send_multipart([ident,
                                        b'ERROR',
                                        b'Unexpected command: {0}'.format(cmd)])
            else:
                if not tasks.check():
                    raise RuntimeError('Background tasks have stopped')
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ...')
    finally:
        tasks.stop()
        bsock.close(linger=0)
        svc.close(linger=0)


def main():
    parser = argparse.ArgumentParser(description='ZeroMQ RTR interface')
    parser.add_argument('cfgfile', metavar='CONFIGFILE',
                        help='configuration file')
    parser.add_argument('--prog', metavar='PROGNAME',
                        default='rtr_adc_test',
                        help='name of tracking program (must be in PATH)')
    parser.add_argument('--fakegps', metavar='LAT,LON',
                        help='use a fake GPS location')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--adc', metavar='FILE',
                        default=PROC_FILE,
                        help='/proc file for A/D data')
    parser.add_argument('--bcast', metavar='IPADDR',
                        default='255.255.255.255',
                        help='beacon broadcast address')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(threadName)-10s %(message)s')

    signal.signal(signal.SIGTERM, signal_handler)
    mainloop(args.prog, args.cfgfile, args.fakegps, args.adc, args.bcast)


if __name__ == '__main__':
    main()
