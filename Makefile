#
# Makefile for RTR ADC software.
#
CC = gcc
CFLAGS = -Wall -O3 -msse2 -mfpmath=sse -I. $(shell pkg-config --cflags glib-2.0)
LDFLAGS = $(shell pkg-config --libs glib-2.0 gthread-2.0) \
          -L matlab_lib -lmatlab \
          -l24dsi_utils -lgsc_utils -l24dsi_dsl -lrt -lm
BINDIR = $(HOME)/bin
CFGDIR = $(HOME)/config
SVCDIR = $(HOME)/sv

PROGS = rtr_adc_test rtr.py zrtr.py
CFGFILES = settings.conf corrA.txt corrB.txt firAB.txt
SVC = rtrd/run

all: rtr_adc_test

%.o: %.c
	$(CC) $(CFLAGS) -I. -I./matlab_lib -c $< -o $@

.PHONY: matlab_lib install install-sv

matlab_lib:
	$(MAKE) -C $@

rtr_adc_test: rtr_adc_test.o logging.o matlab_lib
	$(CC) rtr_adc_test.o logging.o $(LDFLAGS) -o rtr_adc_test

clean:
	$(MAKE) -C matlab_lib clean
	rm -f *.o *.so *~ *.la *.lo rtr_adc_test

install-sv: $(SVC)
	install -d $(SVCDIR)
	cp -av $(dir $(SVC)) $(SVCDIR)/

install: $(PROGS) $(CFGFILES)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $(PROGS)
	install -d $(CFGDIR)
	install -m 644 -t $(CFGDIR) $(CFGFILES)
