/**
 *
 * Glib-based logging functions
 *
 */
#include <stdio.h>
#include <time.h>
#include <glib.h>
#include <stdlib.h>

static int max_log_level = G_LOG_LEVEL_INFO;

void log_handler(const gchar *domain, GLogLevelFlags level,
		 const gchar *message, gpointer arg)
{
    FILE	*ofp;
    struct tm	tm;
    time_t	t;
    char	datetime[64];

    ofp = arg ? arg : stderr;
    
    if((level & G_LOG_LEVEL_MASK) <= max_log_level)
    {
	time(&t);
	gmtime_r(&t, &tm);
	strftime(datetime, sizeof(datetime), "%Y-%m-%d %H:%M:%SZ", &tm);
	fprintf(ofp, "L:[%s] %s\n", datetime, message);
    }

    if(level & G_LOG_FATAL_MASK)
    {
	fprintf(ofp, "L:Aborting ...\n");
	abort();
    }
    
}

int log_set_level(int level)
{
    int	l = max_log_level;
    
    max_log_level = level;
    
    return l;
}

    
