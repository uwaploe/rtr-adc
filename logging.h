#ifndef _LOGGING_H_
#define _LOGGING_H_

void log_handler(const gchar *domain, GLogLevelFlags level,
		 const gchar *message, gpointer arg);
int log_set_level(int level);

#endif
